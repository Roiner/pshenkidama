<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Afiliado extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Afiliado_Model');
		$this->load->model('Principal_Model');
		$this->load->model('Actividad_Model');
	
		$this->load->helper('download');

		$this->load->helper('url');
	}

	public function addResponsable()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		
		$this->Afiliado_Model->addResponsable($this->input->post());
		#$this->Afiliado_Model->viewResp($this->input->post('vendor_id'));
	}

	public function asignaResponsable($afiliado)
	{
		$data['responsables'] = $this->Afiliado_Model->Responsables();
		$data['afiliado'] = $afiliado;
		$this->load->view('pages/Afiliados/asResponsable', $data);
	}

	public function asignaResponsable2($afiliado)
	{
		$responsablesAsignados = $this->Afiliado_Model->vendor($afiliado);

		foreach ($responsablesAsignados as $key => $responsablesAsignado) {
			$responsables[$key] = $responsablesAsignado->id_vendor;
		}

		$data['responsables'] = $this->Afiliado_Model->Responsables2($responsables);
		
		$data['afiliado'] = $afiliado;
		$this->load->view('pages/Afiliados/asResponsable2', $data);
	}


	public function saveAsignaResponsable2 ()
	{
		$responsables = $this->input->post('responsables');
		$afiliado = $this->input->post('afiliado');

		foreach ($responsables as $key => $responsable) {
			
			$datos['affiliate_id'] = $afiliado;
			$datos['vendor_id'] = $responsable;

			$this->Afiliado_Model->addResponsable2($datos);
		}

		echo json_encode(['message' => 'Responsables Asignados']);
	}

	function disable()
	{
		$afiliado = $this->input->post('id_affiliate');
		$this->Afiliado_Model->disableAfiliado($afiliado);
	}

	public function index()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Agregar Afiliado";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Afiliados/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Afiliados/nuevoAfiliado');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Afiliados/scripts');
		$this->load->view('templates/endHtml');
	}

	function newAfiliado()
	{

		foreach ($_FILES as $key) 
		{
			$fallo = 1;
			$file = $key['tmp_name'];

			//load the excel library
			$this->load->library('excel');
			 
			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			 
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			 
			foreach ($objPHPExcel->getActiveSheet()->getRowIterator() as $row) {
				 
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false);
				foreach ($cellIterator as $cell) {
					if ($cell->getRow() > 1 ) 
					{
						if (!$cell->getValue()) 
						{
							$fallo = 0;
							echo 'Inconsistencia en el Archivo a Importar';
							break;								
						} 
					}
					
				}
			}
			
			if ($fallo == 1) 
			{
				//extract to a PHP readable array format
				foreach ($cell_collection as $cell) {
				    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
				 
				    //The header will/should be in row 1 only. of course, this can be modified to suit your need.
				    if ($row == 1) {
				        $header[$row][$column] = $data_value;
				    } else {
				        $arr_data[$row][$column] = $data_value;
				    }
				}
				//send the data in an array format
				$data['header'] = $header;
				$data['values'] = $arr_data;
				echo $this->load->view('pages/Afiliados/successAfiliado', $data, TRUE);
			}
		}
	}


	function uploadExcel ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$config = [

			'upload_path' => "./files/afiliados/",
			'allowed_types'	=>	'xls',
			'overwrite' => TRUE
		];

		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();

			$this->load->library('Spreadsheet_excel_reader');
			
			$this->spreadsheet_excel_reader->setOutputEncoding('CP1251');
			
			$this->spreadsheet_excel_reader->read($data['full_path']);

			$sheets = $this->spreadsheet_excel_reader->sheets[0];

			error_reporting(0);

			for ($i = 2; $i <= $sheets['numRows']; $i++) {
			
				if ($sheets['cells'][$i][1] == '') break; 
					    $fecha_nac = date_create($sheets['cells'][$i][15]);
					    $fecha_nac = date_format($fecha_nac, "Y-m-d");
						$data_excel['names_affiliate'] = strtoupper($sheets['cells'][$i][1]);
						$data_excel['last_names_affiliate'] = strtoupper($sheets['cells'][$i][2]);
						$data_excel['dni_affiliate'] = $sheets['cells'][$i][3];
						$data_excel['street_affiliate'] = strtoupper($sheets['cells'][$i][4]);
						$data_excel['apartment_affiliate'] = strtoupper($sheets['cells'][$i][5]);
						$data_excel['number_affiliate'] = $sheets['cells'][$i][6];
						$data_excel['dtto_affiliate'] = $sheets['cells'][$i][7];
						$data_excel['postal_code_affiliate'] = $sheets['cells'][$i][8];
						$data_excel['job_affiliate'] = $sheets['cells'][$i][9];
						$data_excel['studies_affiliate'] = $sheets['cells'][$i][10];
						$data_excel['denomination_affiliate'] = $sheets['cells'][$i][11];
						$data_excel['circuit_affiliate'] = $sheets['cells'][$i][12];
						$data_excel['commune_affiliate'] = $sheets['cells'][$i][13];
						$data_excel['gener_affiliate'] = $sheets['cells'][$i][14];
						$data_excel['date_age_affiliate'] = $fecha_nac;
						$data_excel['age_affiliate'] = $sheets['cells'][$i][16];
						$data_excel['facebook_affiliate'] = $sheets['cells'][$i][17];
						$data_excel['twitter_affiliate'] = $sheets['cells'][$i][18];
						$data_excel['instagram_affiliate'] = $sheets['cells'][$i][19];
						$data_excel['email_affiliate'] = $sheets['cells'][$i][20];
						$data_excel['phone_1_affiliate'] = $sheets['cells'][$i][21];
						$data_excel['phone_2_affiliate'] = $sheets['cells'][$i][22];
						$data_excel['checked_phone'] = 0;
						$data_excel['mobile_1_affiliate'] = $sheets['cells'][$i][23];
						$data_excel['mobile_2_affiliate'] = $sheets['cells'][$i][24];
						$data_excel['mobile_3_affiliate'] = $sheets['cells'][$i][25];
						$data_excel['mobile_4_affiliate'] = $sheets['cells'][$i][26];
						$data_excel['mobile_5_affiliate'] = $sheets['cells'][$i][27];
						$data_excel['mobile_6_affiliate'] = $sheets['cells'][$i][28];
						$data_excel['comments_affiliate'] = $sheets['cells'][$i][29];
						$data_excel['status_affiliate'] = 11;
					
						$search = $this->Afiliado_Model->searchtAffilates($sheets['cells'][$i][3]);

						if ($search == 0) {
							$this->db->insert('affiliates', $data_excel);
						}else{
							$this->db->where('dni_affiliate',$sheets['cells'][$i][3]);
							$this->db->update('affiliates', $data_excel);
						}
					}

				echo json_encode(['status' => 'success', 'message' => 'Afiliados agregados!']);
		}else{
				echo json_encode(['status' => 'warning', 'message' => 'Error intente nuevamente!']);
		}
	}

	function removeResponsable()
	{
		$this->Afiliado_Model->removeResponsable($this->input->post());
	}

	function saveAfiliado()
	{
		$this->Afiliado_Model->saveAfiliado($this->input->post());
	}

	function updateData($affiliate)
	{
		$this->Afiliado_Model->updateDataAffiliate($affiliate,$this->input->post());
	}

	function updateCatData($affiliate)
	{
		$datos = [
		    'status_affiliate' => $this->input->post('status_affiliate'),
		    'comments_affiliate' => $this->input->post('comments_affiliate')
		];
		$contactDatos = [
		    'date_contact_affiliate' => $this->input->post('date_contact_affiliate'),
		    'affiliate_id' => $affiliate,
		    'contact_id' => $this->input->post('contact_id')
		];

		$this->Afiliado_Model->updateDataAffiliate($affiliate,$datos);
		$this->Afiliado_Model->insertContactAffiliate($contactDatos);
	}

	function verAfiliado($id)
	{
		$this->load->model('Interes_Model');
		$data['afiliado'] = $this->Afiliado_Model->afiliado($id);
		$data['vendors'] = $this->Afiliado_Model->vendor($id);
		$data['tipo_contacto'] = $this->Afiliado_Model->tContacto();
		$data['status'] = $this->Afiliado_Model->status();
		$data['contacts'] = $this->Afiliado_Model->contacts($id);
		$data['id_affiliate'] = $id;
		$data['interests'] = $this->Interes_Model->listInterest();
		if ($_SESSION['type_user'] == 3) {
			$data['activities'] = $this->Actividad_Model->activitiesName($_SESSION['id_vendor']);
		}

		$this->load->view('templates/styles', $data);
		// $this->load->view('pages/Afiliados/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Afiliados/viewAfiliado', $data);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Afiliados/scripts');
		$this->load->view('templates/endHtml');
	}

	function vendors ($id)
	{
		echo json_encode($this->Afiliado_Model->vendor($id));
	}

	function probar ()
	{
		echo json_encode($this->Actividad_Model->activitiesName($_SESSION['id_vendor']));
	}

	public function addAfiliado()
	{
		$datos = [
		    'id_affiliate' => $this->input->post('id_affiliate'),
		    'id_affiliate_associate' => $this->input->post('id_associate')
		];
		$this->Afiliado_Model->addAfiliadoAsociado($datos);
		echo json_encode(['status' => 'ok']);
	}

	

	public function getAssociates($id)
	{
		$afiliados = $this->Afiliado_Model->afiliadosAsociados($id);
		$result = array('data' => array());

		foreach ($afiliados as $key => $value) {
			$buttons =<<<EOT
			<button class="btn btn-danger btn-remove-associate" value="{$value->id_affiliate_associate}"><i class="fa fa-remove"></i></button>
EOT;

			$result['data'][$key] = array(
				$value->names_affiliate,
				$value->dni_affiliate,
				$buttons
			);
		}
		echo json_encode($result);
	}

	public function getAffiliates($id)
	{
		$result = array('data' => array());
		$asociados = $this->Afiliado_Model->afiliadosAsociados($id);
		$array_asociados=array();
		foreach ($asociados as $key => $value) {
			array_push($array_asociados, $value->id_affiliate_associate);
		}

		$afiliados = $this->Afiliado_Model->afiliadosAsociadosConAfiliado($id,$array_asociados);

		foreach ($afiliados as $key => $value) {
			$buttons =<<<EOT
			<button class="btn btn-info btn-add-affiliate" value="{$value->id_affiliate}"><i class="fa fa-plus"></i></button>
EOT;

			$result['data'][$key] = array(
				$value->names_affiliate,
				$value->dni_affiliate,
				$buttons
			);
		}
		echo json_encode($result);
	}

	public function deleteAsociate()
	{
		$datos = [
		    'id_affiliate' => $this->input->post('id_affiliate'),
		    'id_affiliate_associate' => $this->input->post('id_associate')
		];
		$this->Afiliado_Model->eliminarAsociacion($datos);
		echo json_encode(['status' => 'ok']);
	}

	public function addAssociateInterest()
	{
		$this->load->model('Interes_Model');


		foreach ($this->input->post('interests') as $key => $value) {
			$interest = $this->Interes_Model->searchAffilatesInteres($this->input->post('id_affiliate'),$value['val']);

			$datos=['id_affiliate' => $this->input->post('id_affiliate'), 'id_interest' => $value['val']];
			
			if (count($interest) == 0) {
				if ($value['is_checked']=='true') {
					echo "en 1";
					$this->Interes_Model->addAffilatesInteres($datos);
				}else{
					echo "en 2";
					$this->Interes_Model->deleteAffilatesInteres($datos);
				}
			}else{
				if ($value['is_checked']=='false') {
					echo "en 3";
					$this->Interes_Model->deleteAffilatesInteres($datos);
				}
			}
		}

		if ($this->input->post('other_field') != null) {
			echo "aqui";
			$datos_interest=['name' => $this->input->post('other_field'), 'id_user' => $_SESSION['id_user'] ];
			$id_interest = $this->Interes_Model->saveInteres($datos_interest);
			$datos=['id_affiliate' => $this->input->post('id_affiliate'), 'id_interest' => $id_interest];
			$this->Interes_Model->addAffilatesInteres($datos);
		}

		echo json_encode(['status' => 'ok']);
	}
	

}

/* End of file Afiliado.php */
/* Location: ./application/controllers/Afiliado.php */