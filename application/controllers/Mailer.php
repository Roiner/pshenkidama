<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
		$this->load->model('Actividad_Model');
		$this->load->model('Afiliado_Model');
		$this->load->model('Mailer_Model');
	}

	function compose()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Nuevo Mensaje";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['actividades'] = $this->Actividad_Model->listActivities();
		$data['vendors'] = $this->Afiliado_Model->Responsables();
		$data['type_user'] = $_SESSION['type_user'];
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Mailer/newMail', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function index()
	{
		if ($this->session->userdata('type_user') != 3) {
			redirect(base_url('Principal/error404'));
		}

		$id_vendor = $_SESSION['id_vendor'];
		$data['title_page'] = "Mensajes de Entrada";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$data['mailers']	= $this->Mailer_Model->inbox($id_vendor);
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Mailer/principalMailer');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	function view()
	{
		$data['title_page'] = "Detalle de Mensajes";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Mailer/detalleMail');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	function loadVendors ($activity_id)
	{
		$res = $this->Actividad_Model->showVendors($activity_id);

		if (count($res) > 0) {
			
			$vendors = [];
			
			foreach ($res as $key => $r)  {
				$vendors[$key] = [
					'id' 	=>  $r->id_vendor,
					'email' =>	$r->email_vendor	
				];
			}

			echo json_encode($vendors);
		}
		
	}

	function create ()
	{
		#echo json_encode($this->input->post('file-ad'));
		#echo json_encode($this->input->post());

		$random = rand();

		$data['activity_id'] 	= 	$this->input->post('activity_id');
		$data['subject'] 		= 	$this->input->post('subject');
		$data['message'] 		= 	$this->input->post('message');
		$data['date'] 			= 	date('Y-m-d H:i:s');
		$data['admin_id']		= 	$_SESSION['id_admin'];
		#$data['file']			= 	"files/mails/files/".$random.$_FILES['file']['name'];

		/*$config = array(
			'file_name' => $random.	$_FILES['file']['name'],
			'upload_path' => "./files/mail/files",
			'allowed_types' => "gif|jpg|png|jpeg|doc|docx|pdf|ppt|pptx|xls|xlsx|zip|rar|txt",
			'overwrite' => TRUE
			
		);
		$this->load->library('upload', $config);

		$this->upload->do_upload('file');
		*/

		$mailer_id = $this->Mailer_Model->saveData($data);

		$vendors = $this->input->post('vendors');

		foreach ($vendors as $key => $vendor) {
			
			$inbox['mailer_id'] = $mailer_id;
			$inbox['vendor_id'] = $vendor;
			$inbox['readed']	= 0;
			
			$this->Mailer_Model->createInbox($inbox);

			$vendor  = $this->Afiliado_Model->viewResp($vendor);

			$this->sendEmail($vendor, $data['subject'], $data['activity_id'], $data['message']);
		}

		echo json_encode(['status' => 'success', 'message' => 'Registro exitoso, mensaje enviado!']);
	}



	public function sendEmail ($vendor, $subject, $activity, $message)
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		
		$actividad = $this->Actividad_Model->showActivity($activity);

		$data = [
			'vendor'	=> 	$vendor[0]->name_vendor,
			'message'	=>	$message,
			'activity'	=>	$actividad[0]->name_activity
		];

		$this->load->library('encrypt');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'gator4046.hostgator.com',
		    'smtp_port' => 465,
		    'smtp_crypto' => 'ssl',
		    'smtp_user' => 'hola@henkidama.com',
		    'smtp_pass' => '39UWy9T~HtxF',
		    'mailtype'  => 'html', 
		    'charset'   => 'utf-8',
		    'newline' 	=> "\r\n",
			'crlf' 		=> "\r\n",
			'wordwrap'  => TRUE
		);
		$this->load->library('email', $config);
		
		$this->email->to($vendor[0]->email_vendor);
		$this->email->from('hola@henkidama.com', 'CRM - Gestar');
		$this->email->subject($subject);
		$this->email->message($this->load->view('emails/message', $data, true));
		$this->email->send();

	}


	function outbox ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		
		$id_admin = $_SESSION['id_admin'];
		$data["mailers"] = $this->Mailer_Model->outbox($id_admin);

		$data['title_page'] = "Mensajes Enviados";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Mailer/outbox', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');

	}

	public function sendInvitation ()
	{
		#echo json_encode($this->input->post());

		$this->load->library('encrypt');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'gator4046.hostgator.com',
		    'smtp_port' => 465,
		    'smtp_crypto' => 'ssl',
		    'smtp_user' => 'hola@henkidama.com',
		    'smtp_pass' => '39UWy9T~HtxF',
		    'mailtype'  => 'html', 
		    'charset'   => 'utf-8',
		    'newline' 	=> "\r\n",
			'crlf' 		=> "\r\n",
			'wordwrap'  => TRUE
		);
		$this->load->library('email', $config);
		
		#buscar afiliado y traer el mail
		#Buscar la actividad y traer

		$affiliate = $this->Afiliado_Model->afiliado($this->input->post('id_affiliate'));
		$actividad = $this->Actividad_Model->activity($this->input->post('activity_id'));

		if (!empty($affiliate[0]->email_affiliate)) {
			$this->email->to($affiliate[0]->email_affiliate);
			$this->email->from('hola@henkidama.com', 'CRM - Gestar');
			$this->email->subject('Invitación');

			$data['afiliado'] = $affiliate;
			$data['actividad'] = $actividad;

			$this->email->message($this->load->view('emails/invitation', $data, true));
			$this->email->send();
			echo json_encode(['message' => 'Invitación enviada']);
		} else {
			echo json_encode(['message' => 'Ups, el afiliado no tiene un correo asignado']);
		}
	}


	public function createMail ()
	{
		// if ($this->session->userdata('type_user') != 3) {
		// 	redirect(base_url('Principal/error404'));
		// }
		
		$id_vendor = $this->session->userdata('id_vendor');
		
		$data['title_page'] = "Crear Mensajes";
		$data['actividades'] = $this->Actividad_Model->activitiesName($id_vendor);
		$data['afiliados'] 	= $this->Afiliado_Model->misAfiliados2($id_vendor);
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Mailer/createMail', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function saveMail ()
	{
		#echo json_encode($this->input->post());

		$afiliados = $this->input->post('afiliados');
		$afiliados = implode(",", $afiliados);
		
		$this->load->library('encrypt');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'gator4046.hostgator.com',
		    'smtp_port' => 465,
		    'smtp_crypto' => 'ssl',
		    'smtp_user' => 'hola@henkidama.com',
		    'smtp_pass' => '39UWy9T~HtxF',
		    'mailtype'  => 'html', 
		    'charset'   => 'utf-8',
		    'newline' 	=> "\r\n",
			'crlf' 		=> "\r\n",
			'wordwrap'  => TRUE
		);
		$this->load->library('email', $config);

		$this->email->bcc($afiliados);
		$this->email->from('hola@henkidama.com', 'CRM - Gestar');
		$this->email->subject('Invitación');

		$data['mensaje'] = $this->input->post('message');

		$this->email->message($this->load->view('emails/invitacion2', $data, true));
		
		$this->email->send();
		echo json_encode(['message' => 'Invitación enviada']);
	}


	public function composeNotification ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
			
		$data['title_page'] = "Crear Notificación";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Mailer/composeNotification', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function loadUsersMail($option)
	{

			$data['afiliados'] = $this->Afiliado_Model->afiliadosMail();
			$data['vendedores'] = $this->Afiliado_Model->vendedoresMail();

			foreach ($data['vendedores'] as $key => $v) {
			$vendedores[$key] = [
									'name' => $v->name_vendor,
									'email' => $v->email_vendor,
									'type' => 'vendedor'
								];
			}

			foreach ($data['afiliados'] as $key => $af) {
				$afiliados[$key] = [
										'name' => $af->names_affiliate.' '.$af->last_names_affiliate,
										'email' => $af->email_affiliate,
										'type' => 'afiliado'
				];
			}

		if ($option == 'afiliados') {

			echo json_encode($afiliados);

		} elseif ($option == 'vendedores') {

			echo json_encode($vendedores);

		}else{

			$data['todos'] = array_merge($vendedores, $afiliados);
			echo json_encode($data['todos']);
		}
		
	}


	public function sendNotification ()
	{
		$destinatarios = $this->input->post('destinatarios');
		$destinatarios = implode(",", $destinatarios);
		
		$this->load->library('encrypt');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'gator4046.hostgator.com',
		    'smtp_port' => 465,
		    'smtp_crypto' => 'ssl',
		    'smtp_user' => 'hola@henkidama.com',
		    'smtp_pass' => '39UWy9T~HtxF',
		    'mailtype'  => 'html', 
		    'charset'   => 'utf-8',
		    'newline' 	=> "\r\n",
			'crlf' 		=> "\r\n",
			'wordwrap'  => TRUE
		);
		$this->load->library('email', $config);

		$this->email->bcc($destinatarios);
		$this->email->from('hola@henkidama.com', 'CRM - Gestar');
		$this->email->subject($this->input->post('subject'));

		$data['mensaje'] = $this->input->post('message');

		$this->email->message($this->load->view('emails/invitacion2', $data, true));
		
		$this->email->send();

		echo json_encode(['message' => 'Notificaciones enviadas']);
	}

}

/* End of file Mailer.php */
/* Location: ./application/controllers/Mailer.php */