<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Confirma_session extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('General_Model');
	}

	public function index()
	{	
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url('login'));
		} else {
			$this->General_Model->inSession();
			if ($this->session->userdata('type_user') == 1) 
			{
				redirect(base_url('principal'));	
			}
			if ($this->session->userdata('type_user') == 2) 
			{
				redirect(base_url('administrador'));	
			}
			if ($this->session->userdata('type_user') == 3) 
			{
				redirect(base_url('misAfiliados'));	
			}
		}
	}

}

/* End of file Confirma_session.php */
/* Location: ./application/controllers/Confirma_session.php */