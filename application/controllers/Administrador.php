<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
	}

	public function index()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page']     = "Principal";
		$raside['solicitudes']  = $this->Principal_Model->solPend();
		$dashboard['totales']   = $this->Principal_Model->totales();
	    $dashboard['status']    = $this->Principal_Model->status();
		$dashboard['afiliados'] = $this->Principal_Model->afiliados();

		$total_assoc = array();
		foreach ($dashboard['afiliados'] as  $value) {
			$id     =  $value->id_affiliate;
			$street =  $value->street_affiliate;
			$number =  $value->number_affiliate;
			$search_asosc_addres = $this->Principal_Model->search_asosc_addres($id, $street, $number);

			$count =  count($search_asosc_addres);

			if ($count > 0) {
				$assoc["id_affiliate"] = $id;
				$assoc["count"]        = $count;

				$total_assoc[] = $assoc;
			}
			
		}

		 // foreach ($total_assoc as  $value) {
   //          print_r($value);
   //          echo $value["id_affiliate"];
   //          echo "<br>";
   //      }

		$dashboard["assocs"] = $total_assoc;

		 $this->load->view('templates/styles', $data);
		 $this->load->view('templates/header');
		 $this->load->view('templates/aside');
		 $this->load->view('templates/raside',$raside);
		 $this->load->view('pages/dashboard',$dashboard);
		 $this->load->view('templates/footer');
		 $this->load->view('templates/scripts');
		 $this->load->view('templates/endHtml');
	}

}

/* End of file Administrador.php */
/* Location: ./application/controllers/Administrador.php */