<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nigglings extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Niggling_Model');
		$this->load->model('Principal_Model');
		$this->load->library('session');
		$this->load->helper('url');
	}


	public function index()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Dudas Recibidas";
		$data['nigglings']	=  $this->Niggling_Model->lista();
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('templates/raside',$raside);
		$this->load->view('pages/Dudas/index', $data['nigglings']);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}


	public function create ()
	{
		if ($this->session->userdata('type_user') != 3) {
			redirect(base_url('Principal/error404'));
		}

            $data = [
			    'subject'	=>	$this->input->post('niggling_subject'),
				'message' 	=>	$this->input->post('niggling_message'),
				'id_vendor'	=>	$_SESSION['id_vendor'],
				'date'		=>	date('Y-m-d'),
				'is_response'	=>	0
			];
		
			$this->Niggling_Model->saveData($data);
	}


	public function find ($id)
	{	
		$this->Niggling_Model->find($id);
	}


	public function saveResponse()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$id = $this->input->post('niggling_id');

		$data = [
			'response'	=>	$this->input->post('niggling_response'),
			'is_response'	=>	1,
			'date_response'	=>	date('Y-m-d')
		];

		$this->Niggling_Model->saveResponse($data, $id);
	}


	public function delete ($id)
	{
		$this->Niggling_Model->delete($id);
	}

	public function dudasVendor()
	{

		if ($this->session->userdata('type_user') != 3) {
			redirect(base_url('Principal/error404'));
		}

		$vendor_id = $_SESSION['id_vendor'];
			
		$data['title_page'] = "Dudas";
		$data['nigglings']	=  $this->Niggling_Model->listVendor($vendor_id);
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('templates/raside',$raside);
		$this->load->view('pages/Dudas/index', $data['nigglings']);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}
}
