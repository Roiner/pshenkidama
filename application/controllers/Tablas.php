<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tablas extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Tablas_Model');
		$this->load->model('Principal_Model');
		$this->load->model('Responsables_Model');
	}

	function disableAdmin($admin)
	{
		#$admin = $this->input->post('id_admin');
		$this->Tablas_Model->disableAdmin($admin);
		$this->Responsables_Model->inhablilitarAdmin($admin);
	}

	function enableAdmin($admin)
	{
		#$admin = $this->input->post('id_admin');
		$this->Tablas_Model->enableAdmin($admin);
		$this->Responsables_Model->hablilitarAdmin($admin);
	}


	public function index()
	{
		if ($_SESSION['type_user'] == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Tablas Principales";
		$data['administradores'] = $this->Tablas_Model->administradores();
		$data['status'] = $this->Tablas_Model->status();
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Tablas/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('templates/raside',$raside);
		$this->load->view('pages/Tablas/tablas',$data);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Tablas/scripts');
		$this->load->view('templates/endHtml');
	}

	public function vendedores()
	{
		if ($_SESSION['type_user'] == 3) {
			redirect(base_url('Principal/error404'));
		}
		
		$data['title_page'] = "Responsables";
		$data['vendedores'] = $this->Responsables_Model->vendedores();
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Tablas/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Tablas/vendedores',$data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Tablas/scripts');
		$this->load->view('templates/endHtml');
	}


	public function delete_vendor($id)
	{
		if ($this->Responsables_Model->delete($id)) {
			
		}
		$data = array('message' => "operacion exitosa");
		echo json_encode($data);
	}

	function newAdmin()
	{
		$this->load->view('pages/Tablas/newAdmin');
	}

	function newStatus()
	{
		$this->load->view('pages/Tablas/newStatus');
	}

	function saveAdmin()
	{
		$checkUser = $this->encrypt->encode($this->input->post('username'));
		$datosAdmin = array('name_admin' => $this->input->post('nombre'),
			 				'phone_admin' => $this->input->post('telefono'),
			 				'direction_admin' => $this->input->post('direccion'),
			 				'email_admin' => $this->input->post('email'),
			 				'sex_admin' => $this->input->post('genero'),
			 				'check_user_admin' => $checkUser);
		$datosUser = array('name_user' => $this->input->post('username'),
		 					'pass_user' => $this->encrypt->encode($this->input->post('password')),
		 					'type_user' => 2,
		 					'status_user' => 1);
		
		$this->Tablas_Model->saveAdmin($datosAdmin,$datosUser,$checkUser);
	}

	function saveStatus()
	{
		$datos = array('name_status' => $this->input->post('nombre'),
						'description_status' => $this->input->post('descripcion'),
						'color_status' => $this->input->post('color'));
		$this->Tablas_Model->saveStatus($datos);
	}

	function updateAdmin($id)
	{
		$datos = [
		    'name_admin' => $this->input->post('name_admin'),
		    'phone_admin' => $this->input->post('phone_admin'),
		    'direction_admin' => $this->input->post('direction_admin'),
		    'email_admin' => $this->input->post('email_admin'),
		    'sex_admin' => $this->input->post('sex_admin'),
		];

		$user = $this->input->post('name_user');
		$datosU = [
		    'pass_user' => $this->encrypt->encode($this->input->post('pass_user'))
		];

		$this->Tablas_Model->updateAdmin($id,$datos,$datosU,$user);
	}

	function viewAdmin($id_admin)
	{
		$data['administrador'] = $this->Tablas_Model->administrador($id_admin);
		$this->load->view('pages/Tablas/viewAdmin', $data);
	}

}

/* End of file Tablas.php */
/* Location: ./application/controllers/Tablas.php */