<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Responsables extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Responsables_Model');
		$this->load->model('Principal_Model');
		$this->load->model('Afiliado_Model');
	}

	function asigna()
	{
		$datos = ($this->input->post());
		
		foreach ($datos['newAfiliados'] as $key => $dato) {
			$vendor_id = $dato['vendor_id'];
			$affiliate_id = $dato['affiliate_id'];

			$this->Responsables_Model->asignaAfiliado2($vendor_id, $affiliate_id);
		}
		echo json_encode(['message' => 'Afiliados agregados']);
		#var_dump($data);
		#$this->Responsables_Model->asignaAfiliado($this->input->post());
	}

	public function asignaAfiliados($vendor)
	{

		$mis_afiliados     = $this->Responsables_Model->asignados($vendor);
		$data['afiliados'] = $this->Responsables_Model->afiliados_noAsignados($vendor);
		$data['vendedor']  = $vendor;


		//print_r($mis_afiliados);

		foreach ($mis_afiliados as $key => $afiliado) {
			foreach ($data['afiliados'] as $key2 => $value) {
				if ($value->id_affiliate == $afiliado->affiliate_id) {
					unset($data['afiliados'][$key2]);
				}
			}
		}
		
		//print_r($data['afiliados']) ."<br>";
		$this->load->view('pages/Responsables/asAfiliados', $data);
	}


	public function getAfiliados()
	{
		
		$data['afiliados'] = $this->Responsables_Model->getafiliados($vendor);
		$this->load->view('pages/Responsables/asAfiliados', $data);
	}

	function enableVendor ($vendor)
	{
		$this->Responsables_Model->enableVendor($vendor);
	}

	function disableVendor ($vendor)
	{
		$this->Responsables_Model->disableVendor($vendor);
	}

	function disable()
	{
		$vendor = $this->input->post('id_vendor');
		$this->Responsables_Model->disableResponsable($vendor);
	}


	public function index()
	{
		if ($_SESSION['type_user'] == 3) {
			redirect(base_url('Principal/error404'));
		}
			$data['title_page'] = "Responsables";
			$list['vendedores'] = $this->Responsables_Model->vendedores();
			$raside['solicitudes'] = $this->Principal_Model->solPend();
			$this->load->view('templates/styles', $data);
			$this->load->view('pages/Responsables/styles');
			$this->load->view('templates/header');
			$this->load->view('templates/aside');
			$this->load->view('pages/Responsables/listResponsables',$list);
			$this->load->view('templates/raside',$raside);
			$this->load->view('templates/footer');
			$this->load->view('templates/scripts');
			$this->load->view('pages/Responsables/scripts');
			$this->load->view('templates/endHtml');
		
	}

	function remove()
	{
		$this->Responsables_Model->removeAfiliado($this->input->post());
	}

	function tAfiliados($id)
	{
		$data['asignados'] = $this->Responsables_Model->asignados($id);
		$this->load->view('pages/Responsables/viewAsignados', $data);
	}

	function updateVendor($id)
	{
		$this->Responsables_Model->updateVendor($id,$this->input->post());
	}

	function verResponsable($id)
	{
		$data['vendedor'] = $this->Responsables_Model->vendedor($id);
		$data['asignados'] = $this->Responsables_Model->asignados($id);
		$data['totales'] = $this->Principal_Model->totalesVendor($id);
		$data['status'] = $this->Principal_Model->status();
		$this->load->view('pages/Responsables/viewResponsable', $data);
	}

	function habilitarAcceso ($id)
	{
		$this->Responsables_Model->habilitarAdmin($id);
	}

	function inhabilitarAcceso ($id)
	{
		$this->Responsables_Model->inhablilitarAdmin($id);
	}


	function misAfiliados ($vendedor_id = null)
	{
		if ($vendedor_id == null) {
			$vendedor_id =  $_SESSION['id_vendor'];
		}
		
		$data['title_page'] = "Principal - Responsables";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$dashboard['afiliados'] = $this->Afiliado_Model->misAfiliados($vendedor_id);
		$dashboard['status'] = $this->Principal_Model->status();
		$dashboard['totales'] = $this->Principal_Model->totalesVendor($vendedor_id);
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('templates/raside',$raside);
		$this->load->view('pages/dashboard',$dashboard);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

}

/* End of file Responsables.php */
/* Location: ./application/controllers/Responsables.php */