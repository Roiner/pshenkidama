<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
	}


	public function index()
	{
		if ($this->session->userdata('type_user') != 1) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Principal";
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/dashboard');
		$this->load->view('templates/raside');
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function error404 ()
	{
		$this->load->view('404');
	}

	public function reportarDudas()
	{
		if ($this->session->userdata('type_user') != 3) {
			redirect(base_url('Principal/error404'));
		}

		$this->load->view('pages/reporteDudas');
	}


	public function removeCache ()
	{
		$this->output->delete_cache();
	}

}

/* End of file Principal.php */
/* Location: ./application/controllers/Principal.php */