<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ayuda extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
		$this->load->model('Ayuda_Model');
		$this->load->library('encrypt');

	}

	public function index()
	{
		$data['title_page'] = "Ayuda";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$datos['normas'] = $this->Ayuda_Model->normas();
		$datos['faq'] = $this->Ayuda_Model->faq();
		$datos['sms'] = $this->Ayuda_Model->sms();
		$datos['resp'] = $this->Ayuda_Model->resp();
		$datos['audios'] = $this->Ayuda_Model->audios();
		$datos['videos'] = $this->Ayuda_Model->videos();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Ayuda/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Ayuda/dashboard',$datos);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Ayuda/scripts');
		$this->load->view('templates/endHtml');
	}


	public function delete()
	{
		$id    = $this->input->get($id);

		if ($this->Ayuda_Model->delete($id["id"], $id["tabla"], $id["col"])) {
			$data = array('message' => "operacion exitosa");
			echo json_encode($data);
		}
		

	}

	function saveAudio()
	{
		$datos = [
		    'title_audio' => $this->input->post('title_audio'),
			'folder_audio' => "files/ayuda/audios/".$_FILES['file_audio']['name']
		];

		$this->Ayuda_Model->saveAudio($datos);
		
		$config = array(
			'file_name' => $_FILES['file_audio']['name'],
			'upload_path' => "./files/ayuda/audios",
			'overwrite' => TRUE,
			'allowed_types' => 'wmv|mp4|avi|mov',
            'max_size' => '0'
		);

		$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file_audio')) {
                //if file upload failed then catch the errors
                $this->handle_error($this->upload->display_errors());
                $is_file_error = TRUE;
        	} else {
                //store the video file info
                $video_data = $this->upload->data();
            }

		$this->upload->do_upload('file_audio');

		echo json_encode(['message' => 'Audio agregado']);
		
	}

	/*function saveFileAudio()
	{
		/*foreach ($_FILES as $key) 
	 	{
	 		$name = $key['name'];
			$temp = $key['tmp_name']; 
			$local = "./files/ayuda/audios/".$name;	
			
			move_uploaded_file($temp, $local);
	 	}

		$config = array(
			'file_name' => $_FILES['image_activity']['name'],
			'upload_path' => "./files/ayuda/audios",
			'overwrite' => TRUE
		);

		$this->load->library('upload', $config);
		$this->upload->do_upload('file_audio');
	}*/

	function saveFaq()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$datos = array('title_faq' => $this->input->post('title_faq'), 'answer_faq' => $this->input->post('answer_faq'));
		$this->Ayuda_Model->saveFaq($datos);
	}

	function saveNorma()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$datos = array('title_rule' => $this->input->post('title_rule'), 'text_rule' => $this->input->post('text_rule'));
		$this->Ayuda_Model->saveNorma($datos);
	}

	function savePosResp()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$datos = array('title_posible_answer' => $this->input->post('title_posible_answer'), 'text_posible_answer' => $this->input->post('text_posible_answer'));
		$this->Ayuda_Model->savePosResp($datos);
	}

	function saveMensaje()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$datos = array('title_model_message' => $this->input->post('title_model_message'), 'content_model_message' => $this->input->post('content_model_message'));
		$this->Ayuda_Model->saveMensaje($datos);
	}

	public function viewAudio()
	{
		$this->load->view('pages/Ayuda/Tipos/audio2');
	}

	public function viewVideo()
	{
		$this->load->view('pages/Ayuda/Tipos/video');
	}

	public function viewSms()
	{
		$this->load->view('pages/Ayuda/Tipos/mensajes_text');
	}

	public function viewNormas()
	{
		$this->load->view('pages/Ayuda/Tipos/normas');
	}

	public function viewResp()
	{
		$this->load->view('pages/Ayuda/Tipos/posibles_resp');
	}

	public function viewFaq()
	{
		$this->load->view('pages/Ayuda/Tipos/faq');
	}


	public function saveMedia ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$this->load->library('encrypt');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: *');

		//echo json_encode($this->input->post('title'));
		
		$type_file = substr($_FILES["file"]['type'], 0, 5);
		
		if ($type_file == 'audio') {
			$target_dir = "./files/ayuda/audios/";
		} else {
			$target_dir = "./files/ayuda/videos/";
		}
		
		$target_file = $target_dir . basename($_FILES["file"]["name"]);

		$data = [
			'title'		=> $this->input->post('title'),
			'file_name' => $_FILES["file"]["name"],
			'path'		=> $target_file,
			'type_file'	=> $type_file
		];

		$this->Ayuda_Model->saveDataMedia($data);

		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_dir.$_FILES['file']['name'])) {
		 echo json_encode(['message' => 'Archivo cargado con éxito!']);
		}else{
			echo json_encode(['message' => 'Error al cargar archivo!']);
		}
	}

	public function newFile ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		
		$data['title_page'] = "Media";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Ayuda/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Ayuda/new');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Ayuda/scripts');
		$this->load->view('templates/endHtml');
	}

}

/* End of file Ayuda.php */
/* Location: ./application/controllers/Ayuda.php */