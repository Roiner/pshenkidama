<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends SuperController {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_Model');
		$this->load->model('Registro_Model');
	}


	public function index()
	{
		$this->session->sess_destroy();
		$data['title_page'] = "Iniciar Sesion";
		$data['mensaje'] = "";
		$data['user'] = "";
		$this->load->view('login',$data);
	}

	public function inSession()
	{
		$this->load->library('encrypt');
		$user = $this->input->post('user');
		$pass = $this->input->post('pass');

		$realuser = $this->Login_Model->verificaruser($user);

		if ($realuser == "valid") 
		{

			$realpass = $this->Login_Model->realpass($user);
			
			// if (sha1($pass) != $realpass) 
			// {
			// 	$data['title_page'] = "Iniciar Sesion";
			// 	$data['mensaje'] = "Contraseña Invalida";
			// 	$data['user'] = $user;
			// 	$this->load->view('login',$data);
			// } else {
				$privilegio = $this->Login_Model->privilegio($user);
				$this->Login_Model->insertUserConected(['id_user_conected' => $this->Login_Model->idUser($user)]);

				if ($privilegio == 1) 
				{
					$dataMaster = $this->Login_Model->masterData($user);
					$array = array(
						'username' => $user,
						'code_user' => $dataMaster['code_user'],
						'nombre' => $dataMaster['name_master'],
						'cargo' => $dataMaster['cargo_master'],
						'registro' => $dataMaster['date_register_master'],
						'type_user' => $privilegio,
						'id_user' => $this->Login_Model->idUser($user),
					);
					
					$this->session->set_userdata( $array );
					redirect(base_url('confirma_session'));
				}
				if ($privilegio == 2) 
				{
					$dataAdmin = $this->Login_Model->adminData($user);
					$array = array(
						'username' => $user,
						'id_admin' => $dataAdmin['id_admin'],
						'code_user' => $dataAdmin['code_user'],
						'nombre' => $dataAdmin['name_admin'],
						'phone' => $dataAdmin['phone_admin'],
						'email' => $dataAdmin['email_admin'],
						'registro' => $dataAdmin['date_register_admin'],
						'type_user' => $privilegio,
						'id_user' => $this->Login_Model->idUser($user),
					);
					
					$this->session->set_userdata( $array );
					redirect('confirma_session');
				}
				if ($privilegio == 3) 
				{
					$dataVendor = $this->Login_Model->vendorData($user);
					#echo json_encode($dataVendor);
					$array = array(
						'username' => $user,
						'id_vendor'	=>	$dataVendor['id_vendor'],
						'code_user' => $dataVendor['code_user'],
						'nombre' => $dataVendor['name_vendor'],
						'phone' => $dataVendor['phone_vendor'],
						'email' => $dataVendor['email_vendor'],
						'registro' => $dataVendor['date_register_vendor'],
						'type_user' => $privilegio,
						'id_user' => $this->Login_Model->idUser($user),
					);
					
					$this->session->set_userdata( $array );
					redirect('confirma_session');
				}
			// }

		} else {
			if ($realuser == "disabled") 
			{
				$data['title_page'] = "Iniciar Sesion";
				$data['mensaje'] = "Usuario Inactivo comunicate con el dpto de soporte";
				$data['user'] = "";
				$this->load->view('login',$data);
			}
			if ($realuser == "invalid") 
			{
				$data['title_page'] = "Iniciar Sesion";
				$data['mensaje'] = "Usuario Invalido";
				$data['user'] = "";
				$this->load->view('login',$data);
			}
			
		}
	}

	public function outSession()
	{
		$this->removeCache();
		$this->Login_Model->outSession();
		$this->session->sess_destroy();
		redirect('confirma_session');
	}


	public function facebook()
	{
        
        $this->load->library('FacebookSDK');
        #$this->config->load('facebook');

        $facebook  = new Facebook\Facebook(array('app_id' => "607023276302790",'app_secret' => "15ac84418b0e390aa3a40c307a6c17bf",'default_graph_version' => 'v2.9'));

        $helper = $facebook->getRedirectLoginHelper();

        $accessToken = $helper->getAccessToken(base_url('Login/facebook'));

        if ($accessToken) {
            try {
                //OBtener datos

                $facebook->setDefaultAccessToken($accessToken);

                $response = $facebook->get('/me');

                $userProfile = $response->getGraphUser();

                $responseEmail = $facebook->get('/me?fields=email');

                $userEmail = $responseEmail->getGraphUser();

                //Reemplazar variables

                $fb_id = $userProfile['id'];

                $name = $userProfile['name'];

                $email = (isset($userEmail['email']))?$userEmail['email']:'';

                $checkFb = $this->Login_Model->buscarUsuarioFb($fb_id);

                if($checkFb == 'invalid'){

                	/*INICIO REGISTRAR VENDEDOR*/

	                $checkUser = $this->encrypt->encode($email);
					$datos = array(
					    'name_vendor' => $name,
					    'email_vendor' => $email,
					    'dni_vendor' => '',
					    'age_vendor' => '',
					    'sex_vendor' => '',
					    'status_id' => 3,
					    'check_user_vendor' => $checkUser);
					$datosUser = array('name_user' => $name,
					    				'facebook_id' => $fb_id,
					    				'status_user' => 3,
					    				'type_user' => 3 );

					$this->Registro_Model->newVendor($datos,$datosUser,$checkUser);

					$data['mensaje'] = "Su solicitud de registro se recibio de manera exitosa, en el momento que sea aprobada nos encargaremos de avisarle.";
					$data['title_page'] = "Registro Exitoso";
					$this->load->view('pages/registroExitoso',$data);

	                /*FIN REGISTRAR VENDEDOR*/
                }elseif ($checkFb == 'disabled') {

                	$data['title_page'] = "Iniciar Sesion";
					$data['mensaje'] = "Usuario Inactivo comunicate con el dpto de soporte";
					$data['user'] = "";
					$this->load->view('login',$data);

                }else{

						$dataVendor = $this->Login_Model->vendorDataFb($name, $fb_id);
						#echo json_encode($dataVendor);
						$array = array(
							'username' => $name,
							'id_vendor'	=>	$dataVendor['id_vendor'],
							'code_user' => $dataVendor['code_user'],
							'nombre' => $dataVendor['name_vendor'],
							'phone' => $dataVendor['phone_vendor'],
							'email' => $dataVendor['email_vendor'],
							'registro' => $dataVendor['date_register_vendor'],
							'type_user' => 3
						);
						
						$this->session->set_userdata( $array );
						redirect('confirma_session');

                }

		//AQUI HACES PRINT PARA Q VEAS LO Q TE ENVIA FACEBOOK
		

            } catch (FacebookApiExeption $e) {
                $user = null;
            }
        } else {

            #die("<script>top.location='".$helper->getLoginUrl(base_url(). 'index/facebook')."'</script>");
            	
            die("<script>top.location='".$helper->getLoginUrl(base_url().'Login/facebook',array('scope' => 'email'))."'</script>");
        }
    }

    
	public function findVendor ($vendor_id){

		$dataUser = $this->Login_Model->findVendor($vendor_id);

		if (!empty($dataUser)) 
		{
			$user = $dataUser[0]->name_user;

			$dataVendor = $this->Login_Model->vendorData($user);

			$array = array(
				'username' => $user,
				'id_vendor'	=>	$dataVendor['id_vendor'],
				'code_user' => $dataVendor['code_user'],
				'nombre' => $dataVendor['name_vendor'],
				'phone' => $dataVendor['phone_vendor'],
				'email' => $dataVendor['email_vendor'],
				'registro' => $dataVendor['date_register_vendor'],
				'type_user' => 3
			);

			$array2 = array(
				'username' => '',
				'id_vendor'	=>	'',
				'code_user' => '',
				'nombre' => '',
				'phone' => '',
				'email' => '',
				'registro' => '',
				'type_user' => ''
			);
			$this->session->unset_userdata($array2);
			$this->session->set_userdata($array);
			
			echo json_encode(['code' =>200,'message' => 'Has iniciado sesión como: '. $array['username']]);
			
		} else{

			echo json_encode(['code' => 404, 'message' => 'No puedes ingresar, el usuario no ha completado el registro']);
		}

		
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */