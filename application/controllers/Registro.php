<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Registro_Model');
	}

	public function index()
	{
		$this->session->sess_destroy();
		$data['title_page'] = "Iniciar Sesion";
		$this->load->view('registro',$data);
	}

	function completeRegistroEspecial()
	{
		if (!$this->session->userdata('keyReg')) 
		{
			redirect(base_url());
		} else {
			$data['title_page'] = "Registro Master";
			$this->load->view('pages/registroEspecial/registroEspecial', $data);
		}
	}

	function especial()
	{	
		$data['title_page'] = "Check Password Master";
		$this->load->view('pages/registroEspecial/checkPass',$data);
	}

	function masterReg()
	{
		$check_user = $this->encrypt->encode($this->input->post('uname'));
		$passEnc = $this->encrypt->encode($this->input->post('pass'));
		$datosMaster = array('name_master' => $this->input->post('nombre'),
						'cargo_master' => $this->input->post('cargo'),
						'check_user' => $check_user);
		$datosUser = array('name_user' => $this->input->post('uname'),
						'pass_user' => $passEnc,
						'type_user' => 1,
						'status_user' => 1);
		$this->Registro_Model->newMaster($datosMaster,$datosUser,$check_user);
		redirect(base_url());
	}

	function validaCheck()
	{
		$check = $this->input->post('passcheck');
		$p = $this->Registro_Model->validaPassCheck();
		$passAct = $this->encrypt->decode($p);

		if ($passAct == $check) 
		{
			$array = array(
				'keyReg' => $p
			);
			
			$this->session->set_userdata( $array );
			echo "true";	

		} else {
			echo "false";
		}

	}

/*==========================Registro Vendedor===============================*/


	public function registroVendedor()
	{
		$this->load->library('encrypt');
		$checkUser = $this->encrypt->encode($this->input->post('email'));
		$datos = array(
		    'name_vendor' => $this->input->post('nombre')." ".$this->input->post('apellido'),
		    'email_vendor' => $this->input->post('email'),
		    'dni_vendor' => $this->input->post('dni'),
		    'age_vendor' => $this->input->post('edad'),
		    'sex_vendor' => $this->input->post('sexo'),
		    'status_id' => 3,
		    'check_user_vendor' => $checkUser);
		$datosUser = array('name_user' => $this->input->post('email'),
		    				'pass_user' => $this->encrypt->encode($this->input->post('password')),
		    				'status_user' => 3,
		    				'type_user' => 3 );

		$this->Registro_Model->newVendor($datos,$datosUser,$checkUser);

		$data['mensaje'] = "Su solicitud de registro se recibio de manera exitosa, en el momento que sea aprobada nos encargaremos de avisarle.";
		$data['title_page'] = "Registro Exitoso";
		$this->load->view('pages/registroExitoso',$data);
	}

/*==========================/Registro Vendedor===============================*/

}

/* End of file Registro.php */
/* Location: ./application/controllers/Registro.php */