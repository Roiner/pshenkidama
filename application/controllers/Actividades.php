<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividades extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
		$this->load->model('Actividad_Model');
		$this->load->model('Afiliado_Model');
		$this->load->model('Responsables_Model');
		
	}


	public function newActividad()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		$this->load->model('Categoria_Model');
		$data['title_page'] = "Nueva Actividad";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$dataPage['categories']= $this->Categoria_Model->getCategories();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Actividades/Nueva/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Nueva/newActividad',$dataPage);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('pages/Actividades/Nueva/scripts');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	function saveActividad()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		$this->load->model('Categoria_Model');
		$category_id = $this->input->post('category_id');
		$random = rand();

		if ($this->input->post('category_id') =='new' && $this->input->post('new_category')!==null) {
			$datos_categoria['name'] = $this->input->post('new_category');
			$category_id=$this->Categoria_Model->addCategoria($datos_categoria);
		}

		$datos = [
		    'name_activity' => $this->input->post('name_activity'),
			'description_activity' => $this->input->post('description_activity'),
			'mail_activity' => $this->input->post('mail_activity'),
			'date_activity' => $this->input->post('date_activity'),
			'hour_activity' => $this->input->post('hour_activity'),
			'address_activity' => $this->input->post('address_activity'),
			'age_activity ' => json_encode($this->input->post('age_activity')),
			'genre_activity' => $this->input->post('genre_activity'),
			'category_activity' => json_encode($this->input->post('category_activity')),
			'commune_activity' => json_encode($this->input->post('commune_activity')),
			'type_activity' => $this->input->post('type_activity'),
			'status_activity' => $this->input->post('status_activity'),
			'image_activity' => "files/actividad/imagenes/".$random.$_FILES['image_activity']['name'],
			'progress' => $this->input->post('progress'),
			'category_id' => $category_id,
		];


		$config = array(
			'file_name' => $random.	$_FILES['image_activity']['name'],
			'upload_path' => "./files/actividad/imagenes",
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			#'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			
		);
		$this->load->library('upload', $config);

		$this->upload->do_upload('image_activity');

		$this->Actividad_Model->saveActividad($datos);

		$this->session->set_flashdata('msg', 'Actividad Registrada');

		redirect(base_url("Actividades"));
	}

	function saveImageActivity()
	{	
		foreach ($_FILES as $key) 
	 	{
	 		$name = $key['name'];
			$temp = $key['tmp_name']; 
			$local = "<?=base_url()?>files/actividad/imagenes/".$name;	
			
			move_uploaded_file($temp, $local);
	 	}
	}


	public function verActividades()
	{
		$data['title_page'] = "Todas las Actividades";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$list['allActivities'] = $this->Actividad_Model->listActivities();
		$list['vendors'] = $this->Afiliado_Model->Responsables();
		$list['activityVendors'] = $this->Actividad_Model->activityVendors();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Todas/viewTodas',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function verActividadesCalendario()
	{
		if ($this->session->userdata('type_user') == 3) {
			$result = $this->Actividad_Model->getEvents2($this->session->userdata('id_vendor'));
		}else {
			$result = $this->Actividad_Model->getEvents();
		}
		
		echo json_encode($result);
	}

	public function verCalendarioActividades()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Calendario de Actividades";
		$data['activities'] =  $this->Actividad_Model->getEvents();
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Actividades/Calendario/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Calendario/viewCalendario', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Actividades/Calendario/scripts');
		$this->load->view('templates/endHtml');
	}

	public function verCalendarioActividades2()
	{
		$data['title_page'] = "Calendario de Actividades";
		$data['activities'] =   $this->Actividad_Model->getEvents2($this->session->userdata('id_vendor'));
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Actividades/Calendario/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Calendario/viewCalendario', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Actividades/Calendario/scripts');
		$this->load->view('templates/endHtml');
	}

	public function activityVendor($activity_id)
	{
		$data['responsables'] = $this->Afiliado_Model->Responsables();
		$data['actividad'] = $activity_id;
		$data['title_page'] = 'Asignar vendedores';
		$this->load->view('pages/Afiliados/asResponsable', $data);
	}

	public function addActivityVendor ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data = $this->input->post();
		$data['status'] = 1;
		$data['vista'] = 0;
		$asignar= $this->Actividad_Model->addActivityVendor($data);

		if ($asignar == true) {
			$subject = 'Asignación de Actividad';
			$message = 'Te hemos asignado una actividad, para mas información, revisa tu inbox en el CRM - Gestar';
			$vendor  = $this->Afiliado_Model->viewResp($data['id_vendor']);

			$this->sendEmail($vendor, $subject, $message);
		} 
	
	}

	public function showActivityVendor($activity_id)
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['responsables'] = $this->Actividad_Model->showVendors($activity_id);
		$data['delete'] = true;
		$data['actividad'] = $activity_id;
		$data['title_page'] = 'Responsables Asignados a la Actividad';
		$this->load->view('pages/Afiliados/asResponsable', $data);
	}

	public function quitActivityVendor ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data = $this->input->post();
		$this->Actividad_Model->quitVendor($data);
	}


	public function sendEmail ($vendor, $subject, $message)
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data = [
			'vendor'	=> 	$vendor[0]->name_vendor,
			'message'	=>	$message
		];

		$this->load->library('encrypt');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'gator4046.hostgator.com',
		    'smtp_port' => 465,
		    'smtp_crypto' => 'ssl',
		    'smtp_user' => 'hola@henkidama.com',
		    'smtp_pass' => '39UWy9T~HtxF',
		    'mailtype'  => 'html', 
		    'charset'   => 'utf-8',
		    'newline' 	=> "\r\n",
			'crlf' 		=> "\r\n",
			'wordwrap'  => TRUE
		);
		$this->load->library('email', $config);
		
		$this->email->to($vendor[0]->email_vendor);
		$this->email->from('hola@henkidama.com', 'CRM - Gestar');
		$this->email->subject($subject);
		$this->email->message($this->load->view('emails/notify', $data, true));
		$this->email->send();
		
		/*if($this->email->send()) {
			echo json_encode(['message' => 'Mensaje Enviado!']);
		}else{
			echo json_encode(['message' => 'Fallo al Enviar Email']);
		}*/

	}

	public function index()
	{

		$data['title_page'] = "Todas las Actividades";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$list['allActivities'] = $this->Actividad_Model->listActivities();
		$list['vendors'] = $this->Afiliado_Model->Responsables();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Todas/index',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function delete($id)
	{
		if ($this->Actividad_Model->delete($id)) {
			
		}
		$data = array('message' => "operacion exitosa");
		echo json_encode($data);
	}
	public function aprobarVendedores()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$data['title_page'] = "Actividades";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$list['allActivities'] = $this->Actividad_Model->listActivitiesVendor();
		$list['vendors'] = $this->Afiliado_Model->Responsables();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Todas/indexAprobar',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function actividadesAsignadas()
	{
		if ($this->session->userdata('type_user') != 3) {
			redirect(base_url('Principal/error404'));
		}
		
		$data['title_page'] = "Todas las Actividades";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$list['allActivities'] = $this->Actividad_Model->listActivitities2($this->session->userdata('id_vendor'));
		$list['vendors'] = $this->Afiliado_Model->Responsables();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Todas/index',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function inscribir ()
	{
		$data = $this->input->post();
		$data['status'] = 0;
		$this->Actividad_Model->addActivityVendor($data);
	}


	public function aprobeActivityVendor ()
	{
		$data = $this->input->post();
		$this->Actividad_Model->aprobeActivityVendor($data);
	}

	public function editActividad($activity_id)
	{
		$this->load->model('Categoria_Model');
		$data['title_page'] = "Editar Actividad";
		$data['activity'] = $this->Actividad_Model->activity($activity_id);
		$data['categories']= $this->Categoria_Model->getCategories();
		$this->load->view('pages/Actividades/Todas/editActividad', $data);
	}

	public function updateActividad ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$id_activity = $this->input->post('id');

		$data = [
		    'name_activity' => $this->input->post('name_activity'),
			'description_activity' => $this->input->post('description_activity'),
			'mail_activity' => $this->input->post('mail_activity'),
			'date_activity' => $this->input->post('date_activity'),
			'hour_activity' => $this->input->post('hour_activity'),
			'address_activity' => $this->input->post('address_activity'),
			'age_activity ' => json_encode($this->input->post('age_activity')),
			'genre_activity' => $this->input->post('genre_activity'),
			'category_activity' => json_encode($this->input->post('category_activity')),
			'commune_activity' => json_encode($this->input->post('commune_activity')),
			'type_activity' => $this->input->post('type_activity'),
			'status_activity' => $this->input->post('status_activity'),
			'progress' => $this->input->post('progress'),
			'category_id' => $this->input->post('category_id'),

			
			#'image_activity' => "files/actividad/imagenes/".$random.$_FILES['image_activity']['name']
		];

		if (!empty($_FILES['image_activity']['name'])) {
			$random = rand();
			$data['image_activity'] = "files/actividad/imagenes/".$random.$_FILES['image_activity']['name'];
		}

		$datalog =[
			'username' => $_SESSION['username'],
			'id_activity' => $id_activity,
		];
		

		$this->Actividad_Model->updateActividad($id_activity, $data);
		$this->Actividad_Model->addLogActividad($datalog);

		$config = array(
			'file_name' => $random.	$_FILES['image_activity']['name'],
			'upload_path' => "./files/actividad/imagenes",
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
		#	'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			
		);
		$this->load->library('upload', $config);

		$this->upload->do_upload('image_activity');

		$this->session->set_flashdata('msg', 'Actividad Editada');

		redirect(base_url("Actividades"));

	}

	public function showActividad($activity_id)
	{
		
		$data['title_page'] = "Ver Actividad";
		$data['activity'] = $this->Actividad_Model->activity($activity_id);

		// Setear Vista en BD//
		$datos = ['id_activity' => $activity_id, 'id_vendor' => $this->session->userdata('id_vendor')];
		$this->Actividad_Model->vistaActivityVendor($datos);

		$this->load->view('pages/Actividades/Todas/show', $data);
	}

	public function logActividad($activity_id)
	{
		$data['title_page'] = "Ver Responsables";
		$data['responsables'] = $this->Actividad_Model->logActivity($activity_id);
		
		$this->load->view('pages/Actividades/Todas/log', $data);
	}


	public function showActividad2($activity_id)
	{
		
		$data['title_page'] = "Ver Actividad";
		$data['activity'] = $this->Actividad_Model->activity($activity_id);
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$raside['solicitudes'] = $this->Principal_Model->solPend();

		// Setear Vista en BD//
		$datos = ['id_activity' => $activity_id, 'id_vendor' => $this->session->userdata('id_vendor')];
		$this->Actividad_Model->vistaActivityVendor($datos);

		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Todas/show', $data);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function probarCalendario ()
	{
		echo json_encode($this->Actividad_Model->getEvents2());
	}

	public function culminarTarea($id_activity)
	{
		$datalog =[
			'username' => $_SESSION['username'],
			'id_activity' => $id_activity,
		];

		$data = [
			'progress' => 100,
		];
		

		$this->Actividad_Model->updateActividad($id_activity, $data);
		$this->Actividad_Model->addLogActividad($datalog);

		$this->session->set_flashdata('msg', 'Actividad Editada');

		redirect(base_url("Actividades"));
	}


}

/* End of file Actividades.php */
/* Location: ./application/controllers/Actividades.php */