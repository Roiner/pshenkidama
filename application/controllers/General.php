<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('General_Model');
		$this->load->model('Principal_Model');
		$this->load->model('Actividad_Model');
	}

	public function viewResp()
	{
		$id = $this->input->post('id_resp');
		print_r(json_encode($this->General_Model->readResp($id)));
	}

	public function refuseResp()
	{
		$this->General_Model->refuseResp($this->input->post('id_resp'));
	}

	public function acceptResp()
	{
		$this->General_Model->acceptResp($this->input->post('id_resp'));
	}

	public function notificaciones()
	{
		$data['notificaciones'] = $this->Principal_Model->listActivitiesVendor($this->session->userdata('id_vendor'));
		$this->load->view('templates/header', $data);
	}

	public function savePass()
	{
	
		$datos = [
			'pass_user' => 	$this->encrypt->encode($this->input->post('password')),
			'username'	=> 	$this->input->post('user'),
			'olduser'	=> 	$this->session->userdata('username'),
			'email'		=>	$this->input->post('email'),
			'type_user'	=>	$this->session->userdata('type_user'),
			'code_user'	=>	$this->session->userdata('code_user')
		];

		$this->General_Model->savePass($datos);
	}


}

/* End of file General.php */
/* Location: ./application/controllers/General.php */