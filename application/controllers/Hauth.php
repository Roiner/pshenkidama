<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Hauth Controller Class
 */
class Hauth extends CI_Controller {
  /**
   * {@inheritdoc}
   */
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->library('hybridauth');
    $this->load->model('Login_Model');
    $this->load->model('Registro_Model');
  }
  /**
   * {@inheritdoc}
   */
  public function index()
  {
    // Build a list of enabled providers.
    $providers = array('Facebook', 'Twitter');
    foreach ($this->hybridauth->HA->getProviders() as $provider_id => $params)
    {
      $providers[] = anchor("hauth/window/{$provider_id}", $provider_id);
    }
    $this->load->view('hauth/login_widget', array(
      'providers' => $providers,
    ));
  }
  /**
   * Try to authenticate the user with a given provider
   *
   * @param string $provider_id Define provider to login
   */
  public function window($provider_id)
  {
    $params = array(
      'hauth_return_to' => site_url("hauth/window/{$provider_id}"),
    );
    if (isset($_REQUEST['openid_identifier']))
    {
      $params['openid_identifier'] = $_REQUEST['openid_identifier'];
    }
    try
    {
      $adapter = $this->hybridauth->HA->authenticate($provider_id, $params);
      $profile = $adapter->getUserProfile();

      #echo json_encode($profile);

      /*$this->load->view('hauth/done', array(
        'profile' => $profile,
      ));*/
      $tw_id = $profile->identifier;
      $name  = $profile->firstName;
      $email = $profile->email; 

      
      $checkTw = $this->Login_Model->buscarUsuarioTw($tw_id);

          if($checkTw == 'invalid'){

          $checkUser = $this->encrypt->encode($tw_id);
          $datos = array(
              'name_vendor' => $name,
              'email_vendor' => $email,
              'dni_vendor' => '',
              'age_vendor' => '',
              'sex_vendor' => '',
              'status_id' => 3,
              'check_user_vendor' => $checkUser);
          $datosUser = array('name_user' => $name,
                      'twitter_id' => $tw_id,
                      'status_user' => 3,
                      'type_user' => 3 );

          $this->Registro_Model->newVendor($datos,$datosUser,$checkUser);

          $data['mensaje'] = "Su solicitud de registro se recibio de manera exitosa, en el momento que sea aprobada nos encargaremos de avisarle.";
          $data['title_page'] = "Registro Exitoso";
          $this->load->view('pages/registroExitoso',$data);

          }elseif ($checkTw == 'disabled') {

          $data['title_page'] = "Iniciar Sesion";
          $data['mensaje'] = "Usuario Inactivo comunicate con el dpto de soporte";
          $data['user'] = "";
          $this->load->view('login',$data);

          }else{

            $dataVendor = $this->Login_Model->vendorDataTw($name, $fb_id);
            #echo json_encode($dataVendor);
            $array = array(
              'username' => $name,
              'id_vendor' =>  $dataVendor['id_vendor'],
              'code_user' => $dataVendor['code_user'],
              'nombre' => $dataVendor['name_vendor'],
              'phone' => $dataVendor['phone_vendor'],
              'email' => $dataVendor['email_vendor'],
              'registro' => $dataVendor['date_register_vendor'],
              'type_user' => 3
            );
            
            $this->session->set_userdata( $array );
            redirect('confirma_session');

          }
    }

    catch (Exception $e)
    {
      show_error($e->getMessage());
    }
  }
  /**
   * Handle the OpenID and OAuth endpoint
   */
  public function endpoint()
  {
    $this->hybridauth->process();
  }
}