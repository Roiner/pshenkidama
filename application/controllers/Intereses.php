<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intereses extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
		$this->load->model('Interes_Model');
		$this->load->model('Afiliado_Model');
	}


	public function newInteres()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}
		$this->load->model('Categoria_Model');
		$data['title_page'] = "Nueva Interes";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Intereses/Todas/newInteres');
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	function saveInteres()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$datos = [
		    'name' => $this->input->post('name'),
		];

		$this->Interes_Model->saveInteres($datos);

		$this->session->set_flashdata('msg', 'Interes Registrado');

		redirect(base_url("Intereses"));
	}

	public function index()
	{

		$data['title_page'] = "Todos los Intereses";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$list['allInterest'] = $this->Interes_Model->listInterest();
		$list['vendors'] = $this->Afiliado_Model->Responsables();
		$list['allInterestUser'] = $this->Interes_Model->listInterestUser();

		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Intereses/Todas/index',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function delete($id)
	{
		$this->Interes_Model->delete(['id' => $id]);
		$data = array('message' => "operacion exitosa");
		echo json_encode($data);
	}
	

	public function editInteres($interest_id)
	{
		$data['title_page'] = "Editar Interes";
		$data['interest'] = $this->Interes_Model->interest($interest_id);
		$this->load->view('pages/Intereses/Todas/editInteres', $data);
	}

	public function updateInteres ()
	{
		if ($this->session->userdata('type_user') == 3) {
			redirect(base_url('Principal/error404'));
		}

		$id_interest = $this->input->post('id');

		$data = [
		    'name' => $this->input->post('name'),
			#'image_activity' => "files/actividad/imagenes/".$random.$_FILES['image_activity']['name']
		];	

		$this->Interes_Model->updateInterest($id_interest, $data);

		$this->session->set_flashdata('msg', 'Interes Editada');

		redirect(base_url("Intereses"));

	}

	public function listInterestChecked($id)
	{
		$interests=$this->Interes_Model->listInterest();
		$interests_affiliates=$this->Interes_Model->arrayAffiliateAsociate($this->Interes_Model->listffilatesInteres($id));
		$html = "";
		foreach ($interests as $key => $interest) {
			if (in_array($interest->id, $interests_affiliates)) {
				$html.= <<<EOT
				<label><input type="checkbox" class="checkbox-inline check-interests" value="{$interest->id}" checked>{$interest->name}</label><br>
EOT;
			}else{
				$html.= <<<EOT
				<label><input type="checkbox" class="checkbox-inline check-interests" value="{$interest->id}">{$interest->name}</label><br>
EOT;
			}
		}
		echo json_encode(['interests' => $interests, 'html' => $html]);
	}
	


}

/* End of file Actividades.php */
/* Location: ./application/controllers/Actividades.php */