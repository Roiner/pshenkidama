<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}


	public function index()
	{
		$this->load->library('encrypt');
		$data['title_page'] = "Ingresar";
		$this->load->view('templates/styles', $data);
		$this->load->view('welcome');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}
}
