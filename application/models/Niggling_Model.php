<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niggling_Model extends CI_Model {

	
	
	function lista()
	{
		$this->db->select('*');
		$this->db->from('nigglins');
		$this->db->join('vendors', 'vendors.id_vendor = nigglins.id_vendor');
		$this->db->order_by('date', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	function listVendor($vendor_id)
	{
		$this->db->select('*');
		$this->db->from('nigglins');
		$this->db->join('vendors', 'vendors.id_vendor = nigglins.id_vendor');
		$this->db->where('nigglins.id_vendor', $vendor_id);
		$this->db->order_by('date', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	function saveData($data)
	{
		$res = $this->db->insert('nigglins', $data);
		
		if ($res == true) {
			echo json_encode(['type' => 'success', 'message' => 'Registro exitoso!']);	
		}else{
			echo json_encode(['type' => 'error',  'message' => 'Error al registrar']);
		}
	}

	function find($id)
	{
		$this->db->select('*');
		$this->db->from('nigglins');
		$this->db->join('vendors', 'vendors.id_vendor = nigglins.id_vendor');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$query = $query->result();
		echo json_encode(['result' => $query]);
	}

	function saveResponse ($data, $id)
	{
		$this->db->select('*');
		$this->db->from('nigglins');
		$this->db->where('id', $id);
		$res = $this->db->update('nigglins', $data);

		if ($res == true) {
			echo json_encode(['type' => 'success', 'message' => 'Registro exitoso!']);	
		}else{
			echo json_encode(['type' => 'error',  'message' => 'Error al registrar']);
		}
	}

	function delete ($id)
	{
		$res = $this->db->delete('nigglins', array('id' => $id));

		if ($res == true) {
			echo json_encode(['type' => 'success', 'message' => 'Registro eliminado!']);	
		}else{
			echo json_encode(['type' => 'error',  'message' => 'Error al eliminar']);
		}
	}

}

/* End of file Niggling_Model.php */
/* Location: ./application/models/Niggling_Model.php */