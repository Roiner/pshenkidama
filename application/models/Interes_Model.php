<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interes_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	function listInterest()
	{
		$this->db->select('*');
		$this->db->from('interests');
		$this->db->join('users', 'users.id_user = interests.id_user','left');
		$this->db->where('interests.id_user IS NULL');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	function listInterestUser()
	{
		$this->db->select('*');
		$this->db->from('interests');
		$this->db->join('users', 'users.id_user = interests.id_user','left');
		$this->db->where('interests.id_user IS NOT NULL');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete($datos)
	{
		$this->db->where($datos);
		$this->db->delete('interests');
	}


	function saveInteres($datos)
	{	
		$this->db->insert('interests', $datos);
		/*if ($this->db->insert('activities', $datos)) {
			echo json_encode(['status' => 'success', 'message' => 'Actividad Registrada!']);
		} else {
			echo json_encode(['status' => 'success', 'message' => 'Error, intenta nuevamente!']);
		}*/
		return $this->db->insert_id();
		
	}

	function interest($id)
	{
		$this->db->select('*');
		$this->db->from('interests');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return ($query->result());
	}

	function showActivity($id)
	{
		$this->db->select('name_activity');
		$this->db->from('activities');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return ($query->result());
	}


	function updateInterest ($id_interest, $data)
	{
		$this->db->select('*');
		$this->db->from('interests');
		$this->db->where('id', $id_interest);

		$this->db->update('interests', $data);
		
		#echo json_encode(['status' => 'success', 'message' => 'Actividad actualizada!']);
	}

	public function searchAffilatesInteres($id_affiliate,$id_interest)
	{
		$this->db->select('*');
		$this->db->from('affiliates_interests');
		$this->db->where('id_interest', $id_interest);
		$this->db->where('id_affiliate', $id_affiliate);
		$query = $this->db->get();
		return ($query->result());
	}

	public function addAffilatesInteres($datos)
	{
		$this->db->insert('affiliates_interests', $datos);
	}
	
	public function deleteAffilatesInteres($datos)
	{
		$this->db->where($datos);
		$this->db->delete('affiliates_interests');
	}

	public function listffilatesInteres($id_affiliate)
	{
		$this->db->select('*');
		$this->db->from('affiliates_interests');
		$this->db->where('id_affiliate', $id_affiliate);
		$query = $this->db->get();
		return ($query->result());
	}

	public function arrayAffiliateAsociate($obj)
	{
		$array =array();
		foreach ($obj as $key => $value) {
			if (isset($value->id_interest)) {
				$array[]= $value->id_interest;
			}
			if (isset($value->id)) {
				$array[]= $value->id;	
			}
		}
		return $array;
	}	

}

/* End of file Actividad_Model.php */
/* Location: ./application/models/Actividad_Model.php */

