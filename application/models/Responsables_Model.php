<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Responsables_Model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
	}

	public function afiliados_noAsignados()
	{
		$this->db->select('*');
		$this->db->from('affiliates');
		//$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate', 'left');
		//$this->db->where('affiliate_vendor.id_affiliate_vendor IS NULL');
		$query = $this->db->get();
		return $query->result();
	}


	public function getafiliados()
	{
		$this->db->select('*');
		$this->db->from('affiliates');
		$query = $this->db->get();
		return $query->result();
	}

	function asignaAfiliado($datos)
	{
		$this->db->insert('affiliate_vendor', $datos);
	}

	function asignaAfiliado2($vendor_id, $affiliate_id)
	{	

		$this->db->select('*');
		$this->db->from('affiliate_vendor');
		$this->db->where('vendor_id', $vendor_id);
		$this->db->where('affiliate_id', $affiliate_id);
		$query = $this->db->get();
		$r = $query->result();

		if (empty($r)) {
			$datos = ['vendor_id' => $vendor_id, 'affiliate_id' => $affiliate_id];
			$this->db->insert('affiliate_vendor', $datos);
		}


		
	}

	public function asignados($id)
	{
		$this->db->select('*');
		$this->db->from('affiliate_vendor');
		$this->db->join('affiliates', 'affiliates.id_affiliate = affiliate_vendor.affiliate_id', 'inner');
		$this->db->where('vendor_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
//SELECT * FROM `vendors` JOIN `check_users` ON `check_users`.`code_user` = `vendors`.`check_user_vendor` JOIN `users` ON `users`.`id_user` = `check_users`.`user_id` WHERE `id_vendor` = 11
//
	function enableVendor($vendor)
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->where('id_vendor', $vendor);
		$this->db->join('check_users', 'check_users.code_user = vendors.check_user_vendor', 'inner');
		$this->db->join('users', 'users.id_user = check_users.user_id', 'inner');
		
		$query = $this->db->get();
		$q = $query->result();

		$this->habilitar($q[0]->user_id, ['status_user' => 1]);
		$this->updateVendor($q[0]->id_vendor, ['status_id' => 1]);
		
		echo json_encode(['status' => 'success', 'message' => 'Responsable Habilitado!']);
	}



	function disableVendor($vendor)
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->where('id_vendor', $vendor);
		$this->db->join('check_users', 'check_users.code_user = vendors.check_user_vendor', 'inner');
		$this->db->join('users', 'users.id_user = check_users.user_id', 'inner');
	
		$query = $this->db->get();
		$q = $query->result();

		$this->habilitar($q[0]->user_id, ['status_user' => 2]);
		$this->updateVendor($q[0]->id_vendor, ['status_id' => 2]);
		
		echo json_encode(['status' => 'success', 'message' => 'Responsable Inhabilitado!']);
	}


	function disableResponsable($id)
	{
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', array('status_id' => 2));
	}

	function removeAfiliado($datos)
	{
		$this->db->where($datos);
		$this->db->delete('affiliate_vendor');
	}

	function updateVendor($id,$datos)
	{
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', $datos);
	}

	function vendedores()
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->join('status', 'status.id_status = vendors.status_id', 'left');
		$this->db->join('sessions', 'sessions.session_check_user = vendors.check_user_vendor', 'left');
		$this->db->where('status_id !=', 3);
		$this->db->where('status_id !=', 0);
		$query = $this->db->get();
		return $query->result();
	}


	public function delete($id)
	{
		$data = array('status_id' => 0);
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', $data);
	}



	function vendedor($id)
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->join('status', 'status.id_status = vendors.status_id', 'left');
		$this->db->join('sessions', 'sessions.session_check_user = vendors.check_user_vendor', 'left');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function hablilitarAdmin($id)
	{
		$this->db->select('*');
		$this->db->from('administrators');
		$this->db->where('id_admin', $id);
		$this->db->join('check_users', 'check_users.code_user = administrators.check_user_admin', 'inner');
		$this->db->join('users', 'users.id_user = check_users.user_id', 'inner');
		
		$query = $this->db->get();
		$q = $query->result();

		$this->habilitar($q[0]->user_id, ['status_user' => 1]);
		
		echo json_encode(['status' => 'success', 'message' => 'Administrador Habilitado!']);
	}

	function inhablilitarAdmin($id)
	{
		$this->db->select('*');
		$this->db->from('administrators');
		$this->db->where('id_admin', $id);
		$this->db->join('check_users', 'check_users.code_user = administrators.check_user_admin', 'inner');
		$this->db->join('users', 'users.id_user = check_users.user_id', 'inner');
		
		$query = $this->db->get();
		$q = $query->result();

		#echo json_encode($q);
		$this->habilitar($q[0]->user_id, ['status_user' => 2]);
		
		echo json_encode(['status' => 'success', 'message' => 'Administrador Inhabilitado!']);
	}


	function habilitar ($id, $data)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id_user', $id);
		
		$this->db->update('users', $data);
		
	}

}

/* End of file Responsables_Model.php */
/* Location: ./application/models/Responsables_Model.php */