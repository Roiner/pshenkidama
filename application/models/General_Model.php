<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function acceptResp($id)
	{
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', array('status_id' => 1));

		$this->db->select('id_user');
		$this->db->from('users');
		$this->db->join('check_users', 'check_users.user_id = users.id_user', 'inner');
		$this->db->join('vendors', 'vendors.check_user_vendor = check_users.code_user', 'inner');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		$id_user = $query->row('id_user');

		$this->db->where('id_user', $id_user);
		$this->db->update('users', array('status_user' => 1));		
	}

	function inSession()
	{
		$this->db->select('*');
		$this->db->from('sessions');
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$query = $this->db->get()->num_rows();

		if ($query == 1) 
		{
			$this->db->where('session_check_user', $this->session->userdata('code_user'));
			$this->db->update('sessions', array('in_session' => date("Y-m-d H:i:s")));			
			$this->db->insert('sessions_hist', array('in_session' => date("Y-m-d H:i:s"), 'session_check_user' => $this->session->userdata('code_user')));
		} else {
			$this->db->insert('sessions', array('in_session' => date("Y-m-d H:i:s"), 'session_check_user' => $this->session->userdata('code_user')));
			$this->db->insert('sessions_hist', array('in_session' => date("Y-m-d H:i:s"), 'session_check_user' => $this->session->userdata('code_user')));
		}



	}

	public function readResp($id)
	{
		$this->db->select('name_vendor,email_vendor,date_register_vendor');
		$this->db->from('vendors');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function refuseResp($id)
	{
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', array('status_id' => 99));

		$this->db->select('id_user');
		$this->db->from('users');
		$this->db->join('check_users', 'check_users.user_id = users.id_user', 'inner');
		$this->db->join('vendors', 'vendors.check_user_vendor = check_users.code_user', 'inner');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		$id_user = $query->row('id_user');

		$this->db->where('id_user', $id_user);
		$this->db->update('users', array('status_user' => 99));		
	}

	public function savePass($datos)
	{
		$this->db->where('name_user', $datos['olduser']);
		$this->db->update('users', [
			'name_user'	=> 	$datos['username'],
			'pass_user' =>	$datos['pass_user']
		]);

		if ($datos['type_user'] == 2) {
			$this->db->where('check_user_admin', $datos['code_user']);
			$this->db->update('administrators', ['email_admin' => $datos['email']]);
		} elseif ($datos['type_user'] == 3) {
			$this->db->where('check_user_vendor', $datos['code_user']);
			$this->db->update('vendors', ['email_vendor' => $datos['email']]);
		}

		$this->session->sess_destroy();
	}

}

/* End of file General_Model.php */
/* Location: ./application/models/General_Model.php */