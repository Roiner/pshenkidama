<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function afiliados()
	{
		$this->db->select('affiliates.*,name_status,color_status');
		$this->db->from('affiliates');
		$this->db->join('status', 'status.id_status = affiliates.status_affiliate', 'inner');
		$this->db->where('status_affiliate != ', 2);
		$query = $this->db->get();
		return $query->result();
	}


	public function search_asosc_addres($id, $street, $number)
	{
		$this->db->select('affiliates.names_affiliate');
		$this->db->from('affiliates');
		$this->db->where('street_affiliate', $street);
		$this->db->where('number_affiliate', $number);
		$this->db->where('id_affiliate !=', $id);
		$query = $this->db->get();
		return  $query->result();


	}

	public function solPend()
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->where('status_id', 3);
		$query = $this->db->get();
		return $query->result();
	}

	function totales()
	{
		$this->db->select('COUNT(*) as NC');
		$this->db->where('status_affiliate', 11);
		$this->db->from('affiliates');
		$nc = $this->db->get()->row('NC');

		$this->db->select('COUNT(*) as C');
		$this->db->where('status_affiliate', 6);
		$this->db->from('affiliates');
		$c = $this->db->get()->row('C');

		$this->db->select('COUNT(*) as PV');
		$this->db->where('status_affiliate', 4);
		$this->db->from('affiliates');
		$pv = $this->db->get()->row('PV');

		$this->db->select('COUNT(*) as PC');
		$this->db->where('status_affiliate', 5);
		$this->db->from('affiliates');
		$pc = $this->db->get()->row('PC');

		$this->db->select('COUNT(*) as VC');
		$this->db->where('status_affiliate', 7);
		$this->db->from('affiliates');
		$vc = $this->db->get()->row('VC');

		$this->db->select('COUNT(*) as CC');
		$this->db->where('status_affiliate', 8);
		$this->db->from('affiliates');
		$cc = $this->db->get()->row('CC');

		$this->db->select('COUNT(*) as VCO');
		$this->db->where('status_affiliate', 9);
		$this->db->from('affiliates');
		$vco = $this->db->get()->row('VCO');

		$this->db->select('COUNT(*) as CO');
		$this->db->where('status_affiliate', 10);
		$this->db->from('affiliates');
		$co = $this->db->get()->row('CO');

		$this->db->select('COUNT(*) as TOT');
		$this->db->from('affiliates');
		$tot = $this->db->get()->row('TOT');

		return array('PV' => $pv,
					'PC' => $pc,
					'VC' => $vc,
					'CC' => $cc,
					'VCO' => $vco,
					'CO' => $co,
					'C' => $c,
					'NC' => $nc,
					'TOT' => $tot);
	}

	function totalesVendor($vendor_id)
	{

		$this->db->select('COUNT(*) as NC');
		$this->db->where('status_affiliate', 11);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$nc = $this->db->get()->row('NC');

		$this->db->select('COUNT(*) as C');
		$this->db->where('status_affiliate', 6);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$c = $this->db->get()->row('C');

		$this->db->select('COUNT(*) as PV');
		$this->db->where('status_affiliate', 4);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$pv = $this->db->get()->row('PV');

		$this->db->select('COUNT(*) as PC');
		$this->db->where('status_affiliate', 5);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$pc = $this->db->get()->row('PC');

		$this->db->select('COUNT(*) as VC');
		$this->db->where('status_affiliate', 7);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$vc = $this->db->get()->row('VC');

		$this->db->select('COUNT(*) as CC');
		$this->db->where('status_affiliate', 8);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$cc = $this->db->get()->row('CC');

		$this->db->select('COUNT(*) as VCO');
		$this->db->where('status_affiliate', 9);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$vco = $this->db->get()->row('VCO');

		$this->db->select('COUNT(*) as CO');
		$this->db->where('status_affiliate', 10);
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$co = $this->db->get()->row('CO');

		$this->db->select('COUNT(*) as TOT');
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->from('affiliates');
		$tot = $this->db->get()->row('TOT');

		return array('PV' => $pv,
					'PC' => $pc,
					'VC' => $vc,
					'CC' => $cc,
					'VCO' => $vco,
					'CO' => $co,
					'C' => $c,
					'NC' => $nc,
					'TOT' => $tot);
	}

	function listActivitiesVendor($vendor_id)
	{
		$this->db->select('id, name_activity, date_activity, image_activity');
		$this->db->from('activities');
		$this->db->join('activity_vendor', 'activity_vendor.id_activity = activities.id');
		$this->db->order_by('id', 'desc');
		$this->db->where('activity_vendor.vista', 0);
		$this->db->where('activity_vendor.id_vendor', $vendor_id);
		$query = $this->db->get();
		return $query->result();
	}

	function status ()
	{
		$this->db->select('*');
		$this->db->from('status');
		$query = $this->db->get();
		return $query->result();
	}

}

/* End of file Principal_Model.php */
/* Location: ./application/models/Principal_Model.php */