<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function adminData($user)
	{
		$this->db->select('*');
		#'name_admin,phone_admin,date_register_admin,code_user,email_admin','user_id', 'id_admin'
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('administrators', 'administrators.check_user_admin = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row_array();
	}

	function masterData($user)
	{
		$this->db->select('name_master,cargo_master,date_register_master,code_user');
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('master', 'master.check_user = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row_array();
	}

	function vendorData($user)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('vendors', 'vendors.check_user_vendor = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row_array();
	}

	function outSession()
	{
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$this->db->update('sessions', array('out_session' => date("Y-m-d H:i:s")));
		$this->db->select('in_session');
		$this->db->from('sessions');
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$query = $this->db->get()->row('in_session');

		$this->db->where('in_session', $query);
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$this->db->update('sessions_hist', array('out_session' => date("Y-m-d H:i:s")));

		$this->deleteUserConected(['id_user_conected' => $_SESSION['id_user']]);
	}

	function privilegio($user)
	{
		$this->db->select('type_user');
		$this->db->from('users');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row('type_user');
	}

	function realpass($user)
	{
		$this->db->select('pass_user');
		$this->db->from('users');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row('pass_user');
	}

	public function verificaruser($user)
	{
		$this->db->select('name_user,status_user');
		$this->db->where('name_user',$user);
		$this->db->from('users');
		$query = $this->db->get();

		if ($query->num_rows() == 1) 
		{
			if ($query->row('status_user') == 1) 
			{
				return "valid";
			} else {
				return "disabled";
			}
		}
		else{
			return "invalid";
		}
	}

	public function buscarUsuarioFb($fb_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('facebook_id', $fb_id);

		$query = $this->db->get();

		if ($query->num_rows() == 1) 
		{
			if ($query->row('status_user') == 1) 
			{
				return "valid";
			} else {
				return "disabled";
			}
		}
		else{
			return "invalid";
		}
	}

	function vendorDataFb($user, $fb_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('vendors', 'vendors.check_user_vendor = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$this->db->where('facebook_id', $fb_id);
		$query = $this->db->get();
		return $query->row_array();
	}


	public function buscarUsuarioTw($tw_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('twitter_id', $tw_id);

		$query = $this->db->get();

		if ($query->num_rows() == 1) 
		{
			if ($query->row('status_user') == 1) 
			{
				return "valid";
			} else {
				return "disabled";
			}
		}
		else{
			return "invalid";
		}
	}


	function vendorDataTw($user, $tw_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('vendors', 'vendors.check_user_vendor = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$this->db->where('twitter_id', $fb_id);
		$query = $this->db->get();
		return $query->row_array();
	}

	function findVendor($vendor_id)
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->join('check_users', 'check_users.code_user = vendors.check_user_vendor', 'inner');
		$this->db->join('users', 'users.id_user = check_users.user_id', 'inner');

		$this->db->where('id_vendor', $vendor_id);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function idUser($user)
	{
		$this->db->select('id_user');
		$this->db->where('name_user',$user);
		$this->db->from('users');
		$query = $this->db->get();

		if ($query->num_rows() == 1) 
		{
			return $query->row('id_user');
		}
		else{
			return "invalid";
		}
	}

	public function allUsers()
	{
		$this->db->select('*');
		$this->db->from('users');
		$query = $this->db->get();
		return $query->result();
	}

	public function GetUserData()

	{  

 		$this->db->select('*');

		$this->db->from('users');

		$this->db->where("id_user",$_SESSION['id_user']);

		$this->db->limit(1);

  		$query = $this->db->get();

 		if ($query) {

			 return $query->row_array();

		 } else {

			 return false;

		 }

   	}

   	public function GetName($id)

	{  

 		$this->db->select('id_user,name_user');

		$this->db->from('users');

		$this->db->where("id_user",$id);

		$this->db->limit(1);

  		$query = $this->db->get();

		$res = $query->row_array();

 		return $res['name_user'];
   	}

   	public function allUsersConected()
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('users_conected', 'users_conected.id_user_conected = users.id_user', 'left');
		$this->db->where('users_conected.id_user_conected IS NOT NULL');
		$this->db->where('users_conected.id_user_conected !=', $_SESSION['id_user']);
		$query = $this->db->get();
		return $query->result();
	}

	public function allUsersNotConected()
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('users_conected', 'users_conected.id_user_conected = users.id_user', 'left');
		$this->db->where('users_conected.id_user_conected IS NULL');
		$this->db->where('users.id_user !=', $_SESSION['id_user']);
		$query = $this->db->get();
		return $query->result();
	}

	function insertUserConected($datos)
	{
		$this->db->insert('users_conected', $datos);
	}

	function deleteUserConected($datos)
	{
		$this->db->where($datos);
		$this->db->delete('users_conected');
	}



}

/* End of file Login_Model.php */
/* Location: ./application/models/Login_Model.php */