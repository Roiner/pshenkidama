<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function newMaster($master,$user,$checkUser)
	{
		$this->db->insert('master', $master);
		$this->db->insert('users', $user);
		
		$insert_id = $this->db->insert_id();
		$datos = ['user_id' => $insert_id, 'code_user' => $checkUser];
		$this->db->insert('check_users', $datos);
	}

	public function newVendor($vendor,$user,$checkUser)
	{
		$this->db->insert('vendors', $vendor);
		$this->db->insert('users', $user);
		
		$insert_id = $this->db->insert_id();
		$datos = ['user_id' => $insert_id, 'code_user' => $checkUser];
		$this->db->insert('check_users', $datos);
	}

	

	function validaPassCheck()
	{
		$this->db->select('pass_special_check');
		$this->db->from('specialcheck');
		$this->db->where('status_id', 1);
		$query = $this->db->get();
		return $query->row('pass_special_check');
	}

}

/* End of file Registro_Model.php */
/* Location: ./application/models/Registro_Model.php */