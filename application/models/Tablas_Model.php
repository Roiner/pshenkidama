<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tablas_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function administrador($id)
	{
		$this->db->select('*');
		$this->db->from('administrators');
		$this->db->join('check_users', 'administrators.check_user_admin = check_users.code_user', 'inner');
		$this->db->join('users', 'check_users.user_id = users.id_user', 'inner');
		$this->db->where('id_admin', $id);
		$query = $this->db->get();
		return $query->result();
	}


	function disableAdmin($id)
	{
		$this->db->where('id_admin', $id);
		$this->db->update('administrators', array('status_id' => 2));
	}

	function enableAdmin($id)
	{
		$this->db->where('id_admin', $id);
		$this->db->update('administrators', array('status_id' => 1));
	}

	public function saveAdmin($datosAdmin,$datosUser,$checkUser)
	{
		$this->db->insert('administrators', $datosAdmin);
		$this->db->insert('users', $datosUser);

		$insert_id = $this->db->insert_id();
		$datos = ['user_id' => $insert_id, 'code_user' => $checkUser];
		$this->db->insert('check_users', $datos);
	}

	function saveStatus($datos)
	{
		$this->db->insert('status', $datos);
	}

	function sesiones()
	{
		$this->db->select('*');
		$this->db->from('sessions');
		$this->db->join('administrators', 'administrators.check_user_admin = sessions.session_check_user', 'inner');
		$query = $this->db->get();
		return $query->result();
	}

	function status()
	{
		$this->db->select('*');
		$this->db->from('status');
		$query = $this->db->get();
		return $query->result();
	}

	function updateAdmin($id,$datosAdmin,$datosUser,$user)
	{
		$this->db->where('id_admin', $id);
		$this->db->update('administrators', $datosAdmin);

		$this->db->where('name_user', $user);
		$this->db->update('users', $datosUser);
	}

	function administradores()
	{
		$this->db->select('*');
		$this->db->from('administrators');
		$this->db->join('check_users', 'administrators.check_user_admin = check_users.code_user', 'inner');
		$this->db->join('users', 'check_users.user_id = users.id_user', 'inner');
		$this->db->join('status', 'status.id_status = administrators.status_id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	function vendedores()
	{	
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('type_user', 3);
		$query = $this->db->get();
		return $query->result();
	}
}

/* End of file Tablas_Model.php */
/* Location: ./application/models/Tablas_Model.php */