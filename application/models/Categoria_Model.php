<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function getCategories()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$query = $this->db->get();
		return $query->result();

	}

	public function addCategoria($datos)
	{
		$this->db->insert('categories', $datos);
		$id = $this->db->insert_id();
		return $id;
	}

}

/* End of file Afiliado_Model.php */
/* Location: ./application/models/Afiliado_Model.php */