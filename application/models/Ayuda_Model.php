<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ayuda_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function delete($id, $tabla, $col)
	{	
		$this->db->where($col, $id);
		return $this->db->delete($tabla);
	}

	function faq()
	{
		$this->db->select('*');
		$this->db->from('faq');
		$query = $this->db->get();
		return $query->result();
	}

	function normas()
	{
		$this->db->select('*');
		$this->db->from('rules');
		$query = $this->db->get();
		return $query->result();
	}

	function sms()
	{
		$this->db->select('*');
		$this->db->from('model_messages');
		$query = $this->db->get();
		return $query->result();
	}

	function resp()
	{
		$this->db->select('*');
		$this->db->from('posible_answer');
		$query = $this->db->get();
		return $query->result();
	}

	function saveAudio($datos)
	{
		$this->db->insert('audio', $datos);
	}

	function saveFaq($datos)
	{
		$this->db->insert('faq', $datos);
	}

	function saveNorma($datos)
	{
		$this->db->insert('rules', $datos);
	}

	function savePosResp($datos)
	{
		$this->db->insert('posible_answer', $datos);
	}

	function saveMensaje($datos)
	{
		$this->db->insert('model_messages', $datos);
	}

	function saveDataMedia($data)
	{
		$this->db->insert('medias', $data);
	}

	function audios ()
	{
		$this->db->select('*');
		$this->db->from('medias');
		$this->db->where('type_file', 'audio');
		$query = $this->db->get();
		return $query->result();
	}


	function videos ()
	{
		$this->db->select('*');
		$this->db->from('medias');
		$this->db->where('type_file', 'video');
		$query = $this->db->get();
		return $query->result();
	}

}

/* End of file Ayuda_Model.php */
/* Location: ./application/models/Ayuda_Model.php */