<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Afiliado_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function addResponsable($datos)
	{
		$this->db->select('*');
		$this->db->from('affiliate_vendor');
		$this->db->where('affiliate_id', $datos['affiliate_id']);
		$this->db->where('vendor_id', $datos['vendor_id']);
		$query = $this->db->get();
		$r = $query->result();
				
		if (empty($r)) {
			$this->db->insert('affiliate_vendor', $datos);
			
			echo json_encode(['status' => 'success', 'message' => 'Responsable agregado!', 'vendor' => $this->viewResp($datos['vendor_id'])]);
			
		}else{
			echo json_encode(['status' => 'error', 'message' => 'El Responsable ya existe!']);
		}

	}


	function addResponsable2($datos)
	{
		$this->db->insert('affiliate_vendor', $datos);
	}

	function afiliado($id)
	{
		$this->db->select('*');
		$this->db->from('affiliates');
		$this->db->where('id_affiliate', $id);
		#$this->db->join('afiliate_vendor', 'afiliate_id');
		#$this->db->join('status', 'status.id_status = affiliates.status_afiliate', 'inner');
		#$this->db->join('status', 'status.id_status = affiliates.id_vendor');
		#$this->db->join('status', 'status.id_status = affiliates.status_affiliate', 'inner');
		#$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate', 'left');
		#$this->db->join('vendors', 'vendors.id_vendor = affiliate_vendor.vendor_id', 'left');
		#$this->db->join('contacts_affiliates', 'contacts_affiliates.affiliate_id = affiliates.id_affiliate', 'left'); ESTE HACE QUE SE REPITA EL MODAL
		
		$query = $this->db->get();
		return $query->result();
	}


	function vendor($id)
	{
		$this->db->select('*');
		$this->db->from('affiliate_vendor');
		$this->db->where('affiliate_id', $id);
		#$sql = "SELECT * FROM `affiliate_vendor` JOIN `vendors` ON `vendors`.`id_vendor` = `affiliate_vendor`.`vendor_id` WHERE `affiliate_id` = 562";
		//$sql = "SELECT * FROM `affiliate_vendor` JOIN `vendors` ON `vendors`.`id_vendor` = `affiliate_vendor`.`vendor_id` WHERE `affiliate_id` = 562;";
		#$this->db->select($sql);
		//
		$this->db->join('vendors', 'vendors.id_vendor = affiliate_vendor.vendor_id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	function disableAfiliado($id)
	{
		$this->db->where('id_affiliate', $id);
		$this->db->update('affiliates', array('status_affiliate' => 2));
	}

	function insertContactAffiliate($datos)
	{
		$this->db->insert('contacts_affiliates', $datos);
	}

	function removeResponsable($datos)
	{
		$this->db->where($datos);
		$this->db->delete('affiliate_vendor');
	}

	function Responsables()
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->where('status_id', 1);
		#$this->db->join('affiliate_vendor', 'vendors.id_vendor = affiliate_vendor.vendor_id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	function Responsables2($responsables)
	{
		$this->db->select('id_vendor, name_vendor');
		$this->db->from('vendors');
		$this->db->where('status_id', 1);
		$this->db->where_not_in('id_vendor', $responsables);
		$query = $this->db->get();
		return $query->result();
	}


	function saveAfiliado($datos)
	{
		$this->db->insert('affiliates', $datos);
	}

	function status()
	{
		$this->db->where('id_status != 1');
		$this->db->where('id_status != 2');
		$this->db->where('id_status != 3');
		$this->db->select('*');
		$this->db->from('status');
		$query = $this->db->get();
		return $query->result();
	}

	function tContacto()
	{
		$this->db->select('*');
		$this->db->from('contacts_types');
		$query = $this->db->get();
		return $query->result();
	}

	function updateDataAffiliate($id_affiliate,$datos)
	{
		$this->db->where('id_affiliate', $id_affiliate);
		$this->db->update('affiliates', $datos);
	}

	function viewResp($id)
	{
		$this->db->select('name_vendor,email_vendor,dni_vendor');
		$this->db->from('vendors');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function searchtAffilates($dni)
	{
		$this->db->where('dni_affiliate',$dni);
	    $query = $this->db->get('affiliates');
	    if ($query->num_rows() > 0)
	    {
	        return true;
	    }
	    else{
	        return false;
	    }
	}

	function contacts ($afiliado)
	{
		$this->db->select('*');
		$this->db->from('contacts_affiliates');
		$this->db->join('contacts_types', 'contacts_types.id_contacts_type = contacts_affiliates.contact_id');
		$this->db->where('affiliate_id', $afiliado);
		$this->db->order_by("date_contact_affiliate", "DESC");

		$query = $this->db->get();
		return $query->result();
	}


	function misAfiliados ($vendor_id)
	{
		$this->db->select('affiliates.*, name_status,color_status');
		$this->db->from('affiliates');
		$this->db->join('status', 'status.id_status = affiliates.status_affiliate', 'inner');
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$query = $this->db->get();
		return $query->result();
	}

	function misAfiliados2 ($vendor_id)
	{
		$this->db->select('id_affiliate,commune_affiliate,gener_affiliate,dni_affiliate,names_affiliate,last_names_affiliate,email_affiliate,status_affiliate,age_affiliate,name_status,color_status');
		$this->db->from('affiliates');
		$this->db->join('status', 'status.id_status = affiliates.status_affiliate', 'inner');
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate');
		$this->db->where('status_affiliate != ', 2);
		$this->db->where('vendor_id', $vendor_id);
		$this->db->where('email_affiliate != ', '');
		$query = $this->db->get();
		return $query->result();
	}

	function afiliadosMail()
	{
		$this->db->select('names_affiliate, last_names_affiliate, email_affiliate');
		$this->db->from('affiliates');
		$this->db->where('affiliates.email_affiliate !=', '');
		$this->db->order_by('names_affiliate', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function vendedoresMail()
	{
		$this->db->select('name_vendor, email_vendor');
		$this->db->from('vendors');
		$this->db->order_by('name_vendor', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function afiliadosAsociadosConAfiliado($id,$associates)
	{
		$this->db->select('*');
		$this->db->from('affiliates');
		$this->db->where_not_in('id_affiliate', $associates);
				
		$query = $this->db->get();
		return $query->result();
	}

	public function addAfiliadoAsociado($datos)
	{
		$this->db->insert('affiliates_associates', $datos);
	}

	function afiliadosAsociados($id)
	{
		$this->db->select('*');
		$this->db->from('affiliates_associates');
		$this->db->join('affiliates', 'affiliates.id_affiliate = affiliates_associates.id_affiliate_associate');
		$this->db->where('affiliates_associates.id_affiliate', $id);
				
		$query = $this->db->get();
		return $query->result();
	}

	public function eliminarAsociacion($datos)
	{
		$this->db->where($datos);
		$this->db->delete('affiliates_associates');
	}

}

/* End of file Afiliado_Model.php */
/* Location: ./application/models/Afiliado_Model.php */