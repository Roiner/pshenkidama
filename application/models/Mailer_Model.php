<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer_Model extends CI_Model {

	
	
	function inbox($vendor_id)
	{
		$this->db->select('*');
		$this->db->from('inboxes');
		$this->db->join('mailers', 'mailers.id = inboxes.mailer_id');
		$this->db->join('activities', 'activities.id = mailers.activity_id', 'inner' );
		$this->db->join('vendors', 'vendors.id_vendor = inboxes.vendor_id');
		$this->db->order_by('date', 'DESC');
		$this->db->where('vendor_id', $vendor_id);
		$query = $this->db->get();
		return $query->result();
	}

	function saveData($data)
	{
		$res = $this->db->insert('mailers', $data);
		return $this->db->insert_id();
	}

	function createInbox($data)
	{
		$this->db->insert('inboxes', $data);
	}

	function outbox($id_admin)
	{
		$this->db->select('*');
		$this->db->from('mailers');
		$this->db->join('activities', 'activities.id = mailers.activity_id', 'inner' );
		$this->db->order_by('mailers.id', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

}

/* End of file Mailer_Model.php */
/* Location: ./application/models/Mailer_Model.php */