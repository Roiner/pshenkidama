<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<style>
	.fileDiv {
  position: relative;
  overflow: hidden;
}
.upload_attachmentfile {
  position: absolute;
  opacity: 0;
  right: 0;
  top: 0;
}
.btnFileOpen {margin-top: -50px; }

.direct-chat-warning .right>.direct-chat-text {
    background: #d2d6de;
    border-color: #d2d6de;
    color: #444;
	text-align: right;
}
.direct-chat-primary .right>.direct-chat-text {
    background: #3c8dbc;
    border-color: #3c8dbc;
    color: #fff;
	text-align: right;
}
.spiner{}
.spiner .fa-spin { font-size:24px;}
.attachmentImgCls{ width:450px; margin-left: -25px; cursor:pointer; }
</style>
 
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Nuevo Interes</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="#">Intereses</li>
                            <li class="active">Nueva Interes</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="col-md-8" id="chatSection">
                                          <!-- DIRECT CHAT -->
                                          <div class="box box-warning direct-chat direct-chat-primary">
                                            <div class="box-header with-border">
                                              <h3 class="box-title" id="ReciverName_txt"><?=$chatTitle;?></h3>

                                              <div class="box-tools pull-right">
                                                <span data-toggle="tooltip" title="Clear Chat" class="ClearChat"><i class="fa fa-comments"></i></span>
                                                <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>-->
                                               <!-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Clear Chat"
                                                        data-widget="chat-pane-toggle">
                                                  <i class="fa fa-comments"></i></button>-->
                                               <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                                </button>-->
                                              </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                              <!-- Conversations are loaded here -->
                                              <div class="direct-chat-messages" id="content" style="height: 480px;">
                                                 <!-- /.direct-chat-msg -->

                                                 <div id="dumppy"></div>

                                              </div>
                                              <!--/.direct-chat-messages-->
                             
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer">
                                              <!--<form action="#" method="post">-->
                                                <div class="input-group">
                                                 <?php
                                                    $obj=&get_instance();
                                                    $obj->load->model('Login_Model');
                                                    $profile_url = '$obj->UserModel->PictureUrl()';
                                                    $user=$obj->Login_Model->GetUserData();
                                                ?>
                                                    
                                                    <input type="hidden" id="Sender_Name" value="<?=$user['name_user'];?>">
                                                    <!-- <input type="hidden" id="Sender_ProfilePic" value="<?=$profile_url;?>"> -->
                                                    
                                                    <input type="hidden" id="ReciverId_txt">
                                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control message">
                                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                                      <div class="btn-group mr-2" role="group" aria-label="First group">
                                                        <button type="button" class="btn btn-success btn-flat btnSend" id="nav_down">Send</button>
                                                        <div class="fileDiv btn btn-info btn-flat"> <i class="fa fa-upload"></i> 
                                                        <input type="file" name="file" class="upload_attachmentfile"/></div>
                                                      </div>
                                                    </div>
                                                </div>
                                              <!--</form>-->
                                            </div>
                                            <!-- /.box-footer-->
                                          </div>
                                          <!--/.direct-chat -->
                                        </div>




                                        <div class="col-md-4">
                                          <!-- USERS LIST -->
                                          <div class="box box-danger">
                                            <div class="box-header with-border">
                                              <h3 class="box-title"><?=$strTitle;?></h3>

                                              <div class="box-tools pull-right">
                                                <span class="label label-success"><?=count($users_conected);?> <?=$strsubTitle;?> Conectados</span>
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                                </button>
                                              </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body no-padding">
                                              <table class="table" id="myTable">
                                                <thead>
                                                  <tr>
                                                    <th>Usuarios</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <?php if(!empty($users_conected)){
                                                    foreach($users_conected as $v):
                                                    ?>
                                                    <tr>
                                                      <td>
                                                        <li class="selectVendor" id="<?=$v['id'];?>" title="<?=$v['name'];?>" style="list-style:none">
                                                          <a class="users-list-name text-success" href="#"><?=$v['name'];?> (En Linea) </a>
                                                        </li>
                                                      </td>
                                                    </tr>
                                                <?php endforeach;?>
                                                <?php } ?>
                                                <?php if(!empty($users_not_conected)){
                                                    foreach($users_not_conected as $v):
                                                    ?>
                                                    <tr>
                                                      <td>
                                                        <li class="selectVendor" id="<?=$v['id'];?>" title="<?=$v['name'];?>" style="list-style:none">
                                                          <a class="users-list-name text-danger" href="#"><?=$v['name'];?></a>
                                                        </li>
                                                      </td>
                                                    </tr>
                                                <?php endforeach;?>
                                                <?php } ?>
                                                </tbody>
                                              </table>
                                              <!-- /.users-list -->
                                            </div>
                                            <!-- /.box-body -->
                                           <!-- <div class="box-footer text-center">
                                              <a href="javascript:void(0)" class="uppercase">View All Users</a>
                                            </div>-->
                                            <!-- /.box-footer -->
                                          </div>
                                          <!--/.box -->
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
<!-- Jquery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>


<!-- /.content-wrapper --> 

<!-- Modal -->
<div class="modal fade" id="myModalImg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="modelTitle">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <img id="modalImgs" src="uploads/attachment/21_preview.png" class="img-thumbnail" alt="Cinque Terre">
        </div>
        
        <!-- Modal footer -->
         
        
      </div>
    </div>
  </div>
<!-- Modal -->
  
<script src="<?=base_url('js/chat.js');?>"></script>

<script>
    
    $(document).ready(function() {
        $('#myTable').DataTable({
          "lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
        });
    });
   
</script>
 