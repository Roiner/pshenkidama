<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url()?>plugins/images/favicon.png">
    <title>Página no encontrada</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url()?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?= base_url()?>css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url()?>css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url()?>css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
   
</head>

<body>
    <!--<div class="container-fluid">
        <section id="wrapper" class="error-page">
            <div class="error-box">
                <div class="error-body text-center">
                    <h1>404</h1>
                    <h3 class="text-uppercase">Página no encontrada !</h3>
                    <p class="text-muted m-t-30 m-b-30">Los sentimos, la página que buscas no existe.</p>
                    <a href="<?= base_url()?>" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Inicio</a> </div>
                <footer class="footer text-center"><?= date('Y')?> © Gestar.</footer>
            </div>
        </section>
    </div>-->
    
    <div class="container-fluid">
        <div class="jumbotron">
            <div style="text-align:center">
                <h1>Error 404</h1>
                <hr>
                <h3>Página no encontrada.</h3>
                 <br>
                <a href="<?= base_url()?>" style="margin-right: 10px;">
                    <i style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px; font-weight: bold;">Volver a Inicio</i>
                </a>
            </div>
            
        </div>
    </div>
    
    <!-- jQuery -->
    <script src="<?= base_url()?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url()?>bootstrap/dist/js/tether.min.js"></script>
    <script src="<?= base_url()?>bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url()?>js/custom.min.js"></script>
</body>

</html>
