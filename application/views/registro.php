<!DOCTYPE html>  
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>CRM PS - <?= $title_page ?></title>
  <!-- Bootstrap Core CSS -->
  <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
  <!-- animation CSS -->
  <link href="<?= base_url() ?>css/animate.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="<?= base_url() ?>css/style_old.css" rel="stylesheet">
  <!-- color CSS -->
  <link href="<?= base_url() ?>css/colors/purple.css" id="theme"  rel="stylesheet">

  <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.css">

  <link rel="stylesheet" href="<?= base_url() ?>css/sty.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar " style="right: auto;">
    <div class="white-box slimscrollsidebar">
      <form class="form-horizontal form-material" method="POST" id="loginform" action="<?= base_url('registroVendedor') ?>"style="padding: 29px; margin-top: 10%;">
        <a href="javascript:void(0)" class="text-center db">
          <img src="<?= base_url() ?>images/logo_dark.png" alt="Home" style="height: 80px;" >
        <h3 class="box-title m-t-40 m-b-0">Registro</h3>
        <div class="form-group m-t-20">
          <div class="col-xs-12">
            <input class="form-control" type="text" required placeholder="Nombre" name="nombre">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required placeholder="Apellido" name="apellido">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="email" required placeholder="Email" name="email">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required placeholder="DNI" name="dni">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="number" required placeholder="Edad" name="edad">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <select name="sexo" id="sexo" class="form-control">
              <option value="">Genero</option>
              <option value="Masculino">Masculino</option>
              <option value="Femenino">Femenino</option>
            </select>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required id="pass" placeholder="Password" name="password">
            <span id="msg" style="color: red;"></span>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required id="vpass" placeholder="Confirma Password" onkeyup="validaPass()">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Acepto todos los <a href="#">terminos</a></label>
            </div>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Registrarme</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Ya tienes una Cuenta? <a href="<?= base_url('login') ?>" class="text-primary m-l-5"><b>Ingresa</b></a></p>
          </div>
        </div>
      </form>
    </div>
  </div>

<div id="particle-canvas" style="z-index: -9 !important; "></div>



<div class="logo">
                     <img src="images/logo-white.png" alt="Home" width="90" height="85">
                  </div>


</section>
<!-- jQuery -->
  <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?= base_url() ?>js/tether.min.js"></script>
  <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
  <!-- Menu Plugin JavaScript -->
  <script src="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

  <!--slimscroll JavaScript -->
  <script src="<?= base_url() ?>js/jquery.slimscroll.js"></script>
  <!--Wave Effects -->
  <script src="<?= base_url() ?>js/waves.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?= base_url() ?>js/custom.min.js"></script>
  <!--Style Switcher -->
  <script src="<?= base_url() ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
  <script>
    function  validaPass() 
    {
      var pass = $("#pass").val();
      var pass1 = $("#vpass").val();
      if (pass1 != pass) 
      {
        $("#msg").html("Las Contraseñas no Coinciden");
        document.getElementById('but').disabled = true;
      } else {
        $("#msg").html("");
        document.getElementById('but').disabled = false;
      }
    }
  </script>

<style>
#particle-canvas {
  width: 100%;
  height: 100%;
}

</style>

</body>
</html>
