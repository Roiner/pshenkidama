    </head>

    <body style="background-color: white;">
      <div class="container-fluid">
                <div class="page-header">
                    
                    <h1 class="text-center" style="margin-top: 20px"> Bienvenido </h1>
                </div>
                <div class="row" style="margin-top: 50px;">
                    
                    <div class="white-box">
                    <div class="col-md-12">
                            

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card">
                                        <img class="card-img-top image-responsive" src="<?= base_url();?>images/logo_dark2.png" alt="PS">
                                        <div class="card-block">
                                            <h4 class="card-title">CRM PS</h4>
                                            <p class="card-text text-justify">CRM PS, Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere ipsa nulla ratione, illo voluptas culpa laudantium dolores aut. Quam dolorum, iste, quibusdam maiores repellat animi in architecto qui. A, repellendus.</p>
                                            <a href="<?= base_url("login");?>" class="btn btn-primary">Ingresar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <img class="card-img-top image-responsive" src="<?= base_url();?>plugins/images/cards/2.jpg" alt="Card image cap">
                                        <div class="card-block">
                                            <h4 class="card-title">Card title</h4>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            <a href="#" class="btn btn-primary">Go somewhere</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <img class="card-img-top image-responsive" src="<?= base_url();?>plugins/images/cards/3.jpg" alt="Card image cap">
                                        <div class="card-block">
                                            <h4 class="card-title">Card title</h4>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            <a href="#" class="btn btn-primary">Go somewhere</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
            