<?php echo anchor('hauth/window/Facebook', 'Facebook');?>

<?php echo anchor('hauth/window/Twitter', 'Twitter');?>

<div class="hybridauth-widget-wrapper">
  <ul class="hybridauth-widget">
    <?php foreach ($providers as $provider): ?>
      <li><?php print $provider; ?></li>
    <?php endforeach; ?>
  </ul>
</div>