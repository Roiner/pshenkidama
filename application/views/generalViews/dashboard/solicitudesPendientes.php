








			<h5 class="box-title"><b>Solicitudes Pendientes</b></h5>
            <ul class="basic-list">
                <?php foreach ($solicitudes as $key): ?>
                <li><small class="text-success">
                        <?= date_format(date_create($key->date_register_vendor),'d-m-Y') ?>
                    </small>
                    <br>
                    <?= $key->name_vendor ?>                                         
                    <a href="#" title="Aceptar" onclick="aprobarResp(<?= $key->id_vendor ?>)"><span class="pull-right label-success label" style="letter-spacing: 0;padding: 4px 9px;"><i class="fa fa-check"></i></span></a>
                    <a href="#" title="Rechazar" onclick="rechazarResp(<?= $key->id_vendor ?>)"><span class="pull-right label-danger label" style="letter-spacing: 0;padding: 4px 9px;"><i class="fa fa-times"></i></span></a>
                </li>
                <?php endforeach ?>
            </ul>
            

           
            