<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CRM PS - <?= $title_page ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
    
    <link href="<?= base_url() ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?= base_url() ?>js/jPlayer/jplayer.flat.css" type="text/css" />

    <link href="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    
    <link href="<?= base_url() ?>plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?= base_url() ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?= base_url() ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
    <!-- animation CSS -->
    <link href="<?= base_url() ?>css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>css/style_old.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url() ?>css/colors/megna-dark.css" id="theme" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.css">

    <link rel="stylesheet" href="<?= base_url()?>css/select2.min.css">

     <link rel="stylesheet" href="<?= base_url()?>/plugins/bower_components/dropify/dist/css/dropify.min.css">


    <link href="<?= base_url()?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url()?>plugins/bower_components/dropzone-master/dist/basic.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?= base_url() ?>css/progress-bar.css">


    <!-- jQuery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
    