
    <?php if($_SESSION['type_user'] == 3) : ?>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="<?= base_url() ?>images/user.png" alt="user-img" class="img-circle"> <span class="hide-menu"><?= $this->session->userdata('nombre') ?><span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?= base_url('outSession') ?>"><i class="fa fa-power-off"></i> Cerrar Sesion</a></li>
                        </ul>
                    </li>
                    
                    <li> <a href="<?= base_url('misAfiliados') ?>" class="waves-effect"><i class="fa fa-users p-r-10"></i> <span class="hide-menu"> Afiliados</span></a> </li>
                    
                   
                    <li> <a href="<?= base_url('mailer') ?>" class="waves-effect"><i class="ti-envelope p-r-10"></i> <span class="hide-menu"> Mails</span></a> </li>
                    
                    <li> <a href="#" class="waves-effect"><i class="fa fa-clipboard p-r-10"></i> <span class="hide-menu"> Actividades<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?= base_url('actividadesAsignadas') ?>">Mis Actividades</a> </li>
                            <li> <a href="<?= base_url('calendarActivitiesVendor') ?>">Calendario</a> </li>
                        </ul>
                    </li>

                    <li> <a href="<?= base_url('helpSystem') ?>" class="waves-effect"><i class="ti-info-alt p-r-10"></i> <span class="hide-menu">Ayuda</span></a> </li>
            
                    
                    <li><a href="#modalReportar" data-toggle="modal" data-target="#modalReportar" onclick="cargar('#allModalReportar','<?= base_url('reporteDudas') ?>');"><i class="fa fa-pencil-square-o" ></i> <span class="hide-menu">Reportar Dudas</span></a></li>


                    <li><a href="<?= base_url('dudasVendedor')?>"><i class="fa fa-list" ></i> <span class="hide-menu">Dudas </span></a></li>
                    
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
   
    <?php else:?>
        
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="<?= base_url() ?>images/user.png" alt="user-img" class="img-circle"> <span class="hide-menu"><?= $this->session->userdata('nombre') ?><span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?= base_url('outSession') ?>"><i class="fa fa-power-off"></i> Cerrar Sesion</a></li>
                        </ul>
                    </li>
                    <li> <a href="<?= base_url() ?>" class="waves-effect"><i class="fa fa-dashboard p-r-10"></i> <span class="hide-menu">  Panel Escritorio</span></a></li>
                    
                    <li> <a href="<?= base_url('newAfiliado') ?>" class="waves-effect"><i class="ti-plus p-r-10"></i> <span class="hide-menu"> Nuevo Afiliado</span></a> </li>
                    
                    <!--<li> <a href="<?= base_url('listResponsables') ?>" class="waves-effect"><i class="ti-desktop p-r-10"></i> <span class="hide-menu"> Responsables</span></a> </li>-->

                    <li> <a href="<?= base_url('composeMail') ?>" class="waves-effect"><i class="ti-envelope p-r-10"></i> <span class="hide-menu"> Mails</span></a> </li>
                    
                    <li> <a href="#" class="waves-effect"><i class="fa fa-clipboard p-r-10"></i> <span class="hide-menu"> Actividades<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?= base_url('newActividad') ?>"> Añadir Nueva</a> </li>
                            <li> <a href="<?= base_url('Actividades') ?>">Todas</a> </li>
                            <li> <a href="<?= base_url('calendarActivities') ?>">Calendario</a> </li>
                        </ul>
                    </li>

                    <li> <a href="#" class="waves-effect"><i class="fa fa-clipboard p-r-10"></i> <span class="hide-menu"> Intereses<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?= base_url('newInteres') ?>"> Añadir Nueva</a> </li>
                            <li> <a href="<?= base_url('Intereses') ?>">Todas</a> </li>
                        </ul>
                    </li>

                    <li> <a href="<?= base_url('helpSystem') ?>" class="waves-effect"><i class="ti-info-alt p-r-10"></i> <span class="hide-menu">Ayuda</span></a> </li>
                    
                    <li> <a href="#" class="waves-effect"><i class="fa fa-users p-r-10"></i> <span class="hide-menu"> Usuarios <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?= base_url('administradores') ?>">Administradores</a> </li>
                            <li> <a href="<?= base_url('vendedores') ?>">Responsables</a> </li>
                        </ul>
                    </li>

                    <li><a href="<?= base_url('dudas')?>"><i class="fa fa-list" ></i> <span class="hide-menu">Dudas Recibidas</span></a></li>
                    
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">

    <?php endif; ?>