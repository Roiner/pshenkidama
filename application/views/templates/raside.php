                
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Opciones <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <form method="post" onsubmit="savePass();return false;" id="act_pass_user">
                                <ul>
                                    
                                    <li><b>Cambiar mis datos</b></li>
                                    <li>
                                        <div class="form-group">
                                            <label for="user"> Usuario </label>
                                            <input id="text" type="user" class="form-control"  name="user" value="<?= $this->session->userdata('username') ?>">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <label for="email"> Correo </label>
                                            <input id="email" type="email" class="form-control" name="email" value="<?= $this->session->userdata('email') ?>">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <label for="password"> Clave </label>
                                            <input id="password" type="password" class="form-control" required name="password">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <button class="btn btn-success">Guardar</button>
                                        </div>
                                    </li>
                                </ul>
                            </form>

                            <?php if ($_SESSION['type_user'] != 3) : ?>
                               
                            <div id="newsSolicitudes">
                                <h5 class="box-title"><b>Solicitudes Pendientes</b></h5>
                                <ul class="basic-list">
                                    <?php foreach ($solicitudes as $key): ?>
                                    <li><small class="text-success">
                                            <?= date_format(date_create($key->date_register_vendor),'d-m-Y') ?>
                                        </small>
                                        <br>
                                        <?= $key->name_vendor ?>                                         
                                        <a href="#" title="Aceptar" onclick="aprobarResp(<?= $key->id_vendor ?>)"><span class="pull-right label-success label" style="letter-spacing: 0;padding: 4px 9px;"><i class="fa fa-check"></i></span></a>
                                        <a href="#" title="Rechazar" onclick="rechazarResp(<?= $key->id_vendor ?>)"><span class="pull-right label-danger label" style="letter-spacing: 0;padding: 4px 9px;"><i class="fa fa-times"></i></span></a>
                                    </li>
                                    <?php endforeach ?>
                                </ul>

                                

                                <!-- Aprobar solicitudes de vendedores
                                <h5 class="box-title"><b>Solicitudes de Vendedores</b></h5>
                                    <h3 class="text-center" style="font-size: bold"> <a href="<?= base_url('aprobarVendedores')?>"> <?= count($aprobarVendedores)?> </a></h3> -->
                            </div>

                        <?php endif?>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->

                
                <script src="<?= base_url()?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>

                <script>
                    function aprobarResp(id_resp) 
                    {
                        var params = {'id_resp' : id_resp}
                        var all;
                        $.ajax({
                          url: '<?= base_url("viewResp") ?>',
                          type: 'POST',
                          data: params,
                          success: function (data) {
                                all = JSON.parse(data);
                                swal({   
                                    title: "Esta seguro de admitir a",   
                                    text: all[0].name_vendor+'\n Registrado: '+all[0].date_register_vendor+'\n Email: '+all[0].email_vendor,   
                                    type: "info",   
                                    showCancelButton: true,   
                                    confirmButtonColor: "#DD6B55",   
                                    confirmButtonText: "Si, Admitir",   
                                    cancelButtonText: "No, Rechazar Admision",   
                                    closeOnConfirm: false,   
                                    closeOnCancel: false 
                                }, function(isConfirm){   
                                    if (isConfirm) {    
                                        $.ajax({
                                            url: '<?= base_url("acceptResp") ?>',
                                            type: 'POST',
                                            data: params,
                                            success: function (data) {
                                                swal("Aceptado", "El Registro fue Admitido", "success");
                                                location.reload();
                                            }  
                                        });    
                                    } else { 
                                        $.ajax({
                                            url: '<?= base_url("refuseResp") ?>',
                                            type: 'POST',
                                            data: params,
                                            success: function (data) {
                                                swal("Rechazado", "El registro fue rechazado", "error");
                                                location.reload();
                                            } 
                                        });   
                                    }  
                                });
                            }
                        });                        
                    }

                    function rechazarResp(id_resp) 
                    {
                        var params = {'id_resp' : id_resp}
                        var all;
                        $.ajax({
                          url: '<?= base_url("viewResp") ?>',
                          type: 'POST',
                          data: params,
                          success: function (data) {
                                all = JSON.parse(data);
                                swal({   
                                    title: "Esta seguro de Rechazar a",   
                                    text: all[0].name_vendor+'\n Registrado: '+all[0].date_register_vendor+'\n Email: '+all[0].email_vendor,   
                                    type: "info",   
                                    showCancelButton: true,   
                                    confirmButtonColor: "#DD6B55",   
                                    confirmButtonText: "Si, Rechazar",   
                                    cancelButtonText: "No, Admitir Registro",   
                                    closeOnConfirm: false,   
                                    closeOnCancel: false 
                                }, function(isConfirm){   
                                    if (isConfirm) {    
                                        $.ajax({
                                            url: '<?= base_url("refuseResp") ?>',
                                            type: 'POST',
                                            data: params,
                                            success: function (data) {
                                                swal("Rechazado", "El registro fue rechazado", "error");
                                                location.reload();
                                            } 
                                        });
                                           
                                    } else { 
                                        $.ajax({
                                            url: '<?= base_url("acceptResp") ?>',
                                            type: 'POST',
                                            data: params,
                                            success: function (data) {
                                                swal("Aceptado", "El Registro fue Admitido", "success");
                                                location.reload();
                                            }  
                                        });    
                                    } 
                                });
                            }
                        });  
                    }

                    function savePass(user) 
                    {
                        $.ajax({
                            url: '<?= base_url("savePass") ?>',
                            type: 'POST',
                            data: $("#act_pass_user").serialize(),
                            success: function () {
                                swal({   
                                    title: "Exito",   
                                    text: "El cambio de Clave se llevo a cabo de manera exitosa, debe iniciar sesion nuevamente tras este cambio.\n por lo cual la sesion se cerrara ahora.",   
                                    timer: 3000,   
                                    showConfirmButton: true 
                                }, function(isConfirm){
                                    window.location = "<?= base_url() ?>";
                                });
                            }  
                        });
                    }
                </script>