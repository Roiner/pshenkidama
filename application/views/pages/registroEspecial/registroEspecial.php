<!DOCTYPE html>  
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>CRM Gestar - <?= $title_page ?></title>
  <!-- Bootstrap Core CSS -->
  <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
  <!-- animation CSS -->
  <link href="<?= base_url() ?>css/animate.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="<?= base_url() ?>css/style_old.css" rel="stylesheet">
  <!-- color CSS -->
  <link href="<?= base_url() ?>css/colors/purple.css" id="theme"  rel="stylesheet">

  <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register" style="background:url(images/back.jpg) center center/cover no-repeat!important;">
  <div class="login-box" style="right: auto;">
    <div class="white-box slimscrollsidebar">
      <form class="form-horizontal form-material" id="loginform" action="<?= base_url('registroMaster') ?>" method="post">
        <a href="javascript:void(0)" class="text-center db">
          <h3 class="box-title m-t-40 m-b-0">Bienvenido Master</h3><small>Registrate</small> 
        </a>
        <div class="form-group m-t-20">
          <div class="col-xs-12">
            <input class="form-control" type="text" name="nombre" required placeholder="Nombre">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required  name="cargo" placeholder="Cargo en la empresa">
          </div>
        </div>
        <div class="form-group m-t-20">
          <div class="col-xs-12">
            <input class="form-control" type="text" required name="uname" placeholder="Username">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required name="pass" placeholder="Password" id="pass">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required placeholder="Confirma Password" id="pass1" onkeyup="validaPass()">
            <span id="msg" style="color: red;"></span>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" id="but">Registrarme</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- jQuery -->
  <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?= base_url() ?>js/tether.min.js"></script>
  <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
  <!-- Menu Plugin JavaScript -->
  <script src="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

  <!--slimscroll JavaScript -->
  <script src="<?= base_url() ?>js/jquery.slimscroll.js"></script>
  <!--Wave Effects -->
  <script src="<?= base_url() ?>js/waves.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?= base_url() ?>js/custom.min.js"></script>
  <!--Style Switcher -->
  <script src="<?= base_url() ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

  <script>
    
    function  validaPass() 
    {
      var pass = $("#pass").val();
      var pass1 = $("#pass1").val();
      if (pass1 != pass) 
      {
        $("#msg").html("Las Contraseñas no Coinciden");
        document.getElementById('but').disabled = true;
      } else {
        $("#msg").html("");
        document.getElementById('but').disabled = false;
      }
    }


  </script>
</body>
</html>
