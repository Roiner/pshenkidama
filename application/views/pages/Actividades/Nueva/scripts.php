


<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script>
	
    $(document).ready(function() {
        $('#image_activity').change(function() {
            if (document.getElementById('image_activity').files[0].size > 2000000) 
            {
                alert("El archivo seleccionado es demasiado pesado, asegurese de seleccionar un archivo de 2MB o menor");
                document.getElementById('but').disabled = true;
            }    else {
            	document.getElementById('but').disabled = false;
            }
        });
    });
</script>	
