<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Nueva Actividad</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="#">Actividades</li>
                            <li class="active">Nueva Actividad</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form id="form-save-actividad" method="POST" action="<?= base_url("saveActividad")?>" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Titulo Actividad</label>
                                                        <input type="text" name="name_activity" class="form-control" placeholder="Titulo" id="name_activity" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha de Actividad</label>
                                                        <input type="date" class="form-control" data-validation="required" data-validation-error-msg="El campo es requerido" name="date_activity" id="date_activity" data-validation="date" data-validation-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Hora de Actividad</label>
                                                        <input type="time" class="form-control" data-validation="required" data-validation-error-msg="El campo es requerido" name="hour_activity" id="hour_activity">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Dirección de Actividad</label>
                                                        <input type="text" name="address_activity" class="form-control" placeholder="Dirección" data-validation="required" data-validation-error-msg="El campo es requerido" id="address_activity">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Cateogoria</label>
                                                        <select name="category_id" id="category_id" class="form-control" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                            <option value="">----seleccione una opcion----</option>
                                                            <option value="new">Nueva categoria</option>
                                                            <?php foreach ($categories as $category): ?>
                                                                <option value="<?= $category->id ?>"><?= $category->name ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="div_new_categoria" class="col-md-12 hidden" >
                                                    <div class="form-group">
                                                        <label class="control-label">Nueva categoria</label>
                                                        <input type="text" name="new_category" class="form-control" placeholder="Nueva categoria"  id="new_category">
                                                    </div>
                                                </div>
                                                <div>
                                                    
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Descripción de la Actividad</label>
                                                        <textarea name="description_activity" rows="15" placeholder="Descripción" class="form-control textarea_editor" id="description_activity""></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class=row>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Invitación por Mail</label>
                                                        <textarea name="mail_activity" rows="15" placeholder="Invitación" class="form-control textarea_editor2" id="mail_activity">Estimado [NOMBRE_AFILIADO], <br> </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="box-title m-t-40">Filtros Sugeridos</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-bottom: 21px;">Edad</label>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Hasta 30" id="age_activity_1">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Hasta 30</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 30 a 40" id="age_activity_2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 30 a 40</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 40 a 50" id="age_activity_3">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 40 a 50</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check" style="margin-top: 25px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 50 a 60" id="age_activity_4">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 50 a 60</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 60 en adelante" id="age_activity_5">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 60 en adelante</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Todas" id="age_activity_6">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">todas</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Genero</label>
                                                        <select class="form-control" data-placeholder="Seleccionar un Genero" tabindex="1" name="genre_activity" id="genre_activity" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                            <option value="Masculino">Masculino</option>
                                                            <option value="Femenino">Femenino</option>
                                                            <option value="Indistinto">Indistinto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-top: 30px; margin-bottom: 20px;">Categoria</label>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Confirmado" id="category_activity_1">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Confirmado</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Voto" id="category_activity_2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Voto</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Candidato" id="category_activity_3">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Candidato</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Candidato Confirmado" id="category_activity_4">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Candidato Confirmado</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check bd-example-indeterminate" style="margin-top: 50px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Contrario" id="category_activity_5">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Contrario</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Compañero" id="category_activity_6">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Compañero</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Indistinto" id="category_activity_7">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                        <label class="control-label" style="margin-bottom: 24px;">Comuna</label>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="1" id="commune_activity_1">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">1</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="2" id="commune_activity_2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">2</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="3" id="commune_activity_3">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">3</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="4" id="commune_activity_4">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">4</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="5" id="commune_activity_5">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">5</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="6" id="commune_activity_6">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">6</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="7" id="commune_activity_7">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">7</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="8" id="commune_activity_8">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">8</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="9" id="commune_activity_9">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">9</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="10" id="commune_activity_10">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">10</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="11" id="commune_activity_11">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">11</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="12" id="commune_activity_12">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">12</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="13" id="commune_activity_13">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">13</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="14" id="commune_activity_14">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">14</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="15" id="commune_activity_15">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">15</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="Indistinto" id="commune_activity_16">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Tipo de Actividad</label>
                                                        <select name="type_activity" class="form-control" id="type_activity" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                            <option value="">Seleccionar tipo</option>
                                                            <option value="1">Actividad Permanente</option>
                                                            <option value="2">Actividad Puntual</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Status de Actividad</label>
                                                        <select name="status_activity" class="form-control" id="status_activity" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                            <option value="">Seleccionar status</option>
                                                            <option value="1">Activa</option>
                                                            <option value="2">Inactiva</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>

                                                <!--/span-->
                                                <div class="col-md-6" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Imagen de Actividad</label>
                                                        <input type="file" class="form-control" name="image_activity" id="image_activity" accept=".jpg, .jpeg, .png" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="form-group text-center">
                                                        <label>¿Cual es el progreso de tu actividad?</label>
                                                        <br>
                                                        <input type="radio" class="radio" name="progress" value="0" id="cero" checked>
                                                        <label for="cero" class="label">0%</label>

                                                        <input type="radio" class="radio" name="progress" value="5" id="five">
                                                        <label for="five" class="label">5%</label>

                                                        <input type="radio" class="radio" name="progress" value="25" id="twentyfive">
                                                        <label for="twentyfive" class="label">25%</label>

                                                        <input type="radio" class="radio" name="progress" value="50" id="fifty">
                                                        <label for="fifty" class="label">50%</label>

                                                        <input type="radio" class="radio" name="progress" value="75" id="seventyfive">
                                                        <label for="seventyfive" class="label">75%</label>

                                                        <input type="radio" class="radio" name="progress" value="100" id="onehundred">
                                                        <label for="onehundred" class="label">100%</label>

                                                        <div class="progress">
                                                            <div class="progress-bar"></div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <input type="submit" name="Enviar" class="btn btn-success">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
<!-- Jquery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script>
        $(document).ready(function() {
            $('.textarea_editor').wysihtml5({
                "html": true
            });

            $('.textarea_editor2').wysihtml5({
                "html": true
            });

            $('#category_id').change(function(event) {
                if ($(this).val()=='new') {
                    $('#div_new_categoria').removeClass('hidden');
                }else{
                    $('#div_new_categoria').addClass('hidden');
                }
            });
        });
        /*
            
        function saveActividad()
        {
           var params = {
                'name_activity' : $("#name_activity").val(),
                'description_activity' : $("#description_activity").val(),
                'date_activity' : $("#date_activity").val(),
                'hour_activity' : $("#hour_activity").val(),
                'address_activity' : $("#address_activity").val(),
                'age_activity[]' : [],
                'genre_activity' : $("#genre_activity").val(),
                'category_activity[]' : [],
                'commune_activity[]' : [],
                'type_activity' : $("#type_activity").val(),
                'image_activity' : 'files/actividad/imagenes/'+document.getElementById('image_activity').files[0].name
            };
            $('input[name="age_activity[]"]:checked').each(function() {
              params['age_activity[]'].push($(this).val());
            });
            $('input[name="category_activity[]"]:checked').each(function() {
              params['category_activity[]'].push($(this).val());
            });
            $('input[name="commune_activity[]"]:checked').each(function() {
              params['commune_activity[]'].push($(this).val());
            });

            $.ajax({
                url: '<?= base_url('saveActividad') ?>',
                type: 'POST',
                data: params,
                type: 'json'
                })

                .done(function (data) {
                    console.info(data);
                    //saveFileAudio('<?= base_url('saveImageActivity') ?>');
                    /*swal({   
                        title: "Actividad Almacenada",     
                        showConfirmButton: true 
                    });
                    window.location.reload();*/
                
                //});
        //}

        function decodeHtml(html) {
            var txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        }

        function saveFileAudio(ruta) 
        {
            var archivos = document.getElementById('image_activity');
            
            var archivo = archivos.files;
            var arch = new FormData();
            
            if (archivo.length > 0)
            {
                for (var i = 0; i < archivo.length; i++) 
                {
                    arch.append('archivo'+i,archivo[i]);
                }

                $.ajax({
                    url: ruta,
                    type:'POST',
                    contentType:false,
                    data:arch,
                    processData:false,
                    cache:false
                });
            }
        }  
   
</script>