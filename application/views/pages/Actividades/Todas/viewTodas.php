
			<div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Todas las Actividades</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Lista de Actividades</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <?php $i = 0; ?>
                <!-- .row -->
                <div class="row">
                    <?php foreach ($allActivities as $key): ?>
                        <?php $i ++; ?>
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box" style="padding:10px;">
                            <div class="user-bg" style="height: 300px;margin: -10px;"> <img width="100%" alt="user" src="<?= base_url().$key->image_activity ?>" style="height: 300px;"> </div>
                            <div class="user-btm-box">
                                <!-- .row -->
                                <div class="row text-center">
                                	<div class="col-md-12">
                                		<strong><?= $key->name_activity ?></strong>
                                		<p><?= $key->description_activity ?></p>
                                	</div>
                                </div>
                                <div class="row text-center m-t-10">
                                    <div class="col-md-3 b-r"><strong>Fecha</strong>
                                        <p><?= $key->date_activity ?></p>
                                    </div>
                                    <div class="col-md-3 b-r"><strong>Hora</strong>
                                        <p><?= $key->hour_activity ?></p>
                                    </div>
                                    <div class="col-md-3 b-r"><strong>Edad</strong>
                                        <p>
                                        <?php for ($ia = 0; $ia < count(json_decode($key->age_activity)); $ia++) { ?>
                                            <?= json_decode($key->age_activity)[$ia] ?>
                                        <?php } ?>
                                        </p>
                                    </div>
                                    <div class="col-md-3"><strong>Direccion</strong>
                                        <p><?= $key->address_activity ?></p>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <hr>
                                <!-- .row -->
                                <div class="row text-center m-t-10">
                                    <div class="col-md-3 b-r"><strong>Tipo</strong>
                                        <p><?php if ($key->type_activity == 1): ?>Permantente<?php endif ?>
                                            <?php if ($key->type_activity == 2): ?>Puntual<?php endif ?>
                                        </p>
                                    </div>
                                    <div class="col-md-3 b-r"><strong>Genero</strong>
                                        <p><?= $key->genre_activity ?></p>
                                    </div>
                                    <div class="col-md-3 b-r"><strong>Categoria</strong>
                                        <p>
                                        <?php for ($ib = 0; $ib < count(json_decode($key->category_activity)); $ib++) { ?>
                                            <?= json_decode($key->category_activity)[$ib] ?>
                                        <?php } ?>
                                        </p>
                                    </div>
                                    <div class="col-md-3"><strong>Comuna</strong>
                                        <p>
                                        <?php for ($ic = 0; $ic < count(json_decode($key->commune_activity)); $ic++) { ?>
                                            <?= json_decode($key->commune_activity)[$ic] ?>
                                        <?php } ?>
                                        </p>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                     <a href="#" class="add btn btn-success btn-sm pull-left" data-toggle="modal" data-target="#myModal" onclick="activityId(<?= $key->id ?>)">  Asignar Vendedores </a>
                                </div>
                               
                                <div class="col-md-6">
                                    <a href="#" class="add btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#myModal" onclick="activityVendor(<?= $key->id ?>)">  Vendedores Asignados</a>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Lista de Vendedores</h4>
      </div>
      <div class="modal-body">
            <div id="contentAsignaResponsable">
            </div>
      </div>
     
    </div>
  </div>
</div>


<!-- Jquery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script>
   
    function activityId(activity_id) 
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/activityVendor/'+activity_id); 
    }

    function activityVendor(activity_id)
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/showActivityVendor/'+activity_id);
    }


    /*$(document).ready(function() {
        $('#myModal').on('hide.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
            location.reload();
        });
    });*/
    

</script>