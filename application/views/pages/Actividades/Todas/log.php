<div class="table-responsive">
    <table id="asignaTable" class="table table-striped">
        <thead>
            <tr>
                <th>Responsable</th>
                <th>Fecha</th>
            </tr>   
        </thead>
            <?php foreach ($responsables as $key): ?>
                <tr>
                    <td> <?= $key->username ?> </td>
                    <td> <?= $key->update_date ?> </td>
                </tr>
            <?php endforeach ?>

        <tbody>

        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('#asignaTable').DataTable({
            "lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
        });
    });
</script>