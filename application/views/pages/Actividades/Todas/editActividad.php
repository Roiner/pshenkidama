
        <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Editar Actividad</h4>
                        <div class="page-header">
                            <div align="center">
                               <img alt="<?= $activity[0]->name_activity ?>" src="<?= base_url().$activity[0]->image_activity ?>" style="width: 320px" class="img-responsive">
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form id="form-edit-actividad" method="POST" action="<?= base_url("updateActividad")?>" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <hr>
                                            <div class="row">
                                                <input type="hidden" name="id" value="<?= $activity[0]->id ?>">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Titulo Actividad</label>
                                                        <input type="text" name="name_activity" class="form-control" placeholder="Titulo" id="name_activity" data-validation="required" data-validation-error-msg="El campo es requerido" value=" <?= $activity[0]->name_activity?> ">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha de Actividad</label>
                                                        <input type="date" class="form-control" data-validation="required" data-validation-error-msg="El campo es requerido" name="date_activity" id="date_activity" data-validation="date" data-validation-format="dd/mm/yyyy" value="<?= $activity[0]->date_activity?>">
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Hora de Actividad</label>
                                                        <input type="time" class="form-control" data-validation="required" data-validation-error-msg="El campo es requerido" name="hour_activity" id="hour_activity" value="<?= $activity[0]->hour_activity?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Dirección de Actividad</label>
                                                        <input type="text" name="address_activity" class="form-control" placeholder="Dirección" data-validation="required" data-validation-error-msg="El campo es requerido" id="address_activity" value="<?= $activity[0]->address_activity?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Cateogoria</label>
                                                        <select name="category_id" id="category_id" class="form-control">
                                                            <?php foreach ($categories as $category): ?>
                                                                <option value="<?= $category->id ?>"><?= $category->name ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Descripción de la Actividad</label>
                                                        <textarea name="description_activity" rows="15" placeholder="Descripción" class="form-control textarea_editor" id="description_activity"><?= $activity[0]->description_activity?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Email de Invitación a la Actividad</label>
                                                        <textarea name="mail_activity" rows="15" placeholder="Invitación" class="form-control textarea_editor2" id="description_activity"><?= $activity[0]->mail_activity?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="box-title m-t-40">Filtros Sugeridos</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-bottom: 21px;">Edad</label>

                                                            <?php
            
                                                            $age_activity = str_replace('[', "", $activity[0]->age_activity);
                                                            $age_activity = str_replace(']', "", $age_activity);
                                                            $age_activity = str_replace('"', "", $age_activity);

                                                            $edad = explode(",", $age_activity);
                                                            
                                                            ?>

                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Hasta 30" id="age_activity_1" 
                                                                    
                                                                    <?php if (in_array("Hasta 30", $edad)): ?>

                                                                        checked
                                                                        
                                                                    <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Hasta 30</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 30 a 40" id="age_activity_2"
                                                                        
                                                                        <?php if (in_array("De 30 a 40", $edad)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 30 a 40</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 40 a 50" id="age_activity_3"
                                                                        
                                                                        <?php if (in_array("De 40 a 50", $edad)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 40 a 50</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check" style="margin-top: 25px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 50 a 60" id="age_activity_4"
                                                                    
                                                                    <?php if (in_array("De 50 a 60", $edad)): ?>

                                                                            checked
                                                                            
                                                                    <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 50 a 60</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 60 en adelante" id="age_activity_5"
                                                                    
                                                                    <?php if (in_array("De 60 en adelante", $edad)): ?>

                                                                            checked
                                                                            
                                                                    <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 60 en adelante</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Todas" id="age_activity_6"
                                                                        
                                                                        <?php if (in_array("Todas", $edad)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">todas</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Genero</label>
                                                        <select class="form-control" data-placeholder="Seleccionar un Genero" tabindex="1" name="genre_activity" id="genre_activity" data-validation="required" data-validation-error-msg="El campo es requerido" value="<?=$activity[0]->genre_activity ?>">
                                                            <option value="Masculino"
                                                                <?php if($activity[0]->genre_activity == "Masculino"):?> selected <?php endif?>
                                                                >Masculino</option>
                                                            <option value="Femenino"
                                                                <?php if($activity[0]->genre_activity == "Femenino"):?> selected <?php endif?>>Femenino</option>
                                                            <option value="Indistinto"
                                                                <?php if($activity[0]->genre_activity == "Indistinto"):?> selected <?php endif?>
                                                                >Indistinto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-top: 30px; margin-bottom: 20px;">Categoria</label>

                                                            <?php
            
                                                                $category_activity = str_replace('[', "", $activity[0]->category_activity);
                                                                $category_activity = str_replace(']', "", $category_activity);
                                                                $category_activity = str_replace('"', "", $category_activity);

                                                                $cat = explode(",", $category_activity);
                                                                
                                                            ?>


                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Confirmado" id="category_activity_1"
                                                                        <?php if (in_array("Voto Confirmado", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Confirmado</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Voto" id="category_activity_2"
                                                                        <?php if (in_array("Posible Voto", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Voto</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Candidato" id="category_activity_3"

                                                                        <?php if (in_array("Posible Candidato", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >

                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Candidato</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Candidato Confirmado" id="category_activity_4"  

                                                                        <?php if (in_array("Candidato Confirmado", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Candidato Confirmado</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check bd-example-indeterminate" style="margin-top: 50px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Contrario" id="category_activity_5"

                                                                        <?php if (in_array("Voto Contrario", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>>

                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Contrario</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Compañero" id="category_activity_6"

                                                                        <?php if (in_array("Compañero", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Compañero</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Indistinto" id="category_activity_7"
                                                                        <?php if (in_array("Indistinto", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                        <label class="control-label" style="margin-bottom: 24px;">Comuna</label>
                                                            <div class="form-check">

                                                                <?php
            
                                                                    $comune_activity = str_replace('[', "", $activity[0]->commune_activity);
                                                                    $comune_activity = str_replace(']', "", $comune_activity);
                                                                    $comune_activity = str_replace('"', "", $comune_activity);

                                                                    $comune = explode(",", $comune_activity);
                                                                    
                                                                ?>
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="1" id="commune_activity_1"
                                                                        <?php if (in_array(1, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">1</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="2" id="commune_activity_2"
                                                                        <?php if (in_array(2, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">2</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="3" id="commune_activity_3"
                                                                        <?php if (in_array(3, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">3</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="4" id="commune_activity_4"
                                                                        <?php if (in_array(4, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">4</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="5" id="commune_activity_5"
                                                                        <?php if (in_array(5, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">5</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="6" id="commune_activity_6"
                                                                        <?php if (in_array(6, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">6</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="7" id="commune_activity_7"
                                                                        <?php if (in_array(7, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">7</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="8" id="commune_activity_8"
                                                                        <?php if (in_array(8, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">8</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="9" id="commune_activity_9"
                                                                        <?php if (in_array(9, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">9</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="10" id="commune_activity_10"
                                                                        <?php if (in_array(10, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">10</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="11" id="commune_activity_11"
                                                                        <?php if (in_array(11, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">11</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="12" id="commune_activity_12"
                                                                        <?php if (in_array(12, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">12</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="13" id="commune_activity_13"

                                                                    <?php if (in_array(13, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">13</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="14" id="commune_activity_14" 
                                                                        <?php if (in_array(14, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">14</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="15" id="commune_activity_15"
                                                                        <?php if (in_array(15, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">15</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="Indistinto" id="commune_activity_16"
                                                                        <?php if (in_array("Indistinto", $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Tipo de Actividad</label>
                                                        <select name="type_activity" class="form-control" id="type_activity" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                            <option value="1"
                                                                <?php if($activity[0]->type_activity == 1) : ?> selected <?php endif?>>Actividad Permanente</option>
                                                            <option value="2"
                                                                <?php if($activity[0]->type_activity == 2) : ?> selected <?php endif?>>Actividad Puntual</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Status de Actividad</label>
                                                        <select name="status_activity" class="form-control" id="status_activity" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                            <option value="1" <?php if($activity[0]->status_activity == 1) : ?> selected <?php endif?> >Activa</option>
                                                            <option value="2" <?php if($activity[0]->status_activity == 2) : ?> selected <?php endif?>>Inactiva</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>

                                                <!--/span-->
                                                <div class="col-md-6" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Imagen de Actividad</label>
                                                        <input type="file" class="form-control" name="image_activity" id="image_activity" accept=".jpg, .jpeg, .png" data-validation="required" data-validation-error-msg="El campo es requerido">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="form-group text-center">
                                                        <label>¿Cual es el progreso de tu actividad?</label>
                                                        <br>
                                                        <input type="radio" class="radio" name="progress" value="0" id="cero" <?php if($activity[0]->progress == 0) : ?> checked <?php endif?> >
                                                        <label for="cero" class="label">0%</label>

                                                        <input type="radio" class="radio" name="progress" value="5" id="five" <?php if($activity[0]->progress == 5) : ?> checked <?php endif?>>
                                                        <label for="five" class="label">5%</label>

                                                        <input type="radio" class="radio" name="progress" value="25" id="twentyfive" <?php if($activity[0]->progress == 25) : ?> checked <?php endif?>>
                                                        <label for="twentyfive" class="label">25%</label>

                                                        <input type="radio" class="radio" name="progress" value="50" id="fifty" <?php if($activity[0]->progress == 50) : ?> checked <?php endif?>>
                                                        <label for="fifty" class="label">50%</label>

                                                        <input type="radio" class="radio" name="progress" value="75" id="seventyfive" <?php if($activity[0]->progress == 75) : ?> checked <?php endif?>>
                                                        <label for="seventyfive" class="label">75%</label>

                                                        <input type="radio" class="radio" name="progress" value="100" id="onehundred" <?php if($activity[0]->progress == 100) : ?> checked <?php endif?>>
                                                        <label for="onehundred" class="label">100%</label>

                                                        <div class="progress">
                                                            <div class="progress-bar"></div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success">Enviar</button>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->

<script>
    $(document).ready(function() {
        $('.textarea_editor').wysihtml5({
            "html": true
        });

        $('.textarea_editor2').wysihtml5({
            "html": true
        });
    });
</script>

