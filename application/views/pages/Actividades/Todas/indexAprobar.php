            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> <?= count($allActivities)?> Vendedores esperando aprobación</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Lista de Actividades</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="white-box">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover"  id="myTable">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Nombre</th>
                                            <th>Fecha - Hora</th>
                                            <th>Vendedor</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php foreach ($allActivities as $key): ?>
                                            <tr>
                                                <td>
                                                    <img alt="<?= $key->name_activity ?>" src="<?= base_url().$key->image_activity ?>" style="height: 60px; width: 60px;">
                                                </td>
                                                <td>
                                                   <strong><?= $key->name_activity ?></strong> 
                                                </td>
                                                <td>
                                                    <?php $date = date_create($key->date_activity);?>
                                                    <p><?= date_format($date, 'd-m-Y')?> | <?= $key->hour_activity ?> </p>
                                                </td>
                                                <td>
                                                    <?= $key->name_vendor?>
                                                </td>
                                                
                                                <td>
                                                    <a href="#" title="Aprobar" style="margin-right: 10px;" onclick="aprobeVendor(<?= $key->id_activity ?>,<?= $key->id_vendor ?>);"><i class="fa fa-check" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i></a>

                                                    <a href="#" title="Retirar" style="margin-right: 10px;" onclick="quitVendor(<?= $key->id_activity ?>,<?= $key->id_vendor ?>);"><i class="fa fa-times" style="background: #fb9678; padding: 12px;color: white !important; border-radius: 5px;"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                    </div>
                </div>
            </div>


<!-- Jquery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

<script>
    
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
   
    function aprobeVendor(actividad, vendor) {
            params = {'id_activity' : actividad , 'id_vendor' : vendor};
            $.ajax({
                url: '<?= base_url("aprobeActivityVendor") ?>',
                type: 'POST',
                data: params,
                success: function (data) {
                info = JSON.parse(data);
                                
                    swal({   
                        title: info.message,     
                        showConfirmButton: true 
                    });
                    setTimeout(redirect2,1000);
                }
            });
    }

    function quitVendor(actividad, vendor) {
            params = {'id_activity' : actividad , 'id_vendor' : vendor};
            $.ajax({
                url: '<?= base_url("quitActivityVendor") ?>',
                type: 'POST',
                data: params,
                success: function (data) {
                info = JSON.parse(data);
                                
                    swal({   
                            title: info.message,     
                            showConfirmButton: true 
                        });
                    setTimeout(redirect2,1000);
                }
            });
    }

    function redirect2 ()
    {
        window.location.href = '<?= base_url('aprobarVendedores')?>';
    }
   
</script>