

      <div class="container-fluid">
    
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Todas las Actividades</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Lista de Actividades</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="white-box">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover"  id="myTable">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Nombre</th>
                                            <th>Fecha - Hora</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php foreach ($allActivities as $key): ?>
                                            <tr>
                                                <td>
                                                    <img alt="<?= $key->name_activity ?>" src="<?= base_url().$key->image_activity ?>" style="height: 60px; width: 60px;">
                                                </td>
                                                <td>
                                                    <?php if (isset($key->vista) && ($key->vista == 0)) :?>
                                                        <span class="label label-danger">Nueva</span>
                                                    <?php endif; ?>
                                                    <strong><?= $key->name_activity ?></strong> 
                                                    <br>
                                                    <div class="progress" data-toggle="modal" data-target="#myModal" onclick="showLog(<?= $key->id ?>)" style="cursor: pointer;">
                                                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50"
                                                      aria-valuemin="0" aria-valuemax="100" style="width:<?= $key->progress ?>%">
                                                      <?= $key->progress ?>%
                                                    </div>
                                                  </div>

                                                </td>
                                                
                                                <td>
                                                    <?php
                                                        $date = date_create($key->date_activity);
                                                        $hour = date('h:i A', strtotime($key->hour_activity));
                                                    ?>
                                                    <p><?= date_format($date, 'd-m-Y')?> | <?= $hour ?> </p>
                                                </td>
                                                
                                                <td>
                                                <?php if ($_SESSION['type_user'] != 3) : ?>
                                                    
                                                    <div class="btn-group" role="group">
                                                        <a href="#" class="add " data-toggle="modal" data-target="#myModal" onclick="showActivity(<?= $key->id ?>)" title="Detalles de la Actividad" style="margin-right: 2px">
                                                            <i class="fa fa-search" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                        </a>
                                                    
                                                        <a href="#" class="add " data-toggle="modal" data-target="#myModal" onclick="activityId(<?= $key->id ?>)" title="Asignar Responsable"  style="margin-right: 2px">
                                                            <i class="fa fa-plus" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                        </a>
                                                        
                                                        <a href="#" class="add " data-toggle="modal" data-target="#myModal" onclick="activityVendor(<?= $key->id ?>)"  title="Responsables Asignados" style="margin-right: 2px">
                                                            <i class="fa fa-users" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                        </a>


                                                        <a class="add " onclick="delete_actividad(<?= $key->id ?>, this)" title="Eliminar" style="margin-right: 2px">
                                                            <i class="fa fa-remove" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                        </a>


                                                        <a class="add " title="Culminar Tarea" style="margin-right: 2px" href="<?= base_url("") ?>Actividades/culminarTarea/<?= $key->id ?>">
                                                            <i class="fa fa-check" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                        </a>

                                                    </div>                       
                                                    
                                                <?php else: ?>
                                                    <!-- Nuevo -->
                                                        <a class="add" href="#" data-toggle="modal" data-target="#myModal" onclick="show(<?= $key->id_activity ?>)" title="Detalles de la Actividad">
                                                            <i class="fa fa-search" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                        </a>
                                                        <a class="add " title="Culminar Tarea" style="margin-right: 2px" href="<?= base_url("") ?>Actividades/culminarTarea/<?= $key->id ?>">
                                                            <i class="fa fa-check" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                        </a>
                                                <?php endif; ?>

                                                </td>
                                            </tr>
                                        <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                    </div>
                </div>
            </div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
            <div id="contentAsignaResponsable">
                
            </div>
      </div>
     
    </div>
  </div>
</div>




<!-- Jquery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>



<script>
    
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
   
    function activityId(activity_id) 
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/activityVendor/'+activity_id); 
    }

    function activityVendor(activity_id)
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/showActivityVendor/'+activity_id);
    }

    function showActivity(activity_id)
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/editActividad/'+activity_id);
    }

    /* Nuevo */

    function show(activity_id)
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/showActividad/'+activity_id);
    }

    function showLog(activity_id)
    {
        cargar($("#contentAsignaResponsable"),'<?= base_url("") ?>Actividades/logActividad/'+activity_id); 
    }


    /*$(document).ready(function() {
        $('#myModal').on('hide.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
            location.reload();
        });
    });*/

      function addVendor(actividad, vendor)
      {
        params = {'id_activity' : actividad , 'id_vendor' : vendor};

        console.info(params);
        
          $.ajax({
          url: '<?= base_url("inscribir") ?>',
          type: 'POST',
          data: params,
          success: function (data) {
            info = JSON.parse(data);         
              swal({   
                  title: info.message,     
                  showConfirmButton: true 
                });
            }
          });
      }




      function delete_actividad(id, btn)
      {
          $.ajax({
          url: '<?= base_url() ?>Actividades/delete/'+id,
          type: 'POST',
          data: id,
          success: function (data) {
            info = JSON.parse(data);         
              swal({   
                  title: info.message,     
                  showConfirmButton: true 
                });

              $(btn).closest("tr").remove();
            }
          });
      }
   
</script>

                <?php if (!empty($this->session->flashdata('msg'))): ?>
                    <script>
                        $(document).ready(function() {
                            swal({
                                title: "<?= $this->session->flashdata('msg'); ?>",
                                type: 'success'
                            });
                        });
                    </script>
                <?php endif; ?>