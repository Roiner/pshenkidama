
        <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Detalles de la Actividad</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Detalles de Actividad</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="page-header">
                                <div align="center">
                                   <img alt="<?= $activity[0]->name_activity ?>" src="<?= base_url().$activity[0]->image_activity ?>" style="width: 320px" class="img-responsive">
                                </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                   
                                        <div class="form-body">
                                            <hr>
                                            <div class="row">
                                            
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Titulo Actividad</label>
                                                        <?= $activity[0]->name_activity?>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <?php
                                                        $date = date_create($activity[0]->date_activity);
                                                        $hour = date('h:i A', strtotime($activity[0]->hour_activity));
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha de Actividad</label>
                                                        <?= date_format($date, 'd-m-Y')?>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Hora de Actividad</label>
                                                        <?= $hour?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Dirección de Actividad</label> <br>
                                                        <?= $activity[0]->address_activity?>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Descripción de la Actividad</label> <br>
                                                        <?= $activity[0]->description_activity?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Email de Invitación a la Actividad</label> <br>
                                                        <?= $activity[0]->mail_activity?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="box-title m-t-40">Filtros Sugeridos</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-bottom: 21px;">Edad</label>

                                                            <?php
            
                                                            $age_activity = str_replace('[', "", $activity[0]->age_activity);
                                                            $age_activity = str_replace(']', "", $age_activity);
                                                            $age_activity = str_replace('"', "", $age_activity);

                                                            $edad = explode(",", $age_activity);
                                                            
                                                            ?>

                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Hasta 30" id="age_activity_1" 
                                                                    
                                                                    <?php if (in_array("Hasta 30", $edad)): ?>

                                                                        checked
                                                                        
                                                                    <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Hasta 30</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 30 a 40" id="age_activity_2"
                                                                        
                                                                        <?php if (in_array("De 30 a 40", $edad)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 30 a 40</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 40 a 50" id="age_activity_3"
                                                                        
                                                                        <?php if (in_array("De 40 a 50", $edad)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 40 a 50</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check" style="margin-top: 25px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 50 a 60" id="age_activity_4"
                                                                    
                                                                    <?php if (in_array("De 50 a 60", $edad)): ?>

                                                                            checked
                                                                            
                                                                    <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 50 a 60</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 60 en adelante" id="age_activity_5"
                                                                    
                                                                    <?php if (in_array("De 60 en adelante", $edad)): ?>

                                                                            checked
                                                                            
                                                                    <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 60 en adelante</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Todas" id="age_activity_6"
                                                                        
                                                                        <?php if (in_array("Todas", $edad)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>

                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">todas</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Genero</label>
                                                        <select class="form-control" data-placeholder="Seleccionar un Genero" tabindex="1" name="genre_activity" id="genre_activity" value="<?=$activity[0]->genre_activity ?> " readonly>
                                                            <option value="Masculino"
                                                                <?php if($activity[0]->genre_activity == "Masculino"):?> selected <?php endif?>
                                                                >Masculino</option>
                                                            <option value="Femenino"
                                                                <?php if($activity[0]->genre_activity == "Femenino"):?> selected <?php endif?>>Femenino</option>
                                                            <option value="Indistinto"
                                                                <?php if($activity[0]->genre_activity == "Indistinto"):?> selected <?php endif?>
                                                                >Indistinto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-top: 30px; margin-bottom: 20px;">Categoria</label>

                                                            <?php
            
                                                                $category_activity = str_replace('[', "", $activity[0]->category_activity);
                                                                $category_activity = str_replace(']', "", $category_activity);
                                                                $category_activity = str_replace('"', "", $category_activity);

                                                                $cat = explode(",", $category_activity);
                                                                
                                                            ?>


                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Confirmado" id="category_activity_1"
                                                                        <?php if (in_array("Voto Confirmado", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Confirmado</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Voto" id="category_activity_2"
                                                                        <?php if (in_array("Posible Voto", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Voto</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Candidato" id="category_activity_3"

                                                                        <?php if (in_array("Posible Candidato", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >

                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Candidato</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Candidato Confirmado" id="category_activity_4"  

                                                                        <?php if (in_array("Candidato Confirmado", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Candidato Confirmado</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check bd-example-indeterminate" style="margin-top: 50px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Contrario" id="category_activity_5"

                                                                        <?php if (in_array("Voto Contrario", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>>

                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Contrario</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Compañero" id="category_activity_6"

                                                                        <?php if (in_array("Compañero", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Compañero</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Indistinto" id="category_activity_7"
                                                                        <?php if (in_array("Indistinto", $cat)): ?>

                                                                            checked
                                                                            
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                        <label class="control-label" style="margin-bottom: 24px;">Comuna</label>
                                                            <div class="form-check">

                                                                <?php
            
                                                                    $comune_activity = str_replace('[', "", $activity[0]->commune_activity);
                                                                    $comune_activity = str_replace(']', "", $comune_activity);
                                                                    $comune_activity = str_replace('"', "", $comune_activity);

                                                                    $comune = explode(",", $comune_activity);
                                                                    
                                                                ?>
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="1" id="commune_activity_1"
                                                                        <?php if (in_array(1, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">1</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="2" id="commune_activity_2"
                                                                        <?php if (in_array(2, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">2</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="3" id="commune_activity_3"
                                                                        <?php if (in_array(3, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">3</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="4" id="commune_activity_4"
                                                                        <?php if (in_array(4, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">4</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="5" id="commune_activity_5"
                                                                        <?php if (in_array(5, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">5</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="6" id="commune_activity_6"
                                                                        <?php if (in_array(6, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">6</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="7" id="commune_activity_7"
                                                                        <?php if (in_array(7, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">7</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="8" id="commune_activity_8"
                                                                        <?php if (in_array(8, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">8</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="9" id="commune_activity_9"
                                                                        <?php if (in_array(9, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">9</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="10" id="commune_activity_10"
                                                                        <?php if (in_array(10, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">10</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="11" id="commune_activity_11"
                                                                        <?php if (in_array(11, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">11</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="12" id="commune_activity_12"
                                                                        <?php if (in_array(12, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">12</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="13" id="commune_activity_13"

                                                                    <?php if (in_array(13, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">13</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="14" id="commune_activity_14" 
                                                                        <?php if (in_array(14, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">14</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="15" id="commune_activity_15"
                                                                        <?php if (in_array(15, $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">15</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="Indistinto" id="commune_activity_16"
                                                                        <?php if (in_array("Indistinto", $comune)): ?>

                                                                            checked
                                                                                
                                                                        <?php endif ?>
                                                                    >
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Tipo de Actividad</label>
                                                        <select name="type_activity" class="form-control" id="type_activity" readonly>
                                                            <option value="1"
                                                                <?php if($activity[0]->type_activity == 1) : ?> selected <?php endif?>>Actividad Permanente</option>
                                                            <option value="2"
                                                                <?php if($activity[0]->type_activity == 2) : ?> selected <?php endif?>>Actividad Puntual</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Status de Actividad</label>
                                                        <select name="status_activity" class="form-control" id="status_activity" readonly>
                                                            <option value="1" <?php if($activity[0]->status_activity == 1) : ?> selected <?php endif?> >Activa</option>
                                                            <option value="2" <?php if($activity[0]->status_activity == 2) : ?> selected <?php endif?>>Inactiva</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="margin-top: 29px;">
                                                    <div class="form-group text-center">
                                                        <label>¿Cual es el progreso de tu actividad?</label>
                                                        <br>
                                                        <input type="radio" class="radio" name="progress" value="0" id="cero" checked>
                                                        <label for="cero" class="label">0%</label>

                                                        <input type="radio" class="radio" name="progress" value="5" id="five">
                                                        <label for="five" class="label">5%</label>

                                                        <input type="radio" class="radio" name="progress" value="25" id="twentyfive">
                                                        <label for="twentyfive" class="label">25%</label>

                                                        <input type="radio" class="radio" name="progress" value="50" id="fifty">
                                                        <label for="fifty" class="label">50%</label>

                                                        <input type="radio" class="radio" name="progress" value="75" id="seventyfive">
                                                        <label for="seventyfive" class="label">75%</label>

                                                        <input type="radio" class="radio" name="progress" value="100" id="onehundred">
                                                        <label for="onehundred" class="label">100%</label>

                                                        <div class="progress">
                                                            <div class="progress-bar"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>
                                        
                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
