<div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dudas</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li><a href="#">Dudas</a></li>
                            <li class="active">Lista de Dudas</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Listado de Dudas</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Responsable</th>
                                            <th>Asunto</th>
                                            <th>Mensaje</th>
                                            <th>Respuesta</th>
                                            <th>Fecha</th>
                                            <th>Status</th>
                                            <?php if($_SESSION['type_user'] != 3) : ?>
                                                <th>Acciones</th>
                                            <?php endif; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($nigglings as $key): ?>
                                        <tr>
                                            <td><?= $key->name_vendor ?></td>
                                            <td><?= $key->subject ?></td>
                                            <td><?= $key->message ?></td>
                                            <td><?= $key->response ?></td>
                                            <td>
                                                <?php $date = date_create($key->date); ?>
                                                <?=  date_format($date, 'd-m-Y');?>
                                            </td>
                                            <td>
                                                <?php if($key->is_response == 1): ?>
                                                    <span class="label label-table bg-success">Respondida</span>
                                                <?php else : ?>
                                                    <span class="label label-table bg-danger">Por Responder</span>
                                                <?php endif ?>
                                            
                                            <?php if($_SESSION['type_user'] != 3) : ?>

                                            <td>
                                                    <?php if($key->is_response == 0): ?>
                                                        
                                                        <a  href="#" data-toggle="modal" data-target="#modalResponder" onclick="find(<?= $key->id ?>);">
                                                            <i class="fa fa-search" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                        </a>
                                                    
                                                    <?php endif ?>

                                                   
                                                    <a href="#" style="margin-right: 10px;" onclick="deleteResponse(<?= $key->id ?>);">
                                                        <i class="fa fa-times" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                    </a>
                                            </td>
                                            
                                            <?php endif; ?>
                                        
                                        </tr>   
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

    <div class="modal fade" id="modalResponder">
                    
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

                        <div class="page-heder">
                            <h3 class="text-center">Responder Duda</h3>    
                            <hr>
                        </div>
                        
                        <div class="duda">
                            <b>Responsable:</b>  <p id="vendor"></p>
                            <b>Asunto: </b> <p id="subject"></p>
                            <b>Mensaje: </b> <p id="message"></p>
                        </div>

                        <hr>

                        <form method="post" id="niggling-form-response">
                            <div class="form-body">
                                <div class="row">
                                    <input type="hidden" name="niggling_id">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Respuesta</label>
                                            <textarea name="niggling_response" class="form-control" required id="niggling_response" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-danger waves-effect waves-light">Enviar Respuesta</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                        </form>
                    </div>
                </div>

<!-- jQuery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

<script>
    
    $(document).ready(function() {
        $('#myTable').DataTable();

        $('form#niggling-form-response').on('submit', function(ev){
            
            ev.preventDefault();
            ev.stopPropagation();
            
           $.ajax({
                method: "POST",
                url: "<?= base_url();?>Nigglings/saveResponse",
                data: new FormData(this),
                processData : false,
                contentType : false,
                type: 'json'
            })
              .done(function(data) {
                info = JSON.parse(data);

                if (info.type == 'success') {
                    $('form#niggling-form-response')[0].reset();
                    $('button.close').click();

                    
                    swal({
                        title: info.message,
                        text: "El mensaje ha sido respondido exitosamente!",
                        icon: "success",
                        button: "ok",
                    });

                    $('button.confirm').on('click', function(ev)
                        {
                            location.reload(true);
                        }
                    );


                }else{
                    swal({
                        title: info.message,
                        text: "Por favor intenta nuevamente",
                        icon: "error",
                        button: "ok",
                    });
                }
              });

        });
    });

    function find(id)
    {
       $.ajax({
            method: "POST",
            url: "<?= base_url();?>Nigglings/find/"+id,
            data: id ,
            processData : false,
            contentType : false,
            type: 'json'
        })
        .done(function( data ) {
            info = JSON.parse(data);

            $('p#vendor').text(info.result[0].name_vendor);
            $('p#subject').text(info.result[0].subject);
            $('p#message').text(info.result[0].message);
            $('input[name="niggling_id"]').attr('value', info.result[0].id);
            
        });
    }


    function deleteResponse(id)
    {
        
       $.ajax({
            method: "POST",
            url: "<?= base_url();?>Nigglings/delete/"+id,
            data: id ,
            processData : false,
            contentType : false,
            type: 'json'
        })
        .done(function( data ) {
            info = JSON.parse(data);
            
            if (info.type == 'success') {
                    $('form#niggling-form-response')[0].reset();
                    $('button.close').click();

                    
                    swal({
                        title: info.message,
                        text: "El mensaje ha sido respondido exitosamente!",
                        icon: "success",
                        button: "ok",
                    });

                    $('button.confirm').on('click', function(ev)
                        {
                            location.reload(true);
                        }
                    );


                }else{
                    swal({
                        title: info.message,
                        text: "Por favor intenta nuevamente",
                        icon: "error",
                        button: "ok",
                    });
                }
              
        });
    }        


</script>