<div class="container-fluid">
<div class="row bg-title">
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
<h4 class="page-title">Principal </h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
<ol class="breadcrumb">
<li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
<li class="active">Panel Escritorio</li>
</ol>
</div>
<!-- /.col-lg-12 -->
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="panel-group">
<div class="panel panel-default">

</div>
</div>
</div>
</div>

<!--row -->
<div class="row">
<h4></h4>
<div class="col-sm-12">
<div class="white-box">
<?php foreach ($afiliado as $af): ?>


<h4 class="modal-title">Afiliado: <?= $af->last_names_affiliate .' '. $af->names_affiliate ?> </h4>
</div>
<div class="modal-body">
<div class="col-md-12 col-xs-12">
<div class="white-box">
  <ul class="nav customtab nav-tabs" role="tablist">
    <li role="presentation" class="nav-item">
      <a href="#actividad" class="nav-link active" aria-controls="actividad" role="tab" data-toggle="tab" aria-expanded="true">
        <span class="visible-xs">
          <i class="fa fa-home"></i>
        </span>
        <span class="hidden-xs"> Historial</span>
      </a>
    </li>
    <li role="presentation" class="nav-item">
      <a href="#profile" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">
        <span class="visible-xs">
          <i class="fa fa-home"></i>
        </span>
        <span class="hidden-xs"> Datos Generales</span>
      </a>
    </li>
    <li role="presentation" class="nav-item">
      <a href="#home" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">
        <span class="visible-xs">
          <i class="fa fa-user"></i>
        </span> 
        <span class="hidden-xs">Datos de Contacto</span>
      </a>
    </li>
    <li role="presentation" class="nav-item">
      <a href="#messages" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">
        <span class="visible-xs">
          <i class="fa fa-envelope-o"></i>
        </span> 
        <span class="hidden-xs">Categorizar</span>
      </a>
    </li>
    <li role="presentation" class="nav-item">
      <a href="#asignados" class="nav-link" aria-controls="asignados" role="tab" data-toggle="tab" aria-expanded="false">
        <span class="visible-xs">
          <i class="fa fa-th-list"></i>
        </span> 
        <span class="hidden-xs">Responsables</span>
      </a>
    </li>

    <?php if($_SESSION['type_user'] == 3 || $_SESSION['type_user'] == 2): ?>                                            
      <li role="presentation" class="nav-item">
        <a href="#actividades" class="nav-link" aria-controls="actividades" role="tab" data-toggle="tab" aria-expanded="false">
          <span class="visible-xs">
            <i class="fa fa-th-navicon"></i>
          </span> 
          <span class="hidden-xs">Actividades</span>
        </a>
      </li>

    <?php endif;?>

  <?php if($_SESSION['type_user'] == 3 || $_SESSION['type_user'] == 2): ?>                                            
      <li role="presentation" class="nav-item">
        <a href="#afiliados" class="nav-link" aria-controls="afiliados" role="tab" data-toggle="tab" aria-expanded="false">
          <span class="visible-xs">
            <i class="fa fa-th-navicon"></i>
          </span> 
          <span class="hidden-xs">Afiliado</span>
        </a>
      </li>

    <?php endif;?>    

    <li role="presentation" class="nav-item">
        <a href="#interests" class="nav-link" aria-controls="interests" role="tab" data-toggle="tab" aria-expanded="false">
          <span class="visible-xs">
            <i class="fa fa-th-navicon"></i>
          </span> 
          <span class="hidden-xs">Interess</span>
        </a>
      </li>

    <?php if(!empty($af->twitter_affiliate)) : ?>   
      <li role="presentation" class="nav-item">
        <a href="#twitter" class="nav-link" aria-controls="twitter" role="tab" data-toggle="tab" aria-expanded="false">
          <span class="visible-xs">
            <i class="fa fa-th-navicon"></i>
          </span> 
          <span class="hidden-xs">Twitter</span>
        </a>
      </li>
    <?php endif;?>

  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="actividad">
      <div class="white-box">
        <h3 class="box-title">Contactos con el Afiliado</h3>
        <div class="steamline">
          <?php foreach ($contacts as $contact) :?>
            <div class="sl-item">
              <div class="sl-left"> </div>
              <div class="sl-right">

                <div>
                  <?php $date = date_create($contact->date_contact_affiliate) ?>
                  <a href="#"><?= date_format($date, 'd-m-Y') ?></a>
                </div>
                <p><?= $contact->name_contacts_type?></p>
              </div>
            </div>
          <?php endforeach?>
        </div>
      </div>
    </div>
    <div class="tab-pane" id="profile">
      <hr>
      <form id="general-data-affiliate" onsubmit="return false;">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Nombres</label>
              <input type="text" class="form-control" value="<?= $af->names_affiliate ?>"  name="names_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Apellidos</label>
              <input type="text" class="form-control" value="<?= $af->last_names_affiliate ?>"  name="last_names_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">DNI</label>
              <input type="text" class="form-control" value="<?= $af->dni_affiliate ?>"  name="dni_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Sexo</label>
              <select name="gener_affiliate" id="gener_affiliate" class="form-control">
                <option value="">Sexo</option>
                <option value="Masculino" <?php if ($af->gener_affiliate == "Masculino"): ?>selected<?php endif ?>>Masculino</option>
                <option value="Femenino" <?php if ($af->gener_affiliate == "Femenino"): ?>selected<?php endif ?>>Femenino</option>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Fecha de Nacimiento</label>
              <input type="date" class="form-control" value="<?= $af->date_age_affiliate ?>"  name="date_age_affiliate" id="date_nacimiento">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Edad</label>
              <input type="text" class="form-control" value="<?= $af->age_affiliate ?>" id="age_affiliate"  name="age_affiliate" readonly>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Número de Calle</label>
              <input type="text" class="form-control" value="<?= $af->number_affiliate ?>"  name="number_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Calle</label>
              <input type="text" class="form-control" value="<?= $af->street_affiliate ?>"  name="street_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Dpto</label>
              <input type="text" class="form-control" value="<?= $af->apartment_affiliate ?>"  name="apartment_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Barrio</label>
              <input type="text" class="form-control" value="<?= $af->dtto_affiliate ?>"  name="dtto_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Codigo Postal</label>
              <input type="text" class="form-control" value="<?= $af->postal_code_affiliate ?>"  name="postal_code_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Estudios</label>
              <input type="text" class="form-control" value="<?= $af->studies_affiliate ?>"  name="studies_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Trabajo</label>
              <input type="text" class="form-control" value="<?= $af->job_affiliate ?>"  name="job_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Circuito</label>
              <input type="text" class="form-control" value="<?= $af->circuit_affiliate ?>"  name="circuit_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Comuna</label>
              <input type="text" class="form-control" value="<?= $af->commune_affiliate ?>"  name="commune_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for=""></label>
              <a href="#" class="btn btn-success" style="margin-top: 26px;" onclick="saveGeneralData(<?= $af->id_affiliate ?>);">Actualizar Datos</a>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="tab-pane" id="home">
      <form onsubmit="return false;" id="contact-data-affiliate">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Telefono 1</label>
              <input type="text" class="form-control" value="<?= $af->phone_1_affiliate ?>"  name="phone_1_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Telefono 2</label>
              <input type="text" class="form-control" value="<?= $af->phone_2_affiliate ?>"  name="phone_2_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Comprobado</label>
              <input type="text" class="form-control" value="<?= $af->checked_phone ?>"  name="checked_phone">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Movil 1</label>
              <input type="text" class="form-control" value="<?= $af->mobile_1_affiliate ?>"  name="mobile_1_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Movil 2</label>
              <input type="text" class="form-control" value="<?= $af->mobile_2_affiliate ?>"  name="mobile_2_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Movil 3</label>
              <input type="text" class="form-control" value="<?= $af->mobile_3_affiliate ?>"  name="mobile_3_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Movil 4</label>
              <input type="text" class="form-control" value="<?= $af->mobile_4_affiliate ?>"  name="mobile_4_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Movil 5</label>
              <input type="text" class="form-control" value="<?= $af->mobile_5_affiliate ?>"  name="mobile_5_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Movil 6</label>
              <input type="text" class="form-control" value="<?= $af->mobile_6_affiliate ?>"  name="mobile_6_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Facebook</label>
              <input type="text" class="form-control" value="<?= $af->facebook_affiliate ?>"  name="facebook_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Twitter</label>
              <input type="text" class="form-control" value="<?= $af->twitter_affiliate ?>"  name="twitter_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Instagram</label>
              <input type="text" class="form-control" value="<?= $af->instagram_affiliate ?>"  name="instagram_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Email</label>
              <input type="text" class="form-control" value="<?= $af->email_affiliate ?>"  name="email_affiliate">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for=""></label>
              <a href="#" class="btn btn-success" style="margin-top: 26px;" onclick="saveContactData(<?= $af->id_affiliate ?>)">Actualizar Datos</a>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="tab-pane" id="messages">
      <form onsubmit="return false;" id="categorizacion-data-affiliate">
        <div class="row">
          <div class="col-md-6">
            <label for="">Fecha de Contacto</label>
            <input type="date" class="form-control" required  name="date_contact_affiliate">
            <!-- value="<?= date_format(date_create($af->date_contact_affiliate),'Y-m-d') ?>" -->
          </div>
          <div class="col-md-6">
            <label for="">Canal de Contacto</label>
            <select name="contact_id" id="contact_id" class="form-control">
              <option value="">Seleccionar</option>
              <?php foreach ($tipo_contacto as $tp): ?>
                <option value="<?= $tp->id_contacts_type ?>"<?php if ($af->contact_id == $tp->id_contacts_type): ?>selected<?php endif ?>><?= $tp->name_contacts_type ?> </option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="col-md-6">
            <label for="">Estatus Afiliafo</label>
            <select name="status_affiliate" id="status_affiliate" class="form-control">
              <option value="">Seleccionar</option>
              <?php foreach ($status as $st): ?>
                <option value="<?= $st->id_status ?>" <?php if ($af->status_affiliate == $st->id_status): ?>selected<?php endif ?>><?= $st->name_status ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="col-md-12">
            <label for="">Comentarios</label>
            <textarea name="comments_affiliate" rows="3" class="form-control coments" required><?= $af->comments_affiliate ?></textarea>
          </div>
          <div class="col-md-12">
            <button class="btn btn-success" style="margin-top: 60px;" onclick="saveCategorizacionData(<?= $af->id_affiliate ?>)">Guardar</button>
          </div>
        </div>
      </form>
    </div>
    <div class="tab-pane" id="asignados">
      <div class="table-responsive">
        <table id="asTable" class="table table-striped">
          <thead>
            <th>Nombre de Responsable</th>
            <th>DNI Responsable</th>
            <th>Email Responsable</th>
            <?php if($_SESSION['type_user'] != 3) : ?>
              <th>Quitar</th>
            <?php endif ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($vendors as $key => $vendor) : ?>
            <tr id="<?= $vendor->id_vendor?>">
              <td><?= $vendor->name_vendor ?></td>
              <td><?= $vendor->dni_vendor ?></td>
              <td><?= $vendor->email_vendor ?></td>
              <?php if($_SESSION['type_user'] != 3) : ?>
                <td><a href="#" title="Eliminar/Inhabilitar" style="margin-right: 10px;" onclick="removeResp(<?= $vendor->id_vendor ?>,<?= $af->id_affiliate ?>);"><i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i></a>
                </td>
              <?php endif ?>
            </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </div>

    <?php if($_SESSION['type_user'] != 3) : ?>
      <div class="col-md-12">
        <a href="#" class="btn btn-success" onclick="asignaResponsable2(<?= $af->id_affiliate ?>)"  id="but">Nuevos Responsables</a>
      </div>

                    <!--<div id="contentAsignaResponsable" style="margin-top: 80px;">
                        
                    </div>-->

                    <div id="contentAsignaResponsable2" style="margin-top: 80px;">

                    </div>

                  <?php endif ?>
                </div>


                <div class="tab-pane" id="actividades">
                  <form id="mailAfiliado" method="post">
                    <div class="row">
                     <div class="col-md-12 form-group">
                      <label> Selecciona la Actividad </label>
                      <input type="hidden" name="id_affiliate" value="<?= $af->id_affiliate?>">
                      <select name="activity_id" class="form-control">
                        <option value="">-- Seleccione --</option>
                        <?php foreach ($activities as $activity) : ?>
                          <option value="<?= $activity->id_activity?>">
                            <?= $activity->name_activity; ?>
                          </option>
                        <?php endforeach?>
                      </select>
                    </div>
                    
                    <div class="col-md-12 form-group">
                      <div class="col-md-4">
                        <button type="submit" class="btn btn-success">
                          <i class="fa fa-envelope"></i>  Enviar Invitación
                        </button>
                        <div class="enviando" style="display: none">
                          <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>
                        </div>
                      </div>
                      <div class="col-md-8">
                        <select name="invitation_type" id="invitation_type" class="form-control">
                          <option value="">-- Seleccione --</option>
                          <option value="1">Invitado</option>
                          <option value="2">Confirmado</option>
                          <option value="3">No invitado aun</option>
                        </select>
                      </div>


                            <!-- <div class="col-md-12 form-group">
                                <button type="button" class="btn btn-success" onclick="asignaAfiliados();">
                                    Asociar con
                                </button>
                                   <div class="enviando" style="display: none">
                                    <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>
                                </div>
                              </div> -->


                              <div id="contentAsignaAfiliados" style="margin-top: 80px;">

                              </div>


                            </div> 
                          </div>
                        </form>
                      </div>



                      <div class="tab-pane" id="afiliados">
                        <div class="row">
                          <div class="col-md-6">
                            <h5 class="text-center">Lista de Afiliados</h5>
                            <table class="table" id="table_affiliate">
                              <thead>
                                <th>Nombres</th>
                                <th>Dni</th>
                                <th>Agregar</th>
                              </thead>
                              
                            </table>
                          </div>

                          <div class="col-md-6">
                            <h5 class="text-center">Ya asociado con</h5>
                            <table class="table" id="table_affiliate_associate">
                              <thead>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Quitar</th>
                              </thead>
                            </table>
                          </div>
                        </div>
                      </div>



                      <div class="tab-pane" id="interests">
                          <div class="white-box">
                            <div class="col-md-6" id="div_interests">
                              
                            </div>    
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="">Otro Campo</label>
                                <input type="text" class="form-control" value=""  name="other_field" id="other_field">
                              </div>
                            </div>
                            <div class="form-group">
                              <button class="btn btn-primary" type="button" id="btn_add_interest">Enviar</button>
                            </div>
                            <br>
                            <br>
                        </div>
                      </div>

                      <?php if(!empty($af->twitter_affiliate)) : ?>       
                        <div class="tab-pane" id="twitter">
                          <div class="row">
                            <div class="col-md-12 form-group">
                              <a class="twitter-timeline" href="<?= 'https://twitter.com/'.$af->twitter_affiliate.'?lang=es'?>">Tweets</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div>
                          </div>
                        </div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <a href="<?= 'https://www.messenger.com/t/'. $af->facebook_affiliate ?>" class="btn btn-facebook" target="_blank" title="<?= 'https://www.facebook.com/messages/t/'. $af->facebook_affiliate ?>"><i class="fa fa-facebook"></i> ENVIAR MENSAJE</a> 

                  <a href="http://twitter.com/<?= $af->twitter_affiliate ?>" class="btn btn-twitter" target="_blank" title="<?= $af->twitter_affiliate ?>"><i class="fa fa-twitter"></i></a> 

                  <a href="http://instagram.com/<?= $af->instagram_affiliate ?>" class="btn btn-instagram" target="_blank" title="<?= $af->instagram_affiliate ?>"><i class="fa fa-instagram"></i></a> 

                  <a href="mailto: <?= $af->email_affiliate?>" class="btn btn-github" style="margin-right: 80px;" title="<?= $af->email_affiliate?>"><i class="fa fa-envelope"></i></a>

                  <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar Ventana</button>
                </div>

              <?php endforeach ?>
            </div>
          </div>
        </div>
        <!-- .row -->

        <script>
    var table_affiliate_associate;
    var table_affiliate;
    $(document).ready(function() {
        listInterest();

        $('.data-table').DataTable({
          "lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
        });

        table_affiliate_associate = $("#table_affiliate_associate").DataTable({
          "lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]],
          "type": "POST",
          'ajax': '<?= base_url("getAssociates") ?>'+'/<?= $id_affiliate ?>',
          'orders': []
        });

        table_affiliate = $("#table_affiliate").DataTable({
          "lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]],
          "type": "POST",
          'ajax': '<?= base_url("getAffiliates") ?>'+'/<?= $id_affiliate ?>',
          'orders': []
        });




        $("#table_affiliate").on('click', '.btn-add-affiliate', function(event) {
          params = {'id_affiliate' : '<?= $id_affiliate ?>', 'id_associate' : $(this).val()};
          
            $.ajax({
              url: '<?= base_url("addAssociate") ?>',
              type: 'POST',
              data: params,
              success: function (data) {
                table_affiliate_associate.ajax.reload(null, false); 
                table_affiliate.ajax.reload(null, false); 
              }
            });
        });

        $("#table_affiliate_associate").on('click', '.btn-remove-associate', function(event) {
          params = {'id_affiliate' : '<?= $id_affiliate ?>', 'id_associate' : $(this).val()};
          
            $.ajax({
              url: '<?= base_url("deleteAssociates") ?>',
              type: 'POST',
              data: params,
              success: function (data) {
                table_affiliate_associate.ajax.reload(null, false); 
                table_affiliate.ajax.reload(null, false); 
              }
            });
        });

        $('#btn_add_interest').click(function(event) {
           interests=arrayInterest();
           params = {'id_affiliate' : '<?= $id_affiliate ?>', 'interests' : interests, 'other_field' : $('#other_field').val()};
           $.ajax({
              url: '<?= base_url("addAssociateInterest") ?>',
              type: 'POST',
              data: params,
              success: function (data) {
                listInterest();
                $('#other_field').val(null);
              }
            });
        });
    });

    function arrayInterest(){
      arreglo=[];
      $.each($('.check-interests'), function(index, val) {
          if ($(this).prop('checked')) {
            arreglo.push({'val': $(this).val(), 'is_checked': true});
          }else{
            arreglo.push({'val': $(this).val(), 'is_checked': false});
          }
      });
      return arreglo;
    }

    function listInterest(){
      $.ajax({
        url: '<?= base_url("listInterestChecked") ?>/<?= $id_affiliate ?>',
        type: 'GET',
        success: function (data) {
          info=JSON.parse(data);
          $('#div_interests').html(info.html);
        }
      });
    }
</script>

