
				<div class="table-responsive">
                    <table id="asignaTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Vendedor</th>
                                <th>DNI</th>
                                <th>Email</th>
                                <?php if(!isset($delete)):?>
                                    <th>Asignar</th>
                                <?php else: ?>
                                    <th> Status </th>
                                    <th>Acciones</th>
                                <?php endif ?>
                                
                            </tr>   
                        </thead>

                       
                        <tbody>
                            <?php foreach ($responsables as $key): ?>
                            <tr>
                                <td><?= $key->name_vendor ?></td>
                                <td><?= $key->dni_vendor ?></td>
                                <td><?= $key->email_vendor ?></td>
                                <?php if(isset($actividad) && isset($delete)):?>
                                    <td>
                                        <?php if($key->status== 1 ) : ?>
                                           <span class="label label-success"> Habilitado </span> 
                                        <?php else: ?>
                                            <span class="label label-danger"> Esperando Habilitación </span>
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                                <td>
                                    <?php if (isset($actividad) && !isset($delete)) :?>

                                        <a href="#" title="Asignar" style="margin-right: 10px;" onclick="addVendor(<?= $actividad ?>,<?= $key->id_vendor ?>);"><i class="fa fa-check" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i></a>

                                    <?php elseif (isset($afiliado)): ?>

                                        <a href="#" title="Asignar" style="margin-right: 10px;" onclick="asignaResponsable(<?= $afiliado ?>,<?= $key->id_vendor ?>);"><i class="fa fa-check" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i></a>

                                    <?php elseif (isset($actividad) && isset($delete)): ?>
                                        <a href="#" title="Retirar" style="margin-right: 10px;" onclick="quitVendor(<?= $actividad ?>,<?= $key->id_vendor ?>);"><i class="fa fa-times" style="background: #fb9678; padding: 12px;color: white !important; border-radius: 5px;"></i></a>
                                        <?php if($key->status== 0 ) : ?>
                                        <a href="#" title="Aprobar" style="margin-right: 10px;" onclick="aprobeVendor(<?= $actividad ?>,<?= $key->id_vendor ?>);"><i class="fa fa-check" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i></a>
                                        <?php endif; ?>
                                    <?php endif?>
                                </td>
                            </tr>   
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>

                <script>
            		$(document).ready(function() {
						$('#asignaTable').DataTable({
							"lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
						});
					});

                    function asignaResponsable(afiliado,responsable) 
                    {
                        params = {'vendor_id' : responsable , 'affiliate_id' : afiliado};
                        $.ajax({
                            url: '<?= base_url("addResponsable") ?>',
                            type: 'POST',
                            data: params,
                            success: function (data) {
                                info = JSON.parse(data);
                                $("#but").attr("hidden","true");
                                swal({   
                                        title: info.message,     
                                        showConfirmButton: true 
                                    });

                                if (info.status == 'success') {
                                    addRowResponsables(afiliado,responsable,data);
                                    //$("#contentAsignaResponsable").html("");
                                    cargar($("#contentAsignaResponsable"),'<?= base_url("asignaResponsable") ?>/'+afiliado); 
                                }

                            }  
                        });
                    }

                    function addRowResponsables (afiliado,responsable,data) 
                    {
                        console.info(data);
                        all = JSON.parse(data);
                        //$("#asTable tr:last").remove();
                        var tds = '<tr>';
                        
                        tds += '<td>'+all.vendor[0].name_vendor+'</td>';
                        tds += '<td>'+all.vendor[0].dni_vendor+'</td>';
                        tds += '<td>'+all.vendor[0].email_vendor+'</td>';
                        
                        tds += '<td><a href="#" title="Eliminar/Inhabilitar" style="margin-right: 10px;" onclick="removeResp('+afiliado+','+responsable+');"><i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i></a></td>'
                        tds += '</tr>';
                        $("#asTable").append(tds);
                        //$('button.close').click();
                    }


                    function addVendor(actividad, vendor)
                    {
                        params = {'id_activity' : actividad , 'id_vendor' : vendor};
                        $.ajax({
                            url: '<?= base_url("addActivityVendor") ?>',
                            type: 'POST',
                            data: params,
                            beforeSend: function(){
                               swal({
                                    title: 'Enviando notificación al Responsable',
                                    buttons: false,
                                    showConfirmButton: false,
                                    text: ''
                                });
                            },
                            success: function (data) {
                                info = JSON.parse(data);
                                
                                swal({   
                                        title: info.message,     
                                        showConfirmButton: true 
                                    });

                                if (info.status == 'success') {
                                    $('.modal-content').location.reload();
                                }

                            }  
                        });
                    }

                    function quitVendor(actividad, vendor)
                    {
                        params = {'id_activity' : actividad , 'id_vendor' : vendor};
                        $.ajax({
                            url: '<?= base_url("quitActivityVendor") ?>',
                            type: 'POST',
                            data: params,
                            success: function (data) {
                                info = JSON.parse(data);
                                
                                swal({   
                                        title: info.message,     
                                        showConfirmButton: true 
                                    });
                                $('button.close').click();
                            }
                        });
                    }

                    function aprobeVendor(actividad, vendor)
                    {
                        params = {'id_activity' : actividad , 'id_vendor' : vendor};
                        $.ajax({
                            url: '<?= base_url("aprobeActivityVendor") ?>',
                            type: 'POST',
                            data: params,
                            success: function (data) {
                                info = JSON.parse(data);
                                
                                swal({   
                                        title: info.message,     
                                        showConfirmButton: true 
                                    });
                                $('button.close').click();
                            }
                        });
                    }

                </script>


