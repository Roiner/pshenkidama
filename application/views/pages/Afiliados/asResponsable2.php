
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h3>Responsables</h3>
			</div>

			<form method="post" id="saveAsignaResponsable2">
				
				<div class="form-group">
					<input type="hidden" name="afiliado" value="<?= $afiliado?>">
					<input type="checkbox" id="checkbox"> Todos
					<select name="responsables[]" id="responsables" class="form-control" multiple="true">
					
						<?php foreach($responsables as $key => $res):?>
							<option value="<?= $res->id_vendor?>">
								<?= $res->name_vendor?>
							</option>
						<?php endforeach; ?>

					</select>
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-success">Asignar Responsables</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script>
	$('#responsables').select2({
        allowClear: true,
        closeOnSelect: false,
        placeholder: '-- Seleccionar Responsables --',
    });

    $("#checkbox").click(function(){
	    if($("#checkbox").is(':checked') ){
	        $("#responsables > option").prop("selected","selected");
	        $("#responsables").trigger("change");
	    }else{
	        $("#responsables > option").removeAttr("selected");
	         $("#responsables").trigger("change");
	     }
    });

    $('form#saveAsignaResponsable2').on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();

                $.ajax({
                    method: "POST",
                    url: "<?= base_url("saveAsignaResponsable2")?>",
                    data: new FormData(this),
                    processData : false,
                    contentType : false,
                    type: 'json',
                })
                .done(function (data){
                    info = $.parseJSON(data);
                    swal({
                        title: info.message,
                        icon: "success",
                        button: "ok",
                    });
                    
                    $('button.confirm').click(function(){
                    	window.location.reload();
                    });
                   
                });

            });
</script>