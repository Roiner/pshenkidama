        <style>
           .loader{
           visibility:hidden;
           }
       </style>

            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Agregar Afiliado</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Nuevo Afiliado</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Nuevo Afiliado</h3>
                            <small>Por Favor descargue el archivo ejemplo de importación </small>
                            <br>
                                <a href="<?= base_url();?>files/afiliado.xls" class="btn btn-primary" style="margin-top: 20px;">Descargar Excel Demo</a>
                            <br>
                            <br>
                            
                            <form enctype="multipart/form-data" id="uploadExcel">
                                <div class="form-group">
                                    <input type="file" id="input-file-now" class="dropify" name="file" data-validation="required" data-validation-error-msg="El campo es requerido" accept=".xls"/>
                                </div>

                                <button type="submit" class="btn btn-success"> Enviar </button>  <i class="fa fa-spin fa-refresh loader fa fa-2x" aria-hidden="true"> </i> 
                            </form>

                           
                            <br>

                            <br><br>
                            <small>Recordar que si no va a agregar valor a alguna columna, dejar por defecto (0), para asi evitar inconsistencias<br>Si existen filas en el documento iguales o similares a la base de datos, estas filas serán actualizadas para no perder la información</small>
                        </div>
                    </div>
                </div>
                <!-- .row -->
            
                <div id="content"></div>
                </div>    

            
            <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>


                <script>

                    $('form#uploadExcel').on('submit', function(ev){

                        ev.preventDefault();
                        ev.stopPropagation();

                        $.ajax({
                            method: "post",
                            url: "<?= base_url("Afiliado/uploadExcel");?>",
                            data: new FormData(this),
                            processData : false,
                            contentType : false,
                            type: 'json',
                            beforeSend: function(){
                                $('.loader').css("visibility", "visible");
                            },
                        })

                        .done(function(data){
                            
                            info = $.parseJSON(data);
                            console.info(data);
                            $('.loader').css("visibility", "hidden");

                            if (info.status == 'success') {
                                swal({
                                  title: info.message,
                                  icon: "success",
                                  button: "ok",
                                });    
                            }else{
                                swal({
                                  title: info.message,
                                  icon: "warning",
                                  button: "ok",
                                });    
                            }
                        });
                    });



                    /*function saveAfiliado() 
                    {
                        var archivos = document.getElementById('input-file-now');
                        
                        var archivo = archivos.files;

                        var arch = new FormData();
                        
                        if (archivo.length > 0)
                        {
                            for (var i = 0; i < archivo.length; i++) 
                            {
                                arch.append('archivo'+i,archivo[i]);
                            }

                            $.ajax({
                                url: '<?= base_url("readAfiliado") ?>',
                                type:'POST',
                                contentType:false,
                                data:arch,
                                processData:false,
                                cache:false,
                                success: function (data) {
                                    window.alert("Archivo Leido Correctamente");
                                    $('#content').html(data);
                                }
                            });
                        } else {
                            alert("Debe Arrastrar o Seleccionar un Archivo");
                        }
                    }*/
                </script>

