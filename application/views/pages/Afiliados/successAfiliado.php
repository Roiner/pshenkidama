



			<div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <h3 class="box-title">Confirma Afiliado</h3>
                        
                        <form action="#" method="POST" id="data-afiliado">
							<div class="form-body">
                                <hr>
                                <div class="row">
                            		<?php foreach ($values as $key): ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
												<label for="nombre">Nombres de Afiliado</label>
												<input type="text" readonly value="<?= $key['A'] ?>" name="names_affiliate" class="form-control">
											</div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Apellidos Afiliado</label>
                                                <input type="text" readonly value="<?= $key['B'] ?>" name="last_names_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">DNI Afiliado</label>
                                                <input type="text" readonly value="<?= $key['C'] ?>" name="dni_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Calle</label>
                                                <input type="text" readonly value="<?= $key['D'] ?>" name="street_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Numero</label>
                                                <input type="text" readonly value="<?= $key['E'] ?>" name="number_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Distrito</label>
                                                <input type="text" readonly value="<?= $key['F'] ?>" name="dtto_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Codigo Postal</label>
                                                <input type="text" readonly value="<?= $key['G'] ?>" name="postal_code_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Trabajo Afiliado</label>
                                                <input type="text" readonly value="<?= $key['H'] ?>" name="job_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Estudios Afiliado</label>
                                                <input type="text" readonly value="<?= $key['I'] ?>" name="studies_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Denominacion</label>
                                                <input type="text" readonly value="<?= $key['J'] ?>" name="denomination_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Circuito</label>
                                                <input type="text" readonly value="<?= $key['K'] ?>" name="circuit_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Comuna</label>
                                                <input type="text" readonly value="<?= $key['L'] ?>" name="commune_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Genero</label>
                                                <input type="text" readonly value="<?= $key['M'] ?>" name="gener_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Edad</label>
                                                <input type="text" readonly value="<?= $key['N'] ?>" name="age_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Facebook</label>
                                                <input type="text" readonly value="<?= $key['O'] ?>" name="facebook_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Twitter</label>
                                                <input type="text" readonly value="<?= $key['P'] ?>" name="twitter_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Instagram</label>
                                                <input type="text" readonly value="<?= $key['Q'] ?>" name="instagram_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="text" readonly value="<?= $key['R'] ?>" name="email_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Telefono 1</label>
                                                <input type="text" readonly value="<?= $key['S'] ?>" name="phone_1_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Telefono 2</label>
                                                <input type="text" readonly value="<?= $key['T'] ?>" name="phone_2_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Movil 1</label>
                                                <input type="text" readonly value="<?= $key['U'] ?>" name="mobile_1_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Movil 2</label>
                                                <input type="text" readonly value="<?= $key['V'] ?>" name="mobile_2_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Movil 3</label>
                                                <input type="text" readonly value="<?= $key['W'] ?>" name="mobile_2_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Movil 4</label>
                                                <input type="text" readonly value="<?= $key['X'] ?>" name="mobile_4_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Movil 5</label>
                                                <input type="text" readonly value="<?= $key['Y'] ?>" name="mobile_5_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Movil 6</label>
                                                <input type="text" readonly value="<?= $key['Z'] ?>" name="mobile_6_affiliate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Comentarios</label>
                                                <input type="text" readonly value="<?= $key['AA'] ?>" name="comments_affiliate" class="form-control">
                                            </div>
                                        </div>
									<?php endforeach ?>
                                	
									
                                    <!--/span-->
                                </div>
                                <div class="row">
                                	<span class="pull-right"><a href="#" class="btn btn-success" onclick="confirmaSaveAfiliado();">Guardar</a></span>
                                </div>
                            </div>
						</form>
                    </div>
                </div>                    
            </div>




<script>
	function confirmaSaveAfiliado() 
	{
		$.ajax({
          url: '<?= base_url("confirmaSaveAfiliado") ?>',
          type: 'POST',
          data: $("#data-afiliado").serialize(),
          success: function () {
                window.alert("Afiliado Guardado");
                window.location.reload();
            }
        });
	}
</script>