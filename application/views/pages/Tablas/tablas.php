
        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Usuarios del Sistema </h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Gestion de Usuarios - Administradores</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-12 col-xs-12">
                        <div class="white-box">
                            <!-- Nav tabs -->
                            
                            <div class="col-md-12">
                                <div class="white-box">
                                    <div class="table-responsive">
                                        <table id="myTable" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Email</th>
                                                    <th>Telefono</th>
                                                    <th>Tipo de Usuario</th>
                                                    <th>Status Usuario</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($administradores as $key): ?>
                                                    <tr>
                                                        <td><?= $key->name_admin ?></td>
                                                        <td><?= $key->email_admin ?></td>
                                                        <td><?= $key->phone_admin ?></td>
                                                        <td>Administrador</td>
                                                        <td><span class="label label-table" style="background-color: <?= $key->color_status ?>"><?= $key->name_status ?></span></td>
                                                        <td>
                                                            <a href="#modalAdmin" style="margin-right: 10px;" data-toggle="modal" data-target="#modalAdmin" onclick="cargar('#allModal','<?= base_url('viewAdmin/'.$key->id_admin) ?>');" title="Ver / Editar">
                                                                <i class="fa fa-search" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                            </a> 
                                                            <?php if ($key->status_id == 1): ?>
                                                            <a href="#" style="margin-right: 10px;" title="Inhabilitar / Eliminar" onclick="disableAdmin(<?= $key->id_admin ?>);">
                                                                <i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                            </a>                                                                
                                                            <?php endif ?>
                                                            <?php if ($key->status_id == 2): ?>
                                                            <a href="#" style="margin-right: 10px;" title="Habilitar" onclick="enableAdmin(<?= $key->id_admin ?>);">
                                                                <i class="fa fa-check text-danger" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                            </a>                                                                
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <a href="#modalAdmin" class="btn btn-success" data-toggle="modal" data-target="#modalAdmin" onclick="cargar('#allModal','<?= base_url('newAdmin') ?>');">Registrar Administrador</a>
                            </div>
                            <div class="clearfix"></div>
                                
                            
                        </div>
                    </div>
                </div>
                <!-- .row -->
                
               
                <div class="modal fade" id="modalAdmin">
                    
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

                        <div id="allModal"></div>
                        </div>
                    </div>
                </div>



                <script>
                    function disableAdmin(admin) 
                    {
                        params = {'id_admin' : admin};
                        $.ajax({
                            url: '<?= base_url() ?>disableAdmin/'+admin,
                            type: 'POST',
                            data: params,
                            success: function (data) {
                                 info = $.parseJSON(data);
                                swal({   
                                    title: info.message,
                                    icon: "success",
                                    button: "ok",
                                    closeOnEsc: false,
                                    closeOnClickOutside: false
                                });
                                
                                $('button.confirm').click(function(event) {
                                    location.reload();
                                });
                            }  
                        });    
                    }

                    function enableAdmin(admin) 
                    {
                        params = {'id_admin' : admin};
                        $.ajax({
                            url: '<?= base_url() ?>enableAdmin/'+admin,
                            type: 'POST',
                            data: params,
                            success: function (data) {
                                info = $.parseJSON(data);
                                swal({   
                                    title: info.message,
                                    icon: "success",
                                    button: "ok",
                                    closeOnEsc: false,
                                    closeOnClickOutside: false
                                });
                                
                                $('button.confirm').click(function(event) {
                                    location.reload();
                                });
                            }  
                        });    
                    }
                </script>