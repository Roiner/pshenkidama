




                        <h4 class="modal-title">Registrar un Status</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" onsubmit="return false;" id="form_save_status" role="form">
                            <div class="form-group">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" required>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="control-label">Descripcion</label>
                                <input type="text" class="form-control" id="descripcion" name="descripcion" required>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label">Color informativo de Status</label>
                                <input type="text" class="colorpicker form-control" value="#7ab2fa" name="color" />
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" id="but" class="btn btn-success waves-effect waves-light" onclick="save_status()">Guardar</button>
                    </div>

            <script>
                $(".colorpicker").asColorPicker();
                function save_status() 
                {
                    $.ajax({
                      url: '<?= base_url("saveStatus") ?>',
                      type: 'POST',
                      data: $("#form_save_status").serialize(),
                      success: function () {
                            window.alert("Status Guardado");
                            window.location.reload();
                        }
                    });
                }

            </script>