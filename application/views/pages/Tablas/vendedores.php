
        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Usuarios del Sistema Responsables</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Gestion de Usuarios</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-12 col-xs-12">
                        <div class="white-box">
                            <!-- Nav tabs -->
                            
                            <div class="col-md-12">
                                <div class="white-box">
                                    <div class="table-responsive">
                                        <table id="myTable" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Email</th>
                                                    <th>Telefono</th>
                                                    <th>Contacto</th>
                                                    <th>Status Usuario</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($vendedores as $key): ?>
                                                    <tr>
                                                        <td><?= $key->name_vendor ?></td>
                                                        <td><?= $key->email_vendor ?></td>
                                                        <td><?= $key->phone_vendor ?></td>
                                                        <td>
                                                            <?php 
                                                                $cont_phone = 0;

                                                                if ($key->phone_vendor != 0) {
                                                                   $cont_phone++;
                                                                }

                                                                if ($cont_phone > 0) {
                                                            ?>
                                                                    <div class="pull-left" style="margin-right: 6%; position: relative;">
                                                                        <img src="<?= base_url()?>images/phone.png" alt="" width="20" heigt="20">
                                                                        <span class="badge" style=" position: absolute;
                                                                                                    font-size: 8px;
                                                                                                    top: -7px;
                                                                                                    left: 11px;
                                                                                                    color: #777;
                                                                                                    background: #fff;"><?= $cont_phone?>
                                                                        </span>
                                                                    </div>
                                                            <?php
                                                                }

                                                             ?>

                                                             <?php if ($key->email_vendor != ""): ?>
                                                                <div class="pull-left" style="margin-right: 6%"><img src="<?= base_url()?>images/gmail.png" alt="" width="20" heigt="20"></div>
                                                             <?php endif ?>
                                                        </td>
                                                        <td><span class="label label-table" style="background-color: <?= $key->color_status ?>"><?= $key->name_status ?></span></td>
                                                        <td>
                                                            <div class="btn-group" role="group">
                                                                <a href="#modalAdmin" style="margin-right: 10px;" data-toggle="modal" data-target="#modalAdmin" onclick="cargar('#allModal','<?= base_url('viewResponsable/'.$key->id_vendor) ?>');" title="Ver / Editar">
                                                                    <i class="fa fa-search" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                                </a> 
                                                                <?php if ($key->status_id == 1): ?>
                                                                <a href="#" style="margin-right: 10px;" title="Inhabilitar " onclick="disableVendor(<?= $key->id_vendor ?>);">
                                                                    <i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                                </a>                                                                
                                                                <?php endif ?>
                                                                <?php if ($key->status_id != 1): ?>
                                                                <a href="#" style="margin-right: 10px;" title="Habilitar" onclick="enableVendor(<?= $key->id_vendor ?>);">
                                                                    <i class="fa fa-check text-danger" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                                </a>
                                                                <?php endif ?>

                                                                <a href="#" style="margin-right: 10px;");" title="Entrar Como" onclick="enterAs(<?= $key->id_vendor ?>)">
                                                                    <i class="fa fa-unlock" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                                </a>


                                                                 <a onclick="delete_vendor(<?= $key->id_vendor ?>, this)" title="Eliminar" style="margin-right: 2px">
                                                                <i class="fa fa-trash-o" style="background: #01c0c8; padding: 12px; margin-top: 0px;color: white !important; border-radius: 5px;"></i>
                                                            </a>

                                                        </div>


                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            
                        </div>
                    </div>
                </div>
                <!-- .row -->

                <div class="modal fade" id="modalAdmin">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content ">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

                                <div id="allModal"></div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                

<!-- jQuery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <script>       

        function enterAs (vendor_id)
        {
               $.ajax({
                    method: "POST",
                    url: "<?= base_url("findVendor")?>/"+vendor_id,
                    data: vendor_id,
                    //new FormData(this),
                    processData : false,
                    contentType : false,
                    type: 'json'
                })
                
                .done(function(data) {
                    info = $.parseJSON(data);
                    if (info.code == 200) {
                        swal({
                            title: info.message,
                            icon: "success",
                            button: "ok",
                            closeOnEsc: false,
                            closeOnClickOutside: false
                        });

                        $('button.confirm').click(function(event) {
                            window.location.replace("<?= base_url('misAfiliados');?>");
                        });
                        
                    }else{
                        swal({
                            title: info.message,
                            icon: "error",
                            button: "ok",
                        });
                    }
                    
                });
        }


        function enableVendor (vendor_id)
        {
            $.ajax({
                method: "POST",
                url: "<?= base_url();?>enableVendor/"+vendor_id,
                data: vendor_id,
                processData : false,
                contentType : false,
                type: 'json'
            })
            
            .done(function(data) {
                info = $.parseJSON(data);
                
                swal({
                    title: info.message,
                    icon: "success",
                    button: "ok",
                    closeOnEsc: false,
                    closeOnClickOutside: false
                });
                
                $('button.confirm').click(function(event) {
                    location.reload();
                });
            });
           
        }

        function disableVendor (vendor_id)
        {
            $.ajax({
                method: "POST",
                url: "<?= base_url();?>disableVendor/"+vendor_id,
                data: vendor_id,
                processData : false,
                contentType : false,
                type: 'json'
            })
            
            .done(function(data) {
                info = $.parseJSON(data);
                
                swal({
                    title: info.message,
                    icon: "success",
                    button: "ok",
                    closeOnEsc: false,
                    closeOnClickOutside: false
                });
                
                $('button.confirm').click(function(event) {
                    location.reload();
                });
            });
        }



        function delete_vendor(id, btn)
      {
          $.ajax({
          url: '<?= base_url() ?>tablas/delete_vendor/'+id,
          type: 'POST',
          data: id,
          success: function (data) {
            info = JSON.parse(data);         
              swal({   
                  title: info.message,     
                  showConfirmButton: true 
                });

              $(btn).closest("tr").remove();
            }
          });
      }



       
        

    </script>