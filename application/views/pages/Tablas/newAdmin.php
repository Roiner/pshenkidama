




                        <h4 class="modal-title">Registrar un Administrador</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" onsubmit="return false;" id="form_save_admin" role="form">
                            <div class="form-group">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" required>
                            </div>
                            <div class="form-group">
                                <label for="telefono" class="control-label">Telefono</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" required>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="control-label">Direccion</label>
                                <textarea name="direccion" id="direccion" rows="3" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="genero" class="control-label">Genero</label>
                                <select name="genero" id="genero" class="form-control" required>
                                    <option value="">Genero</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="username" class="control-label">Username</label>
                                <input type="text" class="form-control" id="username" name="username" required>
                            </div>
                            <div class="form-group">
                                <label for="pass" class="control-label">Clave</label>
                                <input type="password" class="form-control" id="pass" name="password" required>
                                <span id="msg" style="color:red;"></span>
                            </div>
                            <div class="form-group">
                                <label for="rpass" class="control-label">Repetir Clave</label>
                                <input type="password" class="form-control" id="rpass" onkeyup="validaPass();" required>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="but" class="btn btn-success waves-effect waves-light" onclick="save_admin()">Guardar</button>
                    </div>
                

            <script>
                function save_admin() 
                {
                    $.ajax({
                      url: '<?= base_url("saveAdmin") ?>',
                      type: 'POST',
                      data: $("#form_save_admin").serialize(),
                      success: function () {
                            swal({   
                                title: "Datos Almacenados",     
                                showConfirmButton: true 
                            });
                            window.location.reload();
                        }
                    });
                }

                function  validaPass() 
                {
                  var pass = $("#pass").val();
                  var pass1 = $("#rpass").val();
                  if (pass1 != pass) 
                  {
                    $("#msg").html("Las Contraseñas no Coinciden");
                    document.getElementById('but').disabled = true;
                  } else {
                    $("#msg").html("");
                    document.getElementById('but').disabled = false;
                  }
                }
            </script>