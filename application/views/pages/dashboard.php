            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Principal </h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Panel Escritorio</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel-group">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse1"> <b> Estadísticas </b> </a>
                          </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                          

                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                            <h5  style="color:<?= $status[5]->color_status ?>; font-weight: bold">Contactado</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[5]->color_status ?>"><?= $totales['C'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= ($totales['C']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['C']*100)/$totales['TOT'] ?>%; background-color: <?= $status[5]->color_status ?> !important"> <span class="sr-only">40% Complete (success)</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                            <h5 style="color:<?= $status[3]->color_status ?>; font-weight: bold">Posible Voto</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[3]->color_status ?>"><?= $totales['PV'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['PV']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['PV']*100)/$totales['TOT'] ?>%; background-color: <?= $status[3]->color_status ?> !important"> <span class="sr-only">40% Complete (success)</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                            <h5 style="color:<?= $status[6]->color_status ?>; font-weight: bold">Voto Confirmado</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[6]->color_status ?>"><?= $totales['VC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= ($totales['VC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['VC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[6]->color_status ?> !important">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
                                            <h5 style="color:<?= $status[4]->color_status ?>; font-weight: bold">Posible Candidato</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[4]->color_status ?>"><?= $totales['PC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= ($totales['PC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['PC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[4]->color_status ?> !important">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="row row-in">
                                <div class="col-lg-3 col-sm-6  row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
                                            <h5 style="color:<?= $status[7]->color_status ?>; font-weight: bold">Candidato Confirmado</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[7]->color_status ?>"><?= $totales['CC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['CC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['CC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[7]->color_status ?> !important">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
                                            <h5 style="color:<?= $status[8]->color_status ?>; font-weight: bold">Voto Contrario</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[8]->color_status ?>"><?= $totales['VCO'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= ($totales['VCO']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['VCO']*100)/$totales['TOT'] ?>%; background-color: <?= $status[8]->color_status ?>  !important" >  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
                                            <h5 style="color:<?= $status[9]->color_status ?>; font-weight: bold">Compañero</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[9]->color_status ?>"><?= $totales['CO'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['CO']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['CO']*100)/$totales['TOT'] ?>%; background-color: <?= $status[9]->color_status ?> !important">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
                                            <h5 style="color:<?= $status[10]->color_status ?>; font-weight: bold">No Contactado</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[10]->color_status ?>"><?= $totales['NC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?= ($totales['NC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['NC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[10]->color_status?> !important">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[5]->color_status ?>"> Contactado </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[3]->color_status ?>;"> Posible Voto </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[6]->color_status ?>"> Voto Confirmado </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[4]->color_status ?>"> Posible Candidato </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[7]->color_status ?>"> Candidato Confirmado </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[8]->color_status ?>"> Voto Contrario </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[9]->color_status ?>"> Compañero </span></label><br>
                                    <label><input type="checkbox" class="checkbox-inline " value="<?= $totales['C'] ?>"><span class="label label-table" style="background-color: <?= $status[10]->color_status ?>"> No Contactado </span></label><br>
                                </div>
                                <div class="col-md-3">
                                    hola
                                </div>
                                <div class="col-md-3">
                                    hola
                                </div>
                                <div class="col-md-3">
                                    hola
                                </div>
                                <!--  -->
                                <div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div>
        </div>
                <!--row -->
                <div class="row">
                    <h4></h4>
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Listado de Afiliados </h3>
                            <div class="table-responsive">
                                <table id="homeTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th> Apellido </th>
                                            <th>  </th>
                                            <th> Nombre </th>
                                            <th> Contacto </th>
                                            <th> DNI </th>
                                            <th> Sexo </th>
                                            <th> Comuna </th>
                                            <th> Categorizado </th>
                                            <th> Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($afiliados as $key): ?>
                                        <tr>
                                            <td><?= $key->last_names_affiliate ?></td>
                                            <td>

                                                <?php foreach ($assocs as  $assoc): ?>
                                                    <?php if ($assoc["id_affiliate"] == $key->id_affiliate): ?>
                                                        <div class="pull-left" style="margin-right: 6%; position: relative;">
                                                            <img src="<?= base_url()?>images/asocia.png" alt="" width="20" heigt="20">
                                                            <span class="badge" style=" position: absolute;
                                                                                        font-size: 8px;
                                                                                        top: -7px;
                                                                                        left: 11px;
                                                                                        color: #777;
                                                                                        background: #fff;"><?= $assoc["count"]?></span>
                                                        </div>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                                
                                            </td>
                                            <td><?= $key->names_affiliate ?></td>
                                            <td>
                                                <?php 
                                                    $cont_phone = 0;

                                                    if ($key->phone_1_affiliate != 0) {
                                                       $cont_phone++;
                                                    }
                                                    if ($key->phone_2_affiliate != 0) {
                                                       $cont_phone++;
                                                    }

                                                    if ($cont_phone > 0) {
                                                ?>
                                                        <div class="pull-left" style="margin-right: 6%; position: relative;">
                                                            <img src="<?= base_url()?>images/phone.png" alt="" width="20" heigt="20">
                                                            <span class="badge" style=" position: absolute;
                                                                                        font-size: 8px;
                                                                                        top: -7px;
                                                                                        left: 11px;
                                                                                        color: #777;
                                                                                        background: #fff;"><?= $cont_phone?>
                                                            </span>
                                                        </div>
                                                <?php
                                                    }

                                                 ?>

                                                 <?php if ($key->facebook_affiliate != ""): ?>
                                                     <div class="pull-left" style="margin-right: 6%"><img src="<?= base_url()?>images/facebook.png" alt="" width="20" heigt="20"></div>
                                                 <?php endif ?>
                                                 <?php if ($key->instagram_affiliate != ""): ?>
                                                     <div class="pull-left" style="margin-right: 6%"><img src="<?= base_url()?>images/instagram.png" alt="" width="20" heigt="20"></div>
                                                 <?php endif ?>

                                                 <?php 
                                                    $cont_whatsapp = 0;

                                                    if ($key->mobile_1_affiliate != 0) {
                                                       $cont_whatsapp++;
                                                    }
                                                    if ($key->mobile_2_affiliate != 0) {
                                                       $cont_whatsapp++;
                                                    }

                                                    if ($cont_whatsapp > 0) {
                                                ?>
                                                        <div class="pull-left" style="margin-right: 6%; position: relative;">
                                                            <img src="<?= base_url()?>images/whatsapp.png" alt="" width="20" heigt="20">
                                                             <span class="badge" style=" position: absolute;
                                                                                        font-size: 8px;
                                                                                        top: -7px;
                                                                                        left: 11px;
                                                                                        color: #777;
                                                                                        background: #fff;"><?= $cont_whatsapp?>
                                                        </div>
                                                <?php
                                                    }

                                                 ?>



                                                 <?php if ($key->twitter_affiliate != ""): ?>
                                                     <div class="pull-left" style="margin-right: 6%"><img src="<?= base_url()?>images/twitter.png" alt="" width="20" heigt="20"></div>
                                                 <?php endif ?>
                                                 <?php if ($key->email_affiliate != ""): ?>
                                                    <div class="pull-left" style="margin-right: 6%"><img src="<?= base_url()?>images/gmail.png" alt="" width="20" heigt="20"></div>
                                                 <?php endif ?>

                                                   <?php if ($key->comments_affiliate != ""): ?>
                                                     <div class="pull-left" style="margin-right: 6%"><img src="<?= base_url()?>images/chat.png" alt="" width="20" heigt="20"></div>
                                                 <?php endif ?>
                                            </td>
                                            <td><?= $key->dni_affiliate ?></td>
                                            <td><?= $key->gener_affiliate ?></td>
                                            <td>Comuna <?= $key->commune_affiliate ?></td>
                                            <td><span class="label label-table" style="background-color: <?= $key->color_status ?>"><?= $key->name_status ?></span></td>
                                            <td>
                                                <!--<a href="#" id="vendor" class="btn btn-success" onclick="vendor(<?= $key->id_affiliate;?>)"></a>-->
                                                <a href="#respModal" data-toggle="modal" data-target="#respModal" onclick="cargar('#viewResp','<?= base_url('viewAfiliado/'.$key->id_affiliate) ?>');" title="Ver" style="margin-right: 10px;">
                                                    <i class="fa fa-search" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                </a> 
                                                
                                                <?php if($_SESSION['type_user'] != 3) : ?>

                                                <a href="#" title="Eliminar/Inhabilitar" style="margin-right: 10px;" onclick="disabledAffiliate(<?= $key->id_affiliate ?>);">
                                                    <i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                </a>
                                                 <?php endif ?>
                                            </td>
                                        </tr>   
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                
               <div class="modal fade" id="respModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar Ventana">×</button>

                                <div id="viewResp"></div>
                        </div>
                        <!-- /.modal-content -->
                    </div>  
                </div>
        
              
                <script>

                    function disabledAffiliate(afiliado) 
                    {
                        params = {'id_affiliate' : afiliado};
                        $.ajax({
                            url: '<?= base_url("disabledAffiliate") ?>',
                            type: 'POST',
                            data: params,
                            success: function () {
                                swal({   
                                    title: "Afiliado Eliminado",     
                                    showConfirmButton: true 
                                });
                                window.location.reload();
                            }  
                        });    
                    }
              </script>