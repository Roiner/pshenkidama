
        <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Editar Interes</h4>
                        <div class="panel panel-info">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form id="form-edit-actividad" method="POST" action="<?= base_url("updateInteres")?>" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <hr>
                                            <div class="row">
                                                <input type="hidden" name="id" value="<?= $interest[0]->id ?>">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre</label>
                                                        <input type="text" name="name" class="form-control" placeholder="Titulo" id="name" data-validation="required" data-validation-error-msg="El campo es requerido" value=" <?= $interest[0]->name?> ">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                       
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success">Enviar</button>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->

