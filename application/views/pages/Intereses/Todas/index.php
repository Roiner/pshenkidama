

      <div class="container-fluid">
    
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Todos los Intereses</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Lista de Intereses</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="white-box">
                      <div class="table-responsive">
                        <table class="table table-striped table-hover"  id="myTable">
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>

                          <tbody>
                            <?php foreach ($allInterest as $key): ?>
                              <tr>
                                <td>
                                  <strong><?= $key->name ?></strong> 
                                </td>
                                <td>
                                  <?php if ($_SESSION['type_user'] != 3) : ?>

                                    <div class="btn-group" role="group">
                                      <a href="#" class="add " data-toggle="modal" data-target="#myModal" onclick="showInterest(<?= $key->id ?>)" title="Detalles de la Actividad" style="margin-right: 2px">
                                        <i class="fa fa-search" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                      </a>

                                      <a class="add " onclick="delete_interest(<?= $key->id ?>, this)" title="Eliminar" style="margin-right: 2px">
                                        <i class="fa fa-remove" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px; cursor: pointer;"></i>
                                      </a>

                                    </div>

                                  <?php endif; ?>

                                </td>
                              </tr>
                            <?php endforeach?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="white-box">
                      <h4>Campos abiertos por otros usuarios</h4>
                      <div class="table-responsive">
                        <table class="table table-striped table-hover"  id="myTable2">
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>Usuario</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>

                          <tbody>
                            <?php foreach ($allInterestUser as $key): ?>
                              <tr>
                                <td>
                                  <strong><?= $key->name ?></strong> 
                                </td>
                                <td>
                                  <strong><?= $key->name_user ?></strong> 
                                </td>
                                <td>
                                  <?php if ($_SESSION['type_user'] != 3) : ?>
                                    
                                    <div class="btn-group" role="group">
                                      <a href="#" class="add " data-toggle="modal" data-target="#myModal" onclick="showInterest(<?= $key->id ?>)" title="Detalles de la Actividad" style="margin-right: 2px">
                                        <i class="fa fa-search" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                      </a>
                                      
                                      <a class="add " onclick="delete_interest(<?= $key->id ?>, this)" title="Eliminar" style="margin-right: 2px">
                                        <i class="fa fa-remove" style="background: #01c0c8; padding: 8px; margin-top: -10px;color: white !important; border-radius: 5px; cursor: pointer;"></i>
                                      </a>

                                    </div>

                                  <?php endif; ?>

                                </td>
                              </tr>
                            <?php endforeach?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
            <div id="contentInteres">
                
            </div>
      </div>
     
    </div>
  </div>
</div>




<!-- Jquery -->
<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>



<script>
    
    $(document).ready(function() {
        $('#myTable').DataTable();
        $('#myTable2').DataTable();
    });
   
    function activityId(interest_id) 
    {
        cargar($("#contentInteres"),'<?= base_url("") ?>activityVendor/'+interest_id); 
    }

    function activityVendor(interest_id)
    {
        cargar($("#contentInteres"),'<?= base_url("") ?>showInterestVendor/'+interest_id);
    }

    function showInterest(interest_id)
    {
        cargar($("#contentInteres"),'<?= base_url("") ?>editInteres/'+interest_id);
    }

    /* Nuevo */

    function show(interest_id)
    {
        cargar($("#contentInteres"),'<?= base_url("") ?>showActividad/'+interest_id);
    }

    function showLog(interest_id)
    {
        cargar($("#contentInteres"),'<?= base_url("") ?>logActividad/'+interest_id); 
    }


    /*$(document).ready(function() {
        $('#myModal').on('hide.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
            location.reload();
        });
    });*/

      function addVendor(actividad, vendor)
      {
        params = {'id_activity' : actividad , 'id_vendor' : vendor};

        console.info(params);
        
          $.ajax({
          url: '<?= base_url("inscribir") ?>',
          type: 'POST',
          data: params,
          success: function (data) {
            info = JSON.parse(data);         
              swal({   
                  title: info.message,     
                  showConfirmButton: true 
                });
            }
          });
      }




      function delete_interest(id, btn)
      {
          $.ajax({
          url: '<?= base_url() ?>Intereses/delete/'+id,
          type: 'POST',
          data: id,
          success: function (data) {
            info = JSON.parse(data);         
              swal({   
                  title: info.message,     
                  showConfirmButton: true 
                });

              $(btn).closest("tr").remove();
            }
          });
      }
   
</script>

                <?php if (!empty($this->session->flashdata('msg'))): ?>
                    <script>
                        $(document).ready(function() {
                            swal({
                                title: "<?= $this->session->flashdata('msg'); ?>",
                                type: 'success'
                            });
                        });
                    </script>
                <?php endif; ?>