  <!-- jQuery -->
    <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>



            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Ayuda Multimedia</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Panel de Ayudas</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Subir Audios de Ayuda Multimedia</h3>
                            <p class="text-muted m-b-30"> Puedes subir acá multiples archivos de ayuda en formato de audio y video, solo tienes que cargarlos desde tu equipo y automaticamente los archivos se registrarán en sistema</p>
                            
                            <input type="text" name="title" id="title" class="form-control form-group" placeholder="Titulo del Archivo">

                            <form action="<?= base_url("Ayuda/saveMedia") ?>" class="dropzone" id="my-awesome-dropzone">
                            </form>
                           
                        </div>
                    </div>
                </div>
            </div>
                
   <script>
        $(document).ready(function(){
            var title = '';
             $('#my-awesome-dropzone').css({
                  display: 'none'});

            $('input#title').keyup(function(ev)
              {
                title = $('input#title').val();
                
                if(title.length > 0){
                  $('#my-awesome-dropzone').css({
                    display: 'block',
                  });  
                }else{
                   $('#my-awesome-dropzone').css({
                  display: 'none'});
                }
                
              });

            Dropzone.options.myAwesomeDropzone = {
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 10, // MB
              acceptedFiles: "audio/* , video/*",
              dictDefaultMessage: 'Click aquí para cargar tus archivos',
              uploadMultiple: false, //QUIAR MULTIPLE
              maxFiles: 1,

              init: function() {
                /*this.on("maxfilesexceeded", function(file){
                  swal({
                      title: 'Solo puedes cargar un archivo',
                      button: "ok",
                    });
                  this.removeFile(file);
                });*/
                this.on("sending", function(file, xhr, formData) {
                  formData.append("title", title);
                });
                this.on("success", function(file, response){
                    info = $.parseJSON(response);
                    swal({
                      title: info.message,
                      button: "ok",
                    });
                    $('button.confirm').click( function (ev)
                    {
                      window.location.href = "<?= base_url("helpSystem")?>";
                    });
                });
              }
            }
              
        });

        /*success: function(file, response){
                info = $.parseJSON(response);
                  swal({
                      title: info.message,
                      button: "ok",
                    });
              }
            }*/
       
    </script>