	<!-- animation CSS -->

	<link href="<?= base_url();?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">

    <link href="<?= base_url();?>css/animate.css" rel="stylesheet">


    <link href="<?= base_url();?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />


        <div class="panel panel-default col-lg-12">
            <div class="panel-body">

            	

            	<!--<form id="uploadAudio" enctype="multipart/form-data" class="dropzone">
            		<div class="form-group">
            			<label for="">Titulo de Audio</label>
            			<input type="text" class="form-control" required name="title_audio" id="titulo_audio">
            	</div>

	                    <input type="file" required name="file_audio" id="file_audio" />
	                
	            </form>-->

	            <div class="white-box">
                            <h3 class="box-title m-b-0">Upload Music </h3>
                            <p class="text-muted m-b-30"> For multiple audio/video files</p>
                            <form action="#" class="dropzone" enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file" />
                                </div>
                            </form>
                        </div>
			</div>
        </div>
			
		<script src="<?= base_url();?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<!-- Dropzone Plugin JavaScript -->
    <script src="<?= base_url();?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>

    
		 

				<script>

					$('form#uploadAudio').on('submit', function(ev){
						
						ev.preventDefault();
						ev.stopPropagation();

						$.ajax({
							method: "post",
							url: '<?= base_url("Ayuda/saveAudio")?>',
							data: new FormData(this),
							processData : false,
							contentType : false,
							type: 'json',
							beforeSend: function(){
			                    $('.loader').css("visibility", "visible");
			                },
						});

						.done(function(data, textStatus){
							
							info = $.parseJSON(data);
							console.info(info);

							$('.loader').css("visibility", "hidden");

		                    swal({
								  title: "Mensaje no enviado!",
								  text: "Por favor, verifica los campos e intenta nuevamente!",
								  icon: "warning",
								  button: "ok",
								});
						});

			   		});



					function saveAudio() 
				    {
				        var params = {'title_audio' : $("#titulo_audio").val() , 'file_audio' :  'files/ayuda/audios/'+document.getElementById('file_audio').files[0].name}
				        $.ajax({
				          	url: '<?= base_url("saveAudio") ?>',
				          	type: 'POST',
				          	data: params,
				          	success: function () {
				                alert("Audio Guardado");
			            	}
				        });
				        	
				    }

				    function saveFileAudio(ruta) 
				    {
				        var archivos = document.getElementById('file_audio');
				        
				        var archivo = archivos.files;
				        var arch = new FormData();
				        
				        if (archivo.length > 0)
				        {
				            for (var i = 0; i < archivo.length; i++) 
				            {
				                arch.append('archivo'+i,archivo[i]);
				            }

				            $.ajax({
				                url: ruta,
				                type:'POST',
				                contentType:false,
				                data:arch,
				                processData:false,
				                cache:false
				            });
				        }
				    }
				</script>