    <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="<?= base_url() ?>plugins/bower_components/dropify/dist/js/dropify.min.js"></script>

    <!--<script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-beta.1/classic/ckeditor.js"></script>-->

            <div class="container-fluid">
                 <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Sistema de Correos</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - PS</a></li>
                            <li>Sistema de Correos</li>
                            <li class="active">Redactar</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                   <a href="<?= base_url('composeMail') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Redactar</a>
                                    
                                    <br>   
                                       <?php if($type_user == 3) :?>
                                            <a href="<?= base_url('mailer') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Entrada </a>
                                        <?php endif; ?>
                                   
                                        <?php if($type_user == 2) :?>
                                            <a href="<?= base_url('outbox') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Enviados </a>

                                            <br>
                                            
                                            <a href="<?= base_url('composeNotification') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Componer Notificación </a>
                                        <?php endif; ?>
                                    
                                </div>
                                
                                    <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                                        <h3 class="box-title">Nuevo Mensaje</h3>
                                        
                                        <form id="mailer">
                                                <div class="container">
                                                    <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <select name="activity_id" class="form-control" required class="select" id="activity">
                                                        
                                                                <option value="">-- Seleccione Actividad --</option>

                                                                <?php foreach ($actividades as $act): ?>

                                                                    <option value="<?= $act->id?>">
                                                                        <?= $act->name_activity?>
                                                                    </option>

                                                                <?php endforeach ?>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <input class="form-control" placeholder="Asunto:" name="subject">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <select name="vendors[]" class="select form-control" required multiple="multiple" id="vendors">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                                <textarea class="textarea_editor form-control"  rows="15" name="message"></textarea>
                                                                <!--<textarea name="message" id="editor"></textarea>
                                                                <script>
                                                                    ClassicEditor
                                                                        .create( document.querySelector( '#editor' ) )
                                                                        .then( editor => {
                                                                            console.log( editor );
                                                                        } )
                                                                        .catch( error => {
                                                                            console.error( error );
                                                                        } );
                                                                        
                                                                </script>-->
                                                            
                                                    

                                                <!--<div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                       <div class="white-box">
                                                            <label for="input-file">Adjuntar archivo</label>
                                                            <input type="file" id="input-file" class="dropify" name="file-ad"/>
                                                        </div>
                                                    </div>
                                                </div>-->

                                                <hr>
                                            
                                                <button type="submit" class="btn btn-success"><i class="fa fa-envelope-o"></i> Enviar</button>
                                            
                                                </div>
                                                
                                            
                                            </form>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
               
              
            </div>

    <script>
        $(document).ready(function() {

            $('.textarea_editor').wysihtml5({
                "html": true
            });

            $('.dropify').dropify();
            
            $('select#activity').on('change', function(event) {
                event.preventDefault();
                event.stopPropagation();

                var activity_id = $(this).val();           

         
            $.ajax({
                method: "POST",
                url: "<?= base_url();?>Mailer/loadVendors/"+activity_id,
                data: activity_id,
                dataType: 'json'
            })

             .done((data, textStatus, jqXHR) => {

                $('select#vendors').empty();
                $('select#vendors').append('<option value=""> -- Seleccione -- </option>');
                $.each(data, function(key, value){
                    $('select#vendors').append('<option value="'+ value.id +'" selected>' + value.email + '</option>');
                });
            });
    
                $('select#vendors').empty();
            });

            ////// SaveMailer //////
            ///
            /// $( '#formId' )
            
            $('form#mailer').on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();

                $.ajax({
                    method: "POST",
                    url: "<?= base_url()?>Mailer/create",
                    data: new FormData(this),
                    processData : false,
                    contentType : false,
                    type: 'json',
                    beforeSend: function(){
                               swal({
                                    title: 'Enviando notificación al Responsable',
                                    buttons: false,
                                    showConfirmButton: false,
                                    text: ''
                                });
                            }
                })


                .done(function (data){
                    info = $.parseJSON(data);
                    swal({
                        title: info.message,
                        icon: "success",
                        button: "ok",
                    });

                    if (info.status == 'success') {
                        window.location.reload();
                    }

                });

            });
            
        
        });

    </script> 