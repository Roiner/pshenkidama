


            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Sistema de Correos</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - PS</a></li>
                            <li>Sistema de Correos</li>
                            <li class="active">Mensajes Enviados</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                    <div> 
                                        <a href="<?= base_url('composeMail') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Redactar </a>

                                        <br>
                                        
                                        <a href="<?= base_url('composeNotification') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Componer Notificación </a>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                                    <div class="inbox-center">
                                        <div class="table-responsive">
                                            <table id="myTable" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                       <th>Actividad</th> 
                                                       <th>Asunto</th> 
                                                       <th>Mensaje</th>
                                                       <th>Fecha</th> 
                                                       <!--<th>Acciones</th>-->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($mailers as $key => $mail):?>

                                                        <tr>
                                                            <td><?= $mail->name_activity?></td>
                                                            <td><?= $mail->subject?> </td>
                                                            <td><?= $mail->message?></td>
                                                            <td>
                                                                <?php 
                                                                    $date = date_create($mail->date);
                                                                    $fecha = date_format($date, 'd-m-Y H:i:s');
                                                                    echo $fecha;
                                                                ?>
                                                            </td>
                                                            <!--<td>
                                                                <a href="#" onclick="showDetails(<?= $mail->id ?>);" title="Ver" style="margin-right: 10px;">
                                                                    <i class="fa fa-search" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                                </a> 
                                                
                                                                <a href="#" title="Eliminar" style="margin-right: 10px;" onclick="deleteMail(<?= $mail->id ?>);">
                                                                    <i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                                </a>
                                                            </td>-->
                                                        </tr>

                                                    <?php endforeach?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
               
            </div>


<script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

<script>
    
    $(document).ready(function() {
        $('#myTable').DataTable();
    });


    function showDetails (mail_id)
    {
        alert(mail_id);
    }

    function deleteMail (mail_id)
    {
        alert(mail_id);
    }
</script>