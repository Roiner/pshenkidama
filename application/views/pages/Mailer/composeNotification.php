
    <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>


        <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                        <h4 class="page-title">Componer Notificación</h4>
                    </div>
                    <div class="col-lg-6 col-sm-8 col-md-6 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - PS</a></li>
                            <li>Componer Notificación</li>
                            <li class="active">Redactar</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                  
                                    <a href="<?= base_url('composeMail') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Redactar  </a>

                                    <br>
                                    <a href="<?= base_url('outbox') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Enviados </a>
                                </div>
                                
                                    <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                                        <h3 class="box-title">Componer Notificación</h3>
                                        
                                        <form id="notify" method="post">
                                            <div class="container">

                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            
                                                             <select name="option" class="form-control" id="option">
                                                                <option value="">-- Seleccione --</option>
                                                                <option value="afiliados"> Afiliados </option>
                                                                <option value="vendedores"> Responsables</option>
                                                                <option value="todos"> Todos </option>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <a href="#" type="submit" id="button" class="btn btn-primary">Nuevo destinatario</a>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            
                                                            <select name="destinatarios[]" class="form-control destinatarios" multiple="multiple">

                                                                <?php foreach ($todos as $key => $todo): ?>
                                                                    <option value="<?= $todo['email']?>">
                                                                        < <?= strtoupper($todo['name']).'. '.$todo['email'].'. '.$todo['type']?> > ,
                                                                    </option>

                                                                <?php endforeach ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" name="subject" class="form-control" placeholder="Asunto">
                                                        </div>
                                                    </div>
                                                </div>

                                                <textarea class="textarea_editor form-control"  rows="15" name="message"></textarea>
                                                                

                                                <hr>

                                                <p class="texto"></p>
                                            
                                                <button type="submit" class="btn btn-success"><i class="fa fa-envelope-o"></i> Enviar</button>
                                            
                                                </div>
                                                
                                            
                                            </form>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>

    <script>
        $(document).ready(function() {

            var option = '';
            
            $('.destinatarios').select2({
                allowClear: true,
                closeOnSelect: false,
                placeholder: '-- Selecciona Destinatarios --',

            });

            $('.textarea_editor').wysihtml5({
                "html": true
            });

            $('select#option').change(function(event) {
                var option = ($('select#option').val());  

                $.ajax({
                  method: "POST",
                  url: "<?= base_url()?>loadUsersMail/"+option,
                  data: option,
                  dataType: 'json'

                })
                
                .done((data, textStatus, jqXHR) => {
                    console.info(data);
                    $('select.destinatarios').empty();
                    $('select.destinatarios').append('<option value=""> -- Seleccione -- </option>');
                    $.each(data, function(key, value){
                        $('select.destinatarios').append('<option value="'+ value.email +'" selected>' + value.name + '. ' + value.email + '. ' + value.type +'</option>');
                    });
                });
        
                    $('select.destinatarios').empty();
            });

            $('#button').click(function(){
                var value = prompt("Por favor ingresa la dirección de correo electrónico");

                    $(".destinatarios")
                        .append($("<option selected></option>")
                        .attr("value", value )
                        .text(value ));
            });
                    
            $('form#notify').on('submit', function(ev){
            
                ev.preventDefault();
                ev.stopPropagation();
                
               $.ajax({
                    method: "POST",
                    url: "<?= base_url("sendNotification")?>",
                    data: new FormData(this),
                    processData : false,
                    contentType : false,
                    type: 'json',
                    beforeSend: function(){
                        swal({
                                title: 'Enviando notificaciones, por favor espere ...',
                                buttons: false,
                                showConfirmButton: false,
                                closeOnClickOutside: false,
                                closeOnEsc: false,
                            });
                    }
                })
                
                .done(function(data) {
                    info = $.parseJSON(data);
                    swal({
                        title: info.message,
                        icon: "success",
                        button: "ok",
                    });
                    $('form#notify')[0].reset();
                    $('.destinatarios').val(null).trigger('change');
                });
            });
    });

    </script>