    <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
    
        <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                        <h4 class="page-title">Componer Mail de Invitación</h4>
                    </div>
                    <div class="col-lg-6 col-sm-8 col-md-6 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - PS</a></li>
                            <li>Componer mail de Invitación</li>
                            <li class="active">Redactar</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 inbox-panel">
                                  
                                    <a href="<?= base_url('mailer') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Entrada </a>
                                    <br>
                                    <a href="<?= base_url('createMail') ?>" class="list-group-item active" style="color: white; font-weight: bold; border: 0">Componer Invitación </a>
                                    
                                </div>
                                
                                    <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                                        <h3 class="box-title">Nuevo Mensaje</h3>
                                        
                                        <form id="mailer" method="post">
                                                <div class="container">

                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="form-group">
                                                                <select name="composition_type" id="composition_type" required class="form-control">
                                                                    <option value="">-- Seleccione Tipo de Composición --</option>
                                                                    <option value="maqueta">Maqueta</option>
                                                                    <option value="libre">Composición Libre</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="actividad">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="form-group">
                                                                <select name="activity_id" class="form-control" class="select" id="activity">
                                                            
                                                                    <option value="">-- Actividad --</option>

                                                                    <?php foreach ($actividades as $act): ?>

                                                                       <option value="<?= $act->id_activity?>">
                                                                            <?= $act->name_activity; ?>
                                                                        </option>

                                                                    <?php endforeach ?>
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            
                                                            <input type="checkbox" id="checkbox"> Todos
                                                            
                                                            <select name="afiliados[]" class="form-control" required multiple="multiple" id="afiliados">

                                                                <?php foreach ($afiliados as $af): ?>

                                                                   <option value="<?= $af->email_affiliate;?>"
                                                                    class="selected">
                                                                        <?= $af->names_affiliate.' '.$af->last_names_affiliate. ' < '.$af->email_affiliate.' > '; ?>
                                                                    </option>

                                                                <?php endforeach ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                                <textarea class="textarea_editor form-control"  rows="15" name="message"></textarea>
                                                                

                                                <hr>

                                                <p class="texto"></p>
                                            
                                                <button type="submit" class="btn btn-success"><i class="fa fa-envelope-o"></i> Enviar</button>
                                            
                                                </div>
                                                
                                            
                                            </form>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
               
              
            </div>
        </div>

    <script>
        $(document).ready(function() {
            var actividades = <?php echo json_encode($actividades);?>
            //$('div#actividad').hide();
            

            $('#afiliados').select2({
                allowClear: true,
                closeOnSelect: false,
                placeholder: '-- Afiliados --',

            });



            $('.textarea_editor').wysihtml5({
                "html": true
            });

            $("#checkbox").click(function(){
                if($("#checkbox").is(':checked') ){
                    $("#afiliados > option").prop("selected","selected");
                    $("#afiliados").trigger("change");
                }else{
                    $("#afiliados > option").removeAttr("selected");
                     $("#afiliados").trigger("change");
                 }
            });

        
            $('select#composition_type').on('change', function(event) {
                //var activity_id = $('select#activity').val();
                
                if ($(this).val() == 'maqueta') {
                    $('div#actividad').show();
                    $("select#activity option").removeAttr("selected");
                }else{
                   $('div#actividad').hide();
                   $('.textarea_editor').data("wysihtml5").editor.setValue('');
                }
                /*if ($(this).val() == 'maqueta') {
                    for (var i = 0; i < actividades.length; i++) {
                        if(actividades[i]['id_activity'] == activity_id)
                        var activity_message = (actividades[i]['mail_activity']);
                    }
                    $('.textarea_editor').data("wysihtml5").editor.setValue(activity_message);
                }else{
                    $('.textarea_editor').data("wysihtml5").editor.setValue('');
                }*/
            });

            $('select#activity').on('change', function (event){
                var activity_id = $('select#activity').val();
                //console.info($('select#composition_type').val());
               if ($('select#composition_type').val() == 'maqueta') {
                    for (var i = 0; i < actividades.length; i++) {
                        if(actividades[i]['id_activity'] == activity_id)
                        var activity_message = (actividades[i]['mail_activity']);
                    }
                    $('.textarea_editor').data("wysihtml5").editor.setValue(activity_message);
                }else{
                    $('.textarea_editor').data("wysihtml5").editor.setValue('');
                }
            });



            $('form#mailer').on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();

                $.ajax({
                    method: "POST",
                    url: "<?= base_url("saveMail")?>",
                    data: new FormData(this),
                    processData : false,
                    contentType : false,
                    type: 'json',
                    beforeSend: function(){
                        swal({
                                title: 'Enviando mensaje, por favor espere ...',
                                buttons: false,
                                showConfirmButton: false,
                                closeOnClickOutside: false,
                                closeOnEsc: false,
                            });
                        }
                })
                .done(function (data){
                    info = $.parseJSON(data);
                    swal({
                        title: info.message,
                        icon: "success",
                        button: "ok",
                    });
                    $('form#mailer')[0].reset();
                    $('#afiliados').val(null).trigger('change');
                });

            });

         
           /* $.ajax({
                method: "POST",
                url: "<?= base_url();?>Mailer/loadVendors/"+activity_id,
                data: activity_id,
                dataType: 'json'
            })

             .done((data, textStatus, jqXHR) => {

                $('select#vendors').empty();
                $('select#vendors').append('<option value=""> -- Seleccione -- </option>');
                $.each(data, function(key, value){
                    $('select#vendors').append('<option value="'+ value.id +'" selected>' + value.email + '</option>');
                });
            });
    
                $('select#vendors').empty();
            });

            ////// SaveMailer //////
            ///
            /// $( '#formId' )
            
            $('form#mailer').on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();

                $.ajax({
                    method: "POST",
                    url: "<?= base_url()?>Mailer/create",
                    data: new FormData(this),
                    processData : false,
                    contentType : false,
                    type: 'json',
                    beforeSend: function(){
                               swal({
                                    title: 'Enviando notificación al Vendedor',
                                    buttons: false,
                                    showConfirmButton: false,
                                    text: ''
                                });
                            }
                })


                .done(function (data){
                    info = $.parseJSON(data);
                    swal({
                        title: info.message,
                        icon: "success",
                        button: "ok",
                    });

                    if (info.status == 'success') {
                        window.location.reload();
                    }

                });

            });*/
            
        
        });

    </script>