
<?php foreach ($vendedor as $vd): ?>


                        <h4 class="modal-title">Responsable: <?= $vd->name_vendor.'. ' . count($asignados) ?> Afiliados</h4>
                    </div>
                    <div class="modal-body">
                    	<div class="col-md-12 col-xs-12">
	                        <div class="white-box">
	                            <ul class="nav customtab nav-tabs" role="tablist">
	                                <li role="presentation" class="nav-item"><a href="#home" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Estadísticas</span></a></li>
	                                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Perfil</span></a></li>
	                                <li role="presentation" class="nav-item"><a href="#asignados" class="nav-link" aria-controls="asignados" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-th-list"></i></span> <span class="hidden-xs">Asignados</span></a></li>
	                            </ul>
	                            <div class="tab-content">
	                                <div class="tab-pane active" id="home">

					                        <div class="row row-in">
				                                <div class="col-lg-3 col-sm-6 row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
				                                            <h5  style="color:<?= $status[5]->color_status ?>; font-weight: bold">Contactado</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[5]->color_status ?>"><?= $totales['C'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= ($totales['C']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['C']*100)/$totales['TOT'] ?>%; background-color: <?= $status[5]->color_status ?> !important"> <span class="sr-only">40% Complete (success)</span></div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-lg-3 col-sm-6 row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
				                                            <h5 style="color:<?= $status[3]->color_status ?>; font-weight: bold">Posible Voto</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[3]->color_status ?>"><?= $totales['PV'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['PV']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['PV']*100)/$totales['TOT'] ?>%; background-color: <?= $status[3]->color_status ?> !important"> <span class="sr-only">40% Complete (success)</span></div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
				                                            <h5 style="color:<?= $status[6]->color_status ?>; font-weight: bold">Voto Confirmado</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[6]->color_status ?>"><?= $totales['VC'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= ($totales['VC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['VC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[6]->color_status ?> !important">  </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-lg-3 col-sm-6 row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
				                                            <h5 style="color:<?= $status[4]->color_status ?>; font-weight: bold">Posible Candidato</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[4]->color_status ?>"><?= $totales['PC'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= ($totales['PC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['PC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[4]->color_status ?> !important">  </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
					                        </div>

					                        <div class="row row-in">
				                                <div class="col-lg-3 col-sm-6  row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
				                                            <h5 style="color:<?= $status[7]->color_status ?>; font-weight: bold">Candidato Confirmado</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[7]->color_status ?>"><?= $totales['CC'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['CC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['CC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[7]->color_status ?> !important">  </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-lg-3 col-sm-6 row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
				                                            <h5 style="color:<?= $status[8]->color_status ?>; font-weight: bold">Voto Contrario</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[8]->color_status ?>"><?= $totales['VCO'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= ($totales['VCO']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['VCO']*100)/$totales['TOT'] ?>%; background-color: <?= $status[8]->color_status ?>  !important" >  </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-lg-3 col-sm-6 row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
				                                            <h5 style="color:<?= $status[9]->color_status ?>; font-weight: bold">Compañero</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[9]->color_status ?>"><?= $totales['CO'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['CO']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['CO']*100)/$totales['TOT'] ?>%; background-color: <?= $status[9]->color_status ?> !important">  </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>

				                                <div class="col-lg-3 col-sm-6 row-in-br">
				                                    <div class="col-in row">
				                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
				                                            <h5 style="color:<?= $status[10]->color_status ?>; font-weight: bold">No Contactado</h5> </div>
				                                        <div class="col-md-6 col-sm-6 col-xs-6">
				                                            <h4 class="counter text-right m-t-15" style="color:<?= $status[10]->color_status ?>"><?= $totales['NC'] ?></h4> </div>
				                                        <div class="col-md-12 col-sm-12 col-xs-12">
				                                            <div class="progress">
				                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?= ($totales['NC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['NC']*100)/$totales['TOT'] ?>%; background-color: <?= $status[10]->color_status?> !important">  </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
					                        </div>
					                </div>
					                
			                              

	                                <div class="tab-pane" id="profile">
	                                    <h4 class="font-bold m-t-30">Modificar Datos</h4>
	                                    <hr>
	                                    <form action="#" onsubmit="return false;" id="profile-vendor">
	                                    	<div class="row">
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Nombre</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->name_vendor ?>" name="name_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">DNI</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->dni_vendor ?>" name="dni_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Teléfono</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->phone_vendor ?>" name="phone_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Dirección</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->direction_vendor ?>" name="direction_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Correo</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->email_vendor ?>" name="email_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Sexo</label>
		                                    			<select name="sex_vendor" id="sex_vendor" class="form-control">
		                                    				<option value="">Sexo</option>
		                                    				<option value="Masculino" <?php if ($vd->sex_vendor == "Masculino"): ?>selected<?php endif ?>>Masculino</option>
		                                    				<option value="Femenino" <?php if ($vd->sex_vendor == "Femenino"): ?>selected<?php endif ?>>Femenino</option>
		                                    			</select>
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Edad</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->age_vendor ?>" name="age_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for=""></label>
		                                    			<a href="#" class="btn btn-success" style="margin-top: 26px;" onclick="saveProfileVendor(<?= $vd->id_vendor ?>)">Actualizar Datos</a>
		                                    		</div>
		                                    	</div>
	                                    	</div>
	                                    </form>

	                                    <div class="row">
	                                    	<a href="<?= base_url();?>Responsables/habilitarAcceso/<?= $vd->id_vendor ?>" class="btn btn-danger" style="margin-top: 26px"> Confirmar Acceso </a>
	                                    </div>
	                                </div>
	                                
	                                <div class="tab-pane" id="asignados">
                                    	<div class="table-responsive" id="TableAff">
			                                <table id="asTable" class="table table-striped">
			                                    <thead>
			                                        <tr>
			                                        	<th>Apellido </th>
			                                            <th>Nombre </th>
			                                            <th>DNI</th>
			                                            <th>Comuna</th>
			                                            <th>Acciones</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <?php foreach ($asignados as $key): ?>
			                                        <tr>
			                                        	<td> <?= $key->last_names_affiliate ?>  </td>
			                                            <td> <?= $key->names_affiliate ?> </td>
			                                            <td><?= $key->dni_affiliate ?></td>
			                                            <td>Comuna <?= $key->commune_affiliate ?></td>
			                                            <td>
			                                            	<a href="#" title="Quitar" style="margin-right: 10px;" onclick="removeAffiliate(<?= $key->id_affiliate ?>,<?= $vd->id_vendor ?>)">
			                                            		<i class="fa fa-times" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
			                                            	</a>
		                                            	</td>
			                                        </tr>   
			                                        <?php endforeach ?>
			                                    </tbody>
			                                </table>
			                            </div>

                            			<div class="col-md-12">
                            				<a href="#" class="btn btn-success" onclick="asignaAfiliados(<?= $vd->id_vendor ?>);">Nuevos Afiliados</a>
                            			</div>

                            			<div id="contentAsignaAfiliados" style="margin-top: 80px;">
                            				
                            			</div>
	                                </div>
	                                
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar Ventana</button>
                    </div>

<?php endforeach ?>

<script>
	
	$(document).ready(function() {
        $('#asTable').DataTable({
        	"lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
        });
	});

	function asignaAfiliados(id_vendor) 
	{
		cargar($("#contentAsignaAfiliados"),'<?= base_url("asignaAfiliados") ?>/'+id_vendor);	
	}

	function saveProfileVendor(id_vendor) 
	{
        $.ajax({
            url: '<?= base_url("updateProfileVendor") ?>/'+id_vendor,
            type: 'POST',
            data: $("#profile-vendor").serialize(),
            success: function () {
                swal({   
                    title: "Usuario Habilitado",
                    timer: 2000,
                    showConfirmButton: true 
                });
                window.location.reload();
            }  
        });   
	}

	function removeAffiliate(afiliado,responsable) 
	{
		var params = {'vendor_id' : responsable, 'affiliate_id' : afiliado};
		$.ajax({
            url: '<?= base_url("removeAffiliate") ?>',
            type: 'POST',
            data: params,
            success: function () {
                swal({   
                    title: "Afiliado Removido",
                    timer: 2000,
                    showConfirmButton: true 
                });
                cargar('#TableAff','<?= base_url('tAfiliados') ?>/'+responsable);
            }  
        });	
	}

</script>