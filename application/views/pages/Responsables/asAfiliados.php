


				<div class="table-responsive">
                    <table id="asignaTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>DNI</th>
                                <th>Comuna</th>
                                <th>Asignar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($afiliados as $key): ?>
                            <tr id="<?= $key->id_affiliate ?>">
                                <td><?= $key->last_names_affiliate ?></td>
                                <td><?= $key->names_affiliate ?> </td>
                                <td><?= $key->dni_affiliate ?></td>
                                <td>Comuna <?= $key->commune_affiliate ?></td>
                                <td>
                                    <a href="#" title="Asignar" style="margin-right: 10px;" onclick="asignaAfiliado(<?= $key->id_affiliate ?>,<?= $vendedor ?>);">
                                        <i class="fa fa-plus" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                    </a>
                                </td>
                            </tr>   
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <button class="btn btn-success" type="submit" id="btn-enviar">Asignar Seleccionados</button>
                </div>

                <script>
                        
                        var afiliados = [];

                		$(document).ready(function() {
							$('#asignaTable').DataTable({
								"lengthMenu": [[30,60,90,120,150, -1], [30,60,90,120,150, "Todos"]]
							});
						});


                        function asignaAfiliado(afiliado,responsable) 
                        {
                            var params = {'vendor_id' : responsable, 'affiliate_id' : afiliado};

                            afiliados.push(params);

                            
                            $('tr#'+afiliado).addClass('table-success');

                           
                            /*$.ajax({
                                url: '<?= base_url("asignaAffiliate") ?>',
                                type: 'POST',
                                data: params,
                                success: function () {
                                    swal({   
                                        title: "Afiliado Asignado",
                                        timer: 2000,
                                        showConfirmButton: true 
                                    });
                                    cargar('#TableAff','<?= base_url('tAfiliados') ?>/'+responsable);
                                    cargar($("#contentAsignaAfiliados"),'<?= base_url("asignaAfiliados") ?>/'+responsable);
                                }  
                            }); */

                            }

                            $('#btn-enviar').click(function(ev){
                                var newAfiliados = afiliados;
                                
                               $.ajax({
                                  method: "POST",
                                  url: "<?= base_url("asignaAffiliate") ?>",
                                  dataType: "JSON",
                                  data: {newAfiliados},
                                  cache: false
                                  
                                })
                                  .done(function( data ) {
                                    
                                    swal({   
                                        title: "Afiliados Agregados",
                                        showConfirmButton: true 
                                    });
                                    $('button.confirm').click(function(ev){
                                        var afiliados = [];
                                        $('tr').removeClass('table-success');
                                    });
                                  });
                                });


                           
                </script>