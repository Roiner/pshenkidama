        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Responsables <?= $type_user?> </h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li><a href="#">Responsables</a></li>
                            <li class="active">Lista de Responsables</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Listado de Responsables</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Email</th>
                                            <th>Edad</th>
                                            <th>DNI</th>
                                            <th>Status</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($vendedores as $key): ?>
                                        <tr>
                                            <td><?= $key->name_vendor ?></td>
                                            <td><?= $key->email_vendor ?></td>
                                            <td>Edad <?= $key->age_vendor ?></td>
                                            <td><?= $key->dni_vendor ?></td>
                                            <td><span class="label label-table" style="background-color: <?= $key->color_status ?>"><?= $key->name_status ?></span></td>
                                            <td>
                                                <a href="#respModal" data-toggle="modal" data-target="#respModal" onclick="cargar('#viewResp','<?= base_url('viewResponsable/'.$key->id_vendor) ?>');" style="margin-right: 10px;">
                                                    <i class="fa fa-search" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                </a>
                                                <a href="#" style="margin-right: 10px;" onclick="disableResponsable(<?= $key->id_vendor ?>);">
                                                    <i class="fa fa-times" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                                </a>
                                            </td>
                                        </tr>   
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                


                <div class="modal fade" id="respModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar Ventana">×</button>

                                <div id="viewResp"></div>
                        </div>
                        <!-- /.modal-content -->
                    </div>  
                </div>


<script>
    
    function disableResponsable(responsable) 
    {
        params = {'id_vendor' : responsable};
        $.ajax({
            url: '<?= base_url("disabledResponsable") ?>',
            type: 'POST',
            data: params,
            success: function () {
                swal({   
                    title: "Responsable Eliminado",     
                    showConfirmButton: true,
                    timer: 2000
                });
                window.location.reload();
            }  
        });    
    }
</script>