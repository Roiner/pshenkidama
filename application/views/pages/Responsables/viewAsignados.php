





											<table id="asTable" class="table table-striped">
			                                    <thead>
			                                        <tr>
			                                            <th>Apellido</th>
			                                            <th>Nombre</th>
			                                            <th>DNI</th>
			                                            <th>Comuna</th>
			                                            <th>Acciones</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <?php foreach ($asignados as $key): ?>
			                                        <tr>
			                                        	<td><?= $key->last_names_affiliate ?></td>
			                                            <td><?= $key->names_affiliate ?> </td>
			                                            <td><?= $key->dni_affiliate ?></td>
			                                            <td>Comuna <?= $key->commune_affiliate ?></td>
			                                            <td>
			                                            	<a href="#" title="Quitar" style="margin-right: 10px;" onclick="removeAffiliate(<?= $key->id_affiliate ?>,<?= $key->vendor_id ?>)">
			                                            		<i class="fa fa-times" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
			                                            	</a>
		                                            	</td>
			                                        </tr>   
			                                        <?php endforeach ?>
			                                    </tbody>
			                                </table>




            <script>
        		$(document).ready(function() {
			        $('#asTable').DataTable({
			        	"lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
			        });
				});
            </script>