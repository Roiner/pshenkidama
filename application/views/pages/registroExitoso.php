<!DOCTYPE html>  
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">    
    <title>CRM Gestar - <?= $title_page ?></title>
     <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?= base_url() ?>css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>css/style_old.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url() ?>css/colors/purple.css" id="theme"  rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register" style="background:url(images/back.jpg) center center/cover no-repeat!important;">
  <div class="login-box">
    <div class="white-box">
      
        
        <div class="form-group">
          <div class="col-xs-12 text-center">
            <div class="user-thumb text-center"> 
              <h3>Felicidades</h3>
              <p><?= $mensaje ?></p>
            </div>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p><a href="<?= base_url() ?>" class="text-primary m-l-5"><b>Salir</b></a></p>
          </div>
        </div>
    
    </div>
  </div>
</section>
<!-- jQuery -->
  <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?= base_url() ?>js/tether.min.js"></script>
  <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
  <!-- Menu Plugin JavaScript -->
  <script src="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

  <!--slimscroll JavaScript -->
  <script src="<?= base_url() ?>js/jquery.slimscroll.js"></script>
  <!--Wave Effects -->
  <script src="<?= base_url() ?>js/waves.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?= base_url() ?>js/custom.min.js"></script>
  <!--Style Switcher -->
  <script src="<?= base_url() ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
  
</body>
</html>
