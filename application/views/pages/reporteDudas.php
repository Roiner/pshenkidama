
						<h4 class="modal-title">Reportar dudas al Administrador</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?= base_url();?>Nigglings/create">
                            <div class="form-group">
                                <label for="niggling_subject" class="control-label">Asunto:</label>
                                <input type="text" class="form-control" id="niggling_subject">
                            </div>
                            <div class="form-group">
                                <label for="niggling_message" class="control-label">Mensaje:</label>
                                <textarea class="form-control" id="niggling_message"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light">Enviar Mensaje</button>
                    </div>