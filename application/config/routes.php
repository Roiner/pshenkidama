<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'confirma_session';
$route['404_override'] = 'Principal/error404';
$route['translate_uri_dashes'] = FALSE;

$route['reporteDudas'] = 'Principal/reportarDudas';
$route['facebook'] = 'Login/facebook';
$route['login/facebook'] = 'login/inFacebook';

//Registro Especial
$route['registroEspecial'] = "registro/especial";
$route['validaCredencial'] = "registro/validaCheck";
$route['completeRegistroEspecial'] = "registro/completeRegistroEspecial";
$route['registroMaster'] = "registro/masterReg";

//Inicio de Sesion
$route['login'] = "Login";
$route['principal'] = "Principal";
$route['administrador'] = "Administrador";
$route['vendedor'] = "Responsables";
$route['inSession'] = "Login/inSession";
$route['outSession'] = "Login/outSession";
$route['registroVendedor'] = "Registro/registroVendedor";

$route['newFile'] = 'Ayuda/newFile';

$route['probar'] = 'Afiliado/probar';
$route['probarCalendario'] = 'Actividades/probarCalendario';

$route['findVendor/(.+)'] = 'Login/findVendor/$1';

//Afiliado
$route['newAfiliado'] = "Afiliado";
$route['readAfiliado'] = "Afiliado/newAfiliado";
$route['confirmaSaveAfiliado'] = "Afiliado/saveAfiliado";
$route['viewAfiliado/(.+)'] = "Afiliado/verAfiliado/$1";
$route['saveGeneralData/(.+)'] = "Afiliado/updateData/$1";
$route['saveContactData/(.+)'] = "Afiliado/updateData/$1";
$route['saveCategorizationData/(.+)'] = "Afiliado/updateCatData/$1";
$route['asignaResponsable/(.+)'] = "Afiliado/asignaResponsable/$1";
$route['asignaResponsable2/(.+)'] = "Afiliado/asignaResponsable2/$1";
$route['saveAsignaResponsable2'] = "Afiliado/saveAsignaResponsable2";
$route['addAssociate'] = "Afiliado/addAfiliado";
$route['getAffiliates/(.+)'] = "Afiliado/getAffiliates/$1";
$route['getAssociates/(.+)'] = "Afiliado/getAssociates/$1";
$route['deleteAssociates'] = "Afiliado/deleteAsociate";
$route['addAssociateInterest'] = "Afiliado/addAssociateInterest";



$route['removeResponsable'] = "Afiliado/removeResponsable";
$route['addResponsable'] = "Afiliado/addResponsable";
$route['disabledAffiliate'] = "Afiliado/disable";
$route['uploadExcel'] = "Afiliado/uploadExcel";
$route['misAfiliados/(.+)'] = "Responsables/misAfiliados/$1";
$route['misAfiliados'] = "Responsables/misAfiliados";

//Responsables
#$route['listResponsables'] = "Responsables";
$route['viewResponsable/(.+)'] = "Responsables/verResponsable/$1";
$route['asignaAfiliados/(.+)'] = "Responsables/asignaAfiliados/$1";
$route['getAfiliados']         = "Responsables/getAfiliadosss";

$route['disabledResponsable'] = "Responsables/disable";
$route['updateProfileVendor/(.+)'] = "Responsables/updateVendor/$1";
$route['removeAffiliate'] = "Responsables/remove";
$route['tAfiliados/(.+)'] = "Responsables/tAfiliados/$1";
$route['asignaAffiliate'] = "Responsables/asigna";
$route['enableVendor/(.+)'] = "Responsables/enableVendor/$1";
$route['disableVendor/(.+)'] = "Responsables/disableVendor/$1";



//Actividades
$route['newActividad'] = "Actividades/newActividad";
$route['saveActividad'] = "Actividades/saveActividad";
$route['allActivities'] = "Actividades/verActividades";
$route['actividadesIndex'] = "Actividades";
$route['calendarActivities'] = "Actividades/verCalendarioActividades";
$route['calendarActivitiesVendor'] = "Actividades/verCalendarioActividades2";
$route['activityVendor/(.+)'] = "Actividades/activityVendor/$1";
$route['addActivityVendor'] = "Actividades/addActivityVendor";
$route['showActivityVendor/(.+)'] = "Actividades/showActivityVendor/$1";
$route['quitActivityVendor'] = "Actividades/quitActivityVendor";
$route['email'] = "Actividades/email";
$route['actividadesAsignadas'] = "Actividades/actividadesAsignadas";
$route['getActivities'] = "Actividades/verActividadesCalendario";
$route['inscribir'] = "Actividades/inscribir";
$route['aprobeActivityVendor'] = "Actividades/aprobeActivityVendor";
$route['aprobarVendedores'] = "Actividades/aprobarVendedores";

$route['editActividad/(.+)'] = "Actividades/editActividad/$1";
$route['updateActividad'] = "Actividades/updateActividad";
$route['showActividad/(.+)'] = "Actividades/showActividad/$1";
$route['verActividad/(.+)'] = "Actividades/showActividad2/$1";


//Intereses
$route['newInteres'] = "Intereses/newInteres";
$route['saveInteres'] = "Intereses/saveInteres";
$route['allInterest'] = "Intereses/verIntereses";
$route['actividadesIndex'] = "Intereses";
$route['editInteres/(.+)'] = "Intereses/editInteres/$1";
$route['updateInteres'] = "Intereses/updateInteres";
$route['showInteres/(.+)'] = "Intereses/showInteres/$1";
$route['verInteres/(.+)'] = "Intereses/showInteres2/$1";
$route['listInterestChecked/(.+)'] = "Intereses/listInterestChecked/$1";

//Tablas
$route['administradores'] = "Tablas";
$route['vendedores'] = "Tablas/vendedores";
$route['newAdmin'] = "Tablas/newAdmin";
$route['viewAdmin/(.+)'] = "Tablas/viewAdmin/$1";
$route['saveAdmin'] = "Tablas/saveAdmin";
$route['updateAdmin/(.+)'] = "Tablas/updateAdmin/$1";
$route['disableAdmin/(.+)'] = "Tablas/disableAdmin/$1";
$route['enableAdmin/(.+)'] = "Tablas/enableAdmin/$1";

$route['newStatus'] = "Tablas/newStatus";
$route['saveStatus'] = "Tablas/saveStatus";

//Ayuda
$route['helpSystem'] = "Ayuda";
$route['audios'] = "Ayuda/viewAudio";
$route['sms'] = "Ayuda/viewSms";
$route['normas'] = "Ayuda/viewNormas";
$route['resp'] = "Ayuda/viewResp";
$route['faq'] = "Ayuda/viewFaq";
$route['videos'] = "Ayuda/viewVideo";
$route['saveAudio'] = "Ayuda/saveAudio";
$route['saveFileAudio'] = "Ayuda/saveFileAudio";
$route['saveFaq'] = "Ayuda/saveFaq";
$route['saveNorma'] = "Ayuda/saveNorma";
$route['savePosResp'] = "Ayuda/savePosResp";
$route['saveMensaje'] = "Ayuda/saveMensaje";

$route['saveMedia'] = "Ayuda/saveMedia";


//Mailer
$route['mailer'] = "Mailer";
$route['composeMail'] = "Mailer/compose";
$route['viewMail'] = "Mailer/view";
$route['mail/create'] = "Mailer/create";
$route['loadVendors/(.+)'] = "Mailer/loadVendors/$1";
$route['outbox'] = "Mailer/outbox";
$route['sendInvitation'] = "Mailer/sendInvitation";

$route['createMail'] = "Mailer/createMail"; 
$route['saveMail'] = "Mailer/saveMail";

$route['composeNotification'] = "Mailer/composeNotification";
$route['loadUsersMail/(.+)'] = 'Mailer/loadUsersMail/$1';
$route['sendNotification'] = "Mailer/sendNotification";


//General 
$route['viewResp'] = "General/viewResp";
$route['refuseResp'] = "General/refuseResp";
$route['acceptResp'] = "General/acceptResp";
$route['notificaciones'] = "General/notificaciones";

$route['savePass'] = "General/savePass";


/*Nen Routes*/

//Dudas
$route['registrar/duda'] = "Nigglings/create";
$route['dudas'] = "Nigglings/index";
$route['dudasVendedor'] = "Nigglings/dudasVendor";
$route['duda/(:num)'] = 'Niggligns/find/$1';
$route['duda/saveResponse'] = 'Nigglings/saveResponse';
$route['duda/delete/(:num)'] = 'Niggligns/delete/$1';

$route['afiliado/vendor/(:num)'] = 'Afiliado/vendors/$1';

$route['removeCache'] = 'Principal/removeCache';


////Chat
$route['chat'] = 'ChatController/index';
$route['send-message'] = 'ChatController/send_text_message';
$route['chat-attachment/upload'] = 'ChatController/send_text_message';
$route['get-chat-history-vendor'] = 'ChatController/get_chat_history_by_vendor';
$route['chat-clear'] = 'ChatController/chat_clear_client_cs';
