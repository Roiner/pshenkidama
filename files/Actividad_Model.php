<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividad_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getEvents()
	{
		$this->db->select('id as id, name_activity as title, date_activity as start, date_activity as end, hour_activity as inicio, description_activity as description, name_status, color_status as color');
		$this->db->from('activities');
		$this->db->join('status', 'status.id_status = activities.status_activity', 'inner');
		$this->db->order_by('start', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	function listActivities()
	{
		$this->db->select('*');
		$this->db->from('activities');
		/*$this->db->join('activity_vendor', 'activity_vendor.id_activity = activities.id');
		$this->db->join('vendors', 'vendors.id_vendor = activity_vendor.id_vendor');*/
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	function listActivitiesVendor()
	{
		$this->db->select('*');
		$this->db->from('activities');
		$this->db->join('activity_vendor', 'activity_vendor.id_activity = activities.id');
		$this->db->join('vendors', 'vendors.id_vendor = activity_vendor.id_vendor');
		$this->db->order_by('date_activity', 'desc');
		$this->db->where('activity_vendor.status', 0);
		$query = $this->db->get();
		return $query->result();
	}

	function listActivitities2($vendor_id)
	{
		$this->db->select('*');
		$this->db->from('activities');
		$this->db->join('activity_vendor', 'activity_vendor.id_activity = activities.id');
		$this->db->join('vendors', 'vendors.id_vendor = activity_vendor.id_vendor');
		$this->db->order_by('date_activity', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	function saveActividad($datos)
	{	
		if ($this->db->insert('activities', $datos)) {
			echo json_encode(['status' => 'success', 'message' => 'Actividad Registrada!']);
		} else {
			echo json_encode(['status' => 'success', 'message' => 'Error, intenta nuevamente!']);
		}
		
	}

	function addActivityVendor($data)
	{
		$this->db->select('*');
		$this->db->from('activity_vendor');
		$this->db->where('id_activity', $data['id_activity']);
		$this->db->where('id_vendor', $data['id_vendor']);
		$query = $this->db->get();
		$r = $query->result();

		if (empty($r)) {
			$this->db->insert('activity_vendor', $data);
			echo json_encode(['status' => 'success', 'message' => 'Vendedor asignado!']);
			return true;
		}else{
			echo json_encode(['status' => 'error', 'message' => 'El vendedor ya existe!']);
			return false;
		}
		
	}

	function showVendors($activity)
	{
		$this->db->select('*');
		$this->db->from('activity_vendor');
		$this->db->join('vendors', 'vendors.id_vendor = activity_vendor.id_vendor');
		$this->db->where('activity_vendor.id_activity', $activity);
		$this->db->where('activity_vendor.status', 1);
		$query = $this->db->get();
		return ($query->result());
	}


	function quitVendor ($data)
	{
		$this->db->select('*');
		$this->db->from('activity_vendor');
		$this->db->where('id_activity', $data['id_activity']);
		$this->db->where('id_vendor', $data['id_vendor']);
		$this->db->delete('activity_vendor');
		echo json_encode(['status' => 'success', 'message' => 'Vendedor retirado!']);
	}

	function activity($id)
	{
		$this->db->select('*');
		$this->db->from('activities');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return ($query->result());
	}

	function showActivity($id)
	{
		$this->db->select('name_activity');
		$this->db->from('activities');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return ($query->result());
	}

	function aprobeActivityVendor ($data)
	{
		$this->db->select('*');
		$this->db->from('activity_vendor');
		$this->db->where('id_activity', $data['id_activity']);
		$this->db->where('id_vendor', $data['id_vendor']);
		$this->db->set('status', 1);
		$this->db->update('activity_vendor');
		echo json_encode(['status' => 'success', 'message' => 'Vendedor habilitado para la actividad!']);
	}


	function updateActividad ($id_activity, $data)
	{
		$this->db->select('*');
		$this->db->from('activities');
		$this->db->where('id', $id_activity);

		$this->db->update('activities', $data);
		
		#echo json_encode(['status' => 'success', 'message' => 'Actividad actualizada!']);
	}

	function activitiesName($id_vendor)
	{
		$this->db->select('*');
		$this->db->from('activities');
		$this->db->join('activity_vendor', 'activity_vendor.id_activity = activities.id');
		$this->db->where('id_vendor', $id_vendor);
		$query = $this->db->get();
		return ($query->result());
	}

}

/* End of file Actividad_Model.php */
/* Location: ./application/models/Actividad_Model.php */